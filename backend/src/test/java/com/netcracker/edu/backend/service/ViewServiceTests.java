package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.model.View;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ViewServiceTests {

    @Autowired
    private ViewService viewService;

    @Test
    @Transactional
    @Rollback
    public void addViewTest() {
        View view = viewService.addView(new View(0, 0, Timestamp.from(Instant.now())));

        Assert.assertNotNull(view);
    }

    @Test
    @Transactional
    @Rollback
    public void countViewsByTripIdTest() {
        Assert.assertTrue(viewService.countViewsByTripId(0) >= 0);
    }
}
