package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.dto.response.ApiResponse;
import com.netcracker.edu.backend.service.ViewService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ViewControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ViewService viewService;

    @Test
    public void addViewTest() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/views",
                ApiResponse.class)).isInstanceOf(ApiResponse.class);
    }

    @Test
    public void countViewsByTripIdTest() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/views/trip/0",
                Long.class)).isInstanceOf(viewService.countViewsByTripId(0).getClass());
    }
}
