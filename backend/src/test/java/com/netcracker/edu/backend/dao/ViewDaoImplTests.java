package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.View;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ViewDaoImplTests {

    @Autowired
    private ViewDao viewDao;

    @Test
    @Transactional
    @Rollback
    public void createAndGetViewTest() throws Exception {

        View view = new View(0, 0, Timestamp.from(Instant.now()));

        view = viewDao.insert(view);

        Assert.assertEquals(view, viewDao.getById(view.getId()).get());
    }

    @Test
    @Transactional
    @Rollback
    public void countViewsByTripIdTest() throws Exception {

        Assert.assertTrue(viewDao.countViewsByTripId(0).get() >= 0);
    }

    @Test
    @Transactional
    @Rollback
    public void getAllViewsTest() throws Exception {

        List<View> views = viewDao.getAll();

        View view = new View(0, 0, Timestamp.from(Instant.now()));

        view = viewDao.insert(view);

        views.add(view);

        Assert.assertArrayEquals(views.toArray(), viewDao.getAll().toArray());
    }
}
