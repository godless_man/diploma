package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ServiceDao;
import com.netcracker.edu.backend.dto.response.AdminReportResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
@Slf4j
public class ReportService {

    @Autowired
    private ServiceDao serviceDao;

    //    @Autowired
    //    private DiscountDao discountDao;

    public List<AdminReportResponse> getAllReport(Date dateStart, Date dateEnd, String serviceType) {
        log.debug("Getting report");

        Timestamp start = new Timestamp(dateStart.getTime());
        Timestamp end = new Timestamp(dateEnd.getTime());

        List<com.netcracker.edu.backend.model.Service> salesService = serviceDao.getSalesBySomePeriod(start, end, serviceType);

        //        List<Discount> discounts = discountDao.getDiscountsForPeriod(start, end);

        List<AdminReportResponse> responses = new ArrayList<>();

        for (com.netcracker.edu.backend.model.Service service : salesService) {
            log.debug(service.getName());
            if (service.getDiscount() != null) {
                if (service.getDiscount().getTypeId() == 1) {
                    service.setPrice(service.getPrice().multiply(BigDecimal.valueOf(100 - service.getDiscount().getAmount() / 100)));
                } else {
                    service.setPrice(service.getPrice().subtract(BigDecimal.valueOf(service.getDiscount().getAmount())));
                }
            }

            if (containsName(responses, service.getLocation().getName())) {
                AdminReportResponse report = responses.stream().filter(o -> o.getLocation().equals(service.getLocation().getName())).findFirst().get();
                report.setPrice((service.getPrice().add(BigDecimal.valueOf(report.getPrice()))).floatValue());
                report.setNumberTour(report.getNumberTour() + 1);
            } else {
                responses.add(new AdminReportResponse(service.getLocation().getName(), 1, service.getPrice().floatValue()));
            }
        }

        return responses;
    }

    public boolean containsName(final List<AdminReportResponse> list, final String city) {
        return list.stream().anyMatch(o -> o.getLocation().equals(city));
    }
}
