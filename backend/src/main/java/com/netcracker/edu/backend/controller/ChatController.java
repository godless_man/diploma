package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.Chat;
import com.netcracker.edu.backend.model.ChatMessage;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.ChatMessageService;
import com.netcracker.edu.backend.service.ChatService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.Instant;

import static com.netcracker.edu.backend.utils.Constants.RoleName;

@RestController
@RequestMapping("/api/chat")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class ChatController {

    @Autowired
    private ChatService chatService;

    @Autowired
    private ChatMessageService chatMessageService;

    @ApiOperation(value = "loadChatByUserId", notes = "Loads chat for current user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured({RoleName.ROLE_APPROVER, RoleName.ROLE_USER})
    @GetMapping
    public ResponseEntity<?> loadChatByUserId(@CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting chat");

        return ResponseEntity.ok(chatService.getChatByUserIdOrCreate(userPrincipal));
    }

    @ApiOperation(value = "loadChatMessagesByChatId", notes = "Loads chat messages for current chat")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured({RoleName.ROLE_APPROVER, RoleName.ROLE_USER})
    @GetMapping("/{id}")
    public ResponseEntity<?> loadChatMessagesByChatId(@PathVariable long id) {
        log.debug("Requesting chat messages");

        return ResponseEntity.ok(chatMessageService.getChatMessageByChatId(id));
    }

    @ApiOperation(value = "postChatMessage", notes = "Takes message and insert it to DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured({RoleName.ROLE_APPROVER, RoleName.ROLE_USER})
    @PostMapping("/{id}")
    public ResponseEntity<?> postChatMessage(@RequestBody String message,
                                             @PathVariable long id, @CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting chat messages");

        return ResponseEntity.ok(chatMessageService.createChatMessage(
                new ChatMessage(id, userPrincipal.getId(), message, Timestamp.from(Instant.now()), userPrincipal.getUsername())));
    }


    @ApiOperation(value = "loadChatsForApprover", notes = "Loads chats for current approver")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured({RoleName.ROLE_APPROVER})
    @GetMapping("/approver")
    public ResponseEntity<?> loadChatsForApprover(@CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting chat");

        return ResponseEntity.ok(chatService.getAllChatsForApprover(userPrincipal.getId()));
    }

    @ApiOperation(value = "changeAssignment", notes = "Change chat status to assigned/unassigned")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal")
    })
    @Secured({RoleName.ROLE_APPROVER})
    @PutMapping
    public ResponseEntity<?> changeAssignment(@RequestBody Chat chat, @CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting chat assignment change");

        return ResponseEntity.ok(chatService.changeChatAssignment(chat, userPrincipal));
    }
}
