package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Coordinate;
import com.netcracker.edu.backend.utils.Constants;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CoordinateMapper implements RowMapper<Coordinate> {
    @Override
    public Coordinate mapRow(ResultSet resultSet, int i) throws SQLException {

        Coordinate coordinate = new Coordinate();

        coordinate.setId(resultSet.getLong(Constants.CoordinateColumns.COORDINATE_ID));
        coordinate.setLatitude(resultSet.getFloat(Constants.CoordinateColumns.COORDINATE_LATITUDE));
        coordinate.setLongitude(resultSet.getFloat(Constants.CoordinateColumns.COORDINATE_LONGITUDE));

        return coordinate;
    }
}
