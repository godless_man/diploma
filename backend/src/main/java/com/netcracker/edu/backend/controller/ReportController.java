package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.service.ReportService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

import static com.netcracker.edu.backend.utils.Constants.RoleName;
import static com.netcracker.edu.backend.utils.Constants.ServiceType;

@RestController
@RequestMapping("/api/account/report")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class ReportController {

    @Autowired
    private ReportService reportService;

    @ApiOperation(value = "getAllTrips", notes = "Loads trips report")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_ADMIN)
    @PostMapping("/trip")
    public ResponseEntity<?> getAllTrips(@Valid @RequestBody Date[] dates) {
        log.debug("Requesting all trips for report");

        return ResponseEntity.ok(reportService.getAllReport(dates[0], dates[1], ServiceType.TRIP));
    }

    @ApiOperation(value = "getAllService", notes = "Loads services report")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_ADMIN)
    @PostMapping("/service")
    public ResponseEntity<?> getAllService(@Valid @RequestBody Date[] dates) {
        log.debug("Requesting all services for report");

        return ResponseEntity.ok(reportService.getAllReport(dates[0], dates[1], ServiceType.SERVICE));
    }

    @ApiOperation(value = "getAllSuggestion", notes = "Loads suggestions report")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_ADMIN)
    @PostMapping("/suggestion")
    public ResponseEntity<?> getAllSuggestion(@Valid @RequestBody Date[] dates) {
        log.debug("Requesting all suggestions for report");

        return ResponseEntity.ok(reportService.getAllReport(dates[0], dates[1], ServiceType.SUGGESTION));
    }

    @ApiOperation(value = "getAllBundle", notes = "Loads bundles report")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_ADMIN)
    @PostMapping("/bundle")
    public ResponseEntity<?> getAllBundle(@Valid @RequestBody Date[] dates) {
        log.debug("Requesting all bundles for report");

        return ResponseEntity.ok(reportService.getAllReport(dates[0], dates[1], ServiceType.BUNDLE));
    }
}
