package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.TroubleTicketFeedback;

public interface TroubleTicketFeedbackDao extends GenericDao<TroubleTicketFeedback> {
}
