package com.netcracker.edu.backend.dto.request;

import com.netcracker.edu.backend.model.Service;
import com.netcracker.edu.backend.model.TroubleTicket;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChangeStatusRequest {

    private Service service;

    private TroubleTicket troubleTicket;

    @NotNull
    private long statusId;

    public Optional<Service> getServiceOptional() {
        return Optional.of(service);
    }

    public Optional<TroubleTicket> getTroubleTicketOptional() {
        return Optional.of(troubleTicket);
    }
}
