package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.UserDao;
import com.netcracker.edu.backend.mapper.LongMapper;
import com.netcracker.edu.backend.mapper.SubscriptionMapper;
import com.netcracker.edu.backend.mapper.UserDetailedMapper;
import com.netcracker.edu.backend.mapper.UserMapper;
import com.netcracker.edu.backend.model.Subscription;
import com.netcracker.edu.backend.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

    @Value("${db.query.user.insert}")
    private String sqlUserInsert;

    @Value("${db.query.user.update}")
    private String sqlUserUpdate;

    @Value("${db.query.user.getByUsername}")
    private String sqlGetUserByUsername;

    @Value("${db.query.user.getByEmail}")
    private String sqlGetUserByEmail;

    @Value("${db.query.user.getByUsernameOrEmail}")
    private String sqlGetUserByUsernameOrEmail;

    @Value("${db.query.user.getAllDetailed}")
    private String sqlGetAllDetailed;

    @Value("${db.query.user.getDetailedById}")
    private String sqlGetDetailedUserById;

    @Value("${db.query.user.getDetailedByUsername}")
    private String sqlGetDetailedUserByUsername;

    @Value("${db.query.user.getDetailedByEmail}")
    private String sqlGetDetailedUserByEmail;

    @Value("${db.query.user.getDetailedByUsernameOrEmail}")
    private String sqlGetDetailedUserByUsernameOrEmail;

    @Value("${db.query.user.getDetailedByRole}")
    private String sqlGetDetailedUserByRole;

    @Value("${db.query.user.countUserTripPurchases}")
    private String sqlCountUserTripPurchases;

    private static final RowMapper<Subscription> subscriptionMapper = new SubscriptionMapper();
    @Value("${db.query.subscription.insert}")
    private String sqlSubscribeInsert;
    @Value("${db.query.subscription.deleteByUserId}")
    private String sqlDeleteByUserId;
    @Value("${db.query.subscription.getByUserIdAndProviderId}")
    private String sqlGetByUserIdAndProviderId;
    @Value("${db.query.subscription.getSubscriptions}")
    private String sqlGetSubscriptions;
    @Value("${db.query.subscription.getSubscribed}")
    private String sqlGetSubscribed;
    @Value("${db.query.subscription.getSubscriptionCountForProvider}")
    private String sqlGetSubscriptionCountForProvider;

    private static final RowMapper<User> userRowMapperDetailed = new UserDetailedMapper();


    public UserDaoImpl() {
        super(new UserMapper(), TableName.USER);
    }

    @Override
    protected String getInsertSql() {
        return sqlUserInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, User entity) throws SQLException {
        int argNum = 1;
        statement.setLong(argNum++, entity.getAuthorityId());
        statement.setString(argNum++, entity.getEmail());
        statement.setString(argNum++, entity.getUsername());
        statement.setString(argNum++, entity.getPassword());
        statement.setBoolean(argNum++, entity.isActive());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlUserUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(User entity) {
        return new Object[]{entity.getAuthorityId(), entity.getEmail(), entity.getUsername(), entity.getPassword(), entity.isActive()};
    }

    @Override
    public Optional<User> getUserByUsernameOrEmail(String usernameOrEmail) {
        log.debug("Getting user by usernameOrEmail: {}", usernameOrEmail);

        List<User> user = jdbcTemplate.query(sqlGetUserByUsernameOrEmail, new Object[]{usernameOrEmail, usernameOrEmail}, rowMapper);

        log.debug("Found streets (by usernameOrEmail) with ids {}", user.stream().map(User::getId).collect(Collectors.toList()));

        return getSingleElement(user);
    }

    @Override
    public boolean existsByUsername(String username) {
        return !(jdbcTemplate.query(sqlGetUserByUsername, new Object[]{username}, rowMapper)).isEmpty();
    }

    @Override
    public boolean existsByEmail(String email) {
        return !(jdbcTemplate.query(sqlGetUserByEmail, new Object[]{email}, rowMapper)).isEmpty();
    }

    @Override
    public List<User> getAllDetailed() {
        log.debug("Getting detailed users");

        List<User> users = jdbcTemplate.query(sqlGetAllDetailed, userRowMapperDetailed);

        log.debug("Found users with ids {}", users.stream().map(User::getId).collect(Collectors.toList()));

        return users;
    }

    @Override
    public Optional<User> getDetailedUserById(long id) {
        log.debug("Getting detailed user by id: {}", id);

        List<User> user = jdbcTemplate.query(sqlGetDetailedUserById, new Object[]{id}, userRowMapperDetailed);

        log.debug("Found users with ids {}", user.stream().map(User::getId).collect(Collectors.toList()));

        return getSingleElement(user);
    }

    @Override
    public Optional<User> getDetailedUserByUsername(String username) {
        log.debug("Getting detailed user by username: {}", username);

        List<User> user = jdbcTemplate.query(sqlGetDetailedUserByUsername, new Object[]{username}, userRowMapperDetailed);

        log.debug("Found users with ids {}", user.stream().map(User::getId).collect(Collectors.toList()));

        return getSingleElement(user);
    }

    @Override
    public Optional<User> getDetailedUserByEmail(String email) {
        log.debug("Getting detailed user by email: {}", email);

        List<User> user = jdbcTemplate.query(sqlGetDetailedUserByEmail, new Object[]{email}, userRowMapperDetailed);

        log.debug("Found users with ids {}", user.stream().map(User::getId).collect(Collectors.toList()));

        return getSingleElement(user);
    }

    @Override
    public Optional<User> getDetailedUserByUsernameOrEmail(String usernameOrEmail) {
        log.debug("Getting detailed user by usernameOrEmail: {}", usernameOrEmail);

        List<User> user = jdbcTemplate.query(sqlGetDetailedUserByUsernameOrEmail, new Object[]{usernameOrEmail, usernameOrEmail}, userRowMapperDetailed);

        log.debug("Found users with ids {}", user.stream().map(User::getId).collect(Collectors.toList()));

        return getSingleElement(user);
    }

    @Override
    public List<User> getDetailedUserByUserRole(String role) {
        log.debug("Getting detailed user by role: {}", role);

        List<User> users = jdbcTemplate.query(sqlGetDetailedUserByRole, new Object[]{role}, userRowMapperDetailed);

        log.debug("Found users with ids {}", users.stream().map(User::getId).collect(Collectors.toList()));

        return users;
    }

    @Override
    public Long countUserTripPurchases(long userId, long tripId) {
        log.debug("Counting all trip(id = {}) purchases of user(id = {}) in {}: ", tripId, userId, this.getClass().getName());

        return jdbcTemplate.queryForObject(sqlCountUserTripPurchases, new Object[]{userId, tripId, tripId}, new LongMapper());
    }

    @Override
    public void deleteByUserId(Long id) {
        log.debug("Deleting user subscription by user id: {}", id);

        jdbcTemplate.update(sqlDeleteByUserId, id);
    }

    @Override
    public boolean isSubscribed(Long userId, long providerId) {
        log.debug("Checking is user with id {} subscribed to provider with id {}", userId, providerId);

        return !jdbcTemplate.query(sqlGetByUserIdAndProviderId, new Object[]{userId, providerId}, subscriptionMapper).isEmpty();
    }

    @Override
    public void insertSubscription(Long userId, long providerId) {
        log.debug("Creating subscription user {} to provider {}", userId, providerId);

        jdbcTemplate.update(sqlSubscribeInsert, userId, providerId);
    }

    @Override
    public List<Long> getSubscriptions(Long id) {
        log.debug("Getting subscription of user with id: {}", id);

        List<Long> subscriptions = jdbcTemplate.query(sqlGetSubscriptions, new Object[]{id}, new LongMapper());

        log.debug("Found subscriptions for user: {}", subscriptions);

        return subscriptions;
    }

    @Override
    public List<Long> getSubscribed(Long providerId) {
        log.debug("Getting subscribed users of provider with id: {}", providerId);

        List<Long> subscriptions = jdbcTemplate.query(sqlGetSubscribed, new Object[]{providerId}, new LongMapper());

        log.debug("Found subscriptions for user: {}", subscriptions);

        return subscriptions;
    }

    @Override
    public Long getSubscriptionCountForProvider(long providerId) {
        log.debug("Getting amount of subscribed users of provider with id: {}", providerId);

        Long subscriptionCount = jdbcTemplate.queryForObject(sqlGetSubscriptionCountForProvider, new Object[]{providerId}, new LongMapper());

        log.debug("Found subscriptions amount for provider: {}", subscriptionCount);

        return subscriptionCount;
    }
}
