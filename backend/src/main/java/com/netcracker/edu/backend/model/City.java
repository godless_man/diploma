package com.netcracker.edu.backend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class City extends Identified {

    private String name;
    private long countryId;
    private Country country;
    private long coordinateId;
    private Coordinate coordinate;

    public City(String name, long countryId, long coordinateId) {
        this.name = name;
        this.countryId = countryId;
        this.coordinateId = coordinateId;
    }
}
