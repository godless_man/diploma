package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.City;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.CityColumns;

public class CityMapper implements RowMapper<City> {
    @Override
    public City mapRow(ResultSet resultSet, int i) throws SQLException {

        City city = new City();

        city.setId(resultSet.getLong(CityColumns.CITY_ID));
        city.setName(resultSet.getString(CityColumns.NAME));
        city.setCountryId(resultSet.getLong(CityColumns.COUNTRY_ID));
        city.setCoordinateId(resultSet.getLong(CityColumns.COORDINATE_ID));

        return city;
    }
}
