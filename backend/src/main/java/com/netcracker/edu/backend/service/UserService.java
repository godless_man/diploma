package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.TokenDao;
import com.netcracker.edu.backend.dao.UserDao;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.Token;
import com.netcracker.edu.backend.model.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import static com.netcracker.edu.backend.utils.Constants.PasswordCharacters.ALL_PASSWORD_CHARACTERS;
import static com.netcracker.edu.backend.utils.Constants.TokenExpirationTime.EXPIRATION;
import static com.netcracker.edu.backend.utils.Constants.VerificationTokenStatus.*;

@Service
@Transactional
@Slf4j
public class UserService {

    private UserDao userDao;
    private UserDetailsService userDetailsService;
    private TokenDao tokenDao;

    @Autowired
    public UserService(UserDao userDao, UserDetailsService userDetailsService, TokenDao tokenDao) {
        this.userDao = userDao;
        this.userDetailsService = userDetailsService;
        this.tokenDao = tokenDao;
    }

    public User getUserById(Long id) {
        log.debug("Getting user");

        return userDao.getById(id)
                .orElseThrow(() -> {
                    log.error("User not found with id: {}", id);
                    return new AppException("User with id " + id + " not found");
                });
    }

    public List<User> getAllDetailed() {
        log.debug("Getting users");

        return userDao.getAllDetailed();
    }

    public User getDetailedUserById(Long id) {
        log.debug("Getting user");

        return userDao.getDetailedUserById(id)
                .orElseThrow(() -> {
                    log.error("User not found with id: {}", id);
                    return new AppException("User with id " + id + " not found");
                });
    }

    public boolean existsByUsername(String username) {
        log.debug("Getting exists by username");

        return userDao.existsByUsername(username);
    }

    public boolean existsByEmail(String email) {
        log.debug("Getting exists by email");

        return userDao.existsByEmail(email);
    }

    public User getUserByUsernameOrEmail(String usernameOrEmail) {
        log.debug("Getting exists by usernameOrEmail");

        return userDao.getUserByUsernameOrEmail(usernameOrEmail)
                .orElseThrow(() -> {
                    log.error("User not found with email or username: {}", usernameOrEmail);
                    return new AppException("User with email or username " + usernameOrEmail + " not found");
                });
    }

    public List<User> getAllUsers() {
        log.debug("Getting users");

        return userDao.getAll();
    }

    public void deleteUserById(long id) {
        log.debug("Deleting user");

        userDetailsService.deleteUserDetailsById(id);
        userDao.deleteById(id);
    }

    public void deleteUser(User user) {
        log.debug("Deleting user");

        userDetailsService.deleteUserDetails(user.getDetails());
        userDao.delete(user);
    }

    public void updateUser(User user) {
        log.debug("Updating user");

        userDao.update(user);
    }

    public User createUser(User user) {
        log.debug("Creating user");

        return userDao.insert(user);
    }

    public void createVerificationTokenForUser(User user, String token) {
        log.debug("Creating verification token for user");

        Token myToken = new Token(token, user.getId(), calculateExpiryDate());
        tokenDao.insert(myToken);
    }

    public String validateVerificationToken(String token) {
        log.debug("Validating verification token for user");

        if (token == null) {
            return TOKEN_INVALID;
        }
        Token verificationToken = tokenDao.findByToken(token)
                .orElseThrow(() -> {
                    log.error("No verification token found");
                    return new AppException("No verification token found");
                });
        User user = getUserById(verificationToken.getUserID());

        if (isTokenExpired(verificationToken)) {
            tokenDao.delete(verificationToken);
            return TOKEN_EXPIRED;
        }

        user.setActive(true);
        tokenDao.delete(verificationToken);
        userDao.update(user);
        return TOKEN_VALID;
    }

    private boolean isTokenExpired(Token verificationToken) {
        Calendar cal = Calendar.getInstance();
        return (verificationToken.getExpiryDate()
                .getTime()
                - cal.getTime()
                .getTime()) <= 0;
    }

    public Timestamp calculateExpiryDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, Integer.parseInt(EXPIRATION));
        return new Timestamp(cal.getTime().getTime());
    }

    public String resetPassword(String email, PasswordEncoder encoder) {
        log.debug("Resetting password for user");

        User user = getUserByUsernameOrEmail(email);
        String newPassword = getNewPassword();
        user.setPassword(encoder.encode(newPassword));
        updateUser(user);
        return newPassword;
    }

    public String getNewPassword() {
        return RandomStringUtils.random(15, ALL_PASSWORD_CHARACTERS);
    }

    public long countUserTripPurchases(long userId, long tripId) {
        log.debug("Counting all trip(id = {}) purchases of user(id = {}) in {}: ", tripId, userId, this.getClass().getName());

        Long amount = userDao.countUserTripPurchases(userId, tripId);

        if (amount == null) {
            log.error("Unable to count purchases");
            throw new AppException("Unable to count purchases");
        } else {
            return amount;
        }
    }

    public List<User> getDetailedUserByRole(String role) {
        log.debug("Getting users with details by role");
        return userDao.getDetailedUserByUserRole(role);
    }
}
