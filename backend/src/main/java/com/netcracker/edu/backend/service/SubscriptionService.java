package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.UserDao;
import com.netcracker.edu.backend.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.netcracker.edu.backend.utils.Constants.ResponseEntities.SUBSCRIBED_SUCCESSFULLY;

@Service
@Transactional
@Slf4j
public class SubscriptionService {

    private UserDao userDao;

    @Autowired
    public SubscriptionService(UserDao userDao) {
        this.userDao = userDao;
    }

    public ResponseEntity<?> subscribe(Long userId, long providerId) {
        log.debug("Subscribing");

        userDao.insertSubscription(userId, providerId);
        return SUBSCRIBED_SUCCESSFULLY;
    }

    public void unsubscribe(Long id) {
        log.debug("Unsubscribing");

        userDao.deleteByUserId(id);
    }

    public boolean isSubscribed(Long userId, long providerId) {
        log.debug("Checking is in subscribe");

        return userDao.isSubscribed(userId, providerId);
    }

    public List<User> getSubscriptions(Long id) {
        log.debug("Getting subscription");

        List<Long> providersId = userDao.getSubscriptions(id);
        List<User> providers = new ArrayList<>();
        for (Long pId : providersId) {
            providers.add(userDao.getDetailedUserById(pId).get());
        }
        return providers;
    }

    List<User> getSubscribedUsers(Long providerId) {
        log.debug("Getting subscribed");

        List<Long> usersId = userDao.getSubscribed(providerId);
        List<User> users = new ArrayList<>();
        for (Long uId : usersId) {
            users.add(userDao.getDetailedUserById(uId).get());
        }
        return users;
    }

    public Long getSubscriptionCountForProvider(long providerId) {
        return userDao.getSubscriptionCountForProvider(providerId);
    }
}
