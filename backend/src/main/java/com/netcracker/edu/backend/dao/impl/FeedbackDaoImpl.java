package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.FeedbackDao;
import com.netcracker.edu.backend.mapper.FeedbackDetailedMapper;
import com.netcracker.edu.backend.mapper.FeedbackMapper;
import com.netcracker.edu.backend.mapper.FeedbackUserMapper;
import com.netcracker.edu.backend.model.Feedback;
import com.netcracker.edu.backend.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
@Slf4j
public class FeedbackDaoImpl extends GenericDaoImpl<Feedback> implements FeedbackDao {

    @Value("${db.query.feedback.insert}")
    private String sqlFeedbackInsert;
    @Value("${db.query.feedback.update}")
    private String sqlFeedbackUpdate;
    @Value("${db.query.feedback.getTripFeedbackWithUsers}")
    private String sqlGetTripFeedbackWithUsers;

    private static final RowMapper FEEDBACK_DETAILED_MAPPER = new FeedbackDetailedMapper();
    private static final RowMapper FEEDBACK_USER_MAPPER = new FeedbackUserMapper();

    public FeedbackDaoImpl() {
        super(new FeedbackMapper(), Constants.TableName.FEEDBACK);
    }

    @Override
    protected String getInsertSql() {
        return sqlFeedbackInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Feedback entity) throws SQLException {

        int argNum = 1;
        statement.setLong(argNum++, entity.getUserId());
        statement.setLong(argNum++, entity.getServiceId());
        statement.setLong(argNum++, entity.getRating());
        statement.setString(argNum++, entity.getFeedbackMessage());
        statement.setTimestamp(argNum++, entity.getFeedbackDate());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlFeedbackUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(Feedback entity) {
        return new Object[]{entity.getUserId(), entity.getServiceId(), entity.getRating(), entity.getFeedbackMessage(), entity.getFeedbackDate()};
    }

    @Override
    public List<Feedback> getTripFeedbackWithUsers(long tripId) {
        log.debug("Getting all trip's feedback with mapped users in {}: ", this.getClass().getName());

        return jdbcTemplate.query(sqlGetTripFeedbackWithUsers, new Object[]{tripId}, FEEDBACK_USER_MAPPER);
    }

}

