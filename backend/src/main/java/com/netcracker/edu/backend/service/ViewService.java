package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ViewDao;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.View;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;

@Service
@Slf4j
public class ViewService {

    @Autowired
    private ViewDao viewDao;

    public View addView(View view) {
        log.debug("Adding view");

        return viewDao.insert(view);
    }

    public View addView(long userId, long serviceId) {
        log.debug("Adding view");

        return viewDao.insert(new View(userId, serviceId, Timestamp.from(Instant.now())));
    }

    public Long countViewsByTripId(long id) {
        log.debug("Counting views by trip(id = {})", id);

        return viewDao.countViewsByTripId(id)
                .orElseThrow(() -> {
                    log.error("Unexpected views error");
                    return new AppException("Unexpected views error");
                });
    }

    public Long countViewsByAllTrips(long id) {
        log.debug("Counting all trips for user id = {}", id);

        return viewDao.countViewsByAllTrips(id)
                .orElseThrow(() -> {
                    log.error("Unexpected views error");
                    return new AppException("Unexpected views error");
                });
    }

    public Long countAllViews(long id) {
        log.debug("Counting all views for user id = {}", id);

        return viewDao.countAllViews(id)
                .orElseThrow(() -> {
                    log.error("Unexpected views error");
                    return new AppException("Unexpected views error");
                });
    }

    public Long countAllViewsByWeek(long id) {
        log.debug("Counting all views by week for user id = {}", id);

        return viewDao.countAllViewsByWeek(id)
                .orElseThrow(() -> {
                    log.error("Unexpected views error");
                    return new AppException("Unexpected views error");
                });
    }
}
