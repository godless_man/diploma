package com.netcracker.edu.backend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
public class Feedback extends Identified {

    @NotNull
    private long userId;

    @NotNull
    private long serviceId;

    @NotNull
    @Min(0)
    @Max(5)
    private long rating;

    private String feedbackMessage;

    @NotNull
    private Timestamp feedbackDate;

    private User user;
    private Service service;
}
