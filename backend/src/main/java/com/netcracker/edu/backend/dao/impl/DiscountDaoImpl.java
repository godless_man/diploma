package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.DiscountDao;
import com.netcracker.edu.backend.mapper.DiscountDetailedMapper;
import com.netcracker.edu.backend.mapper.DiscountMapper;
import com.netcracker.edu.backend.model.Discount;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class DiscountDaoImpl extends GenericDaoImpl<Discount> implements DiscountDao {

    private static final RowMapper<Discount> rowMapperDetailed = new DiscountDetailedMapper();

    @Value("${db.query.discount.insert}")
    private String sqlDiscountInsert;

    @Value("${db.query.discount.update}")
    private String sqlDiscountUpdate;

    @Value("${db.query.discount.getDiscountsOfType}")
    private String sqlGetDiscountsOfType;

    @Value("${db.query.discount.getDiscountsOfTypeDetailed}")
    private String sqlGetDiscountsOfTypeDetailed;

    @Value("${db.query.discount.getDiscountsByServiceId}")
    private String sqlGetDiscountsByServiceId;

    @Value("${db.query.discount.getDiscountsByServiceIdDetailed}")
    private String sqlGetDiscountsByServiceIdDetailed;

    @Value("${db.query.discount.getDiscountsForPeriod}")
    private String sqlGetDiscountsForPeriod;

    @Value("${db.query.discount.getDiscountsForPeriodDetailed}")
    private String sqlGetDiscountsForPeriodDetailed;

    @Value("${db.query.discount.getTripRelatedDiscountsByTripId}")
    private String sqlGetTripRelatedDiscountsByTripId;

    public DiscountDaoImpl() {
        super(new DiscountMapper(), TableName.DISCOUNT);
    }

    @Override
    protected String getInsertSql() {
        return sqlDiscountInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Discount entity) throws SQLException {

        int argNum = 1;
        statement.setLong(argNum++, entity.getServiceId());
        statement.setLong(argNum++, entity.getTypeId());
        statement.setDouble(argNum++, entity.getAmount());
        statement.setTimestamp(argNum++, entity.getStartDate());
        statement.setTimestamp(argNum++, entity.getEndDate());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlDiscountUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(Discount entity) {
        return new Object[]{entity.getServiceId(), entity.getTypeId(), entity.getAmount(), entity.getStartDate(), entity.getEndDate()};
    }

    @Override
    public List<Discount> getDiscountsOfType(String type) {
        log.debug("Getting discounts by type: {}", type);

        List<Discount> discounts = jdbcTemplate.query(sqlGetDiscountsOfType, new Object[]{type}, rowMapper);

        log.debug("Found discounts (by type) with ids {}", discounts.stream().map(Discount::getId).collect(Collectors.toList()));

        return discounts;
    }

    @Override
    public List<Discount> getDiscountsByServiceId(long serviceId) {
        log.debug("Getting discounts by serviceId: {}", serviceId);

        List<Discount> discounts = jdbcTemplate.query(sqlGetDiscountsByServiceId, new Object[]{serviceId}, rowMapper);

        log.debug("Found discounts (by serviceId) with ids {}", discounts.stream().map(Discount::getId).collect(Collectors.toList()));

        return discounts;
    }

    @Override
    public List<Discount> getDiscountsForPeriod(Timestamp from, Timestamp to) {
        log.debug("Getting discounts for period: {} - {}", from, to);

        List<Discount> discounts = jdbcTemplate.query(sqlGetDiscountsForPeriod, new Object[]{from, to}, rowMapper);

        log.debug("Found discounts (for period) with ids {}", discounts.stream().map(Discount::getId).collect(Collectors.toList()));

        return discounts;
    }

    @Override
    public List<Discount> getDiscountsOfTypeDetailed(String type) {
        log.debug("Getting detailed discounts by type: {}", type);

        List<Discount> discounts = jdbcTemplate.query(sqlGetDiscountsOfTypeDetailed, new Object[]{type}, rowMapperDetailed);

        log.debug("Found discounts (by type) with ids {}", discounts.stream().map(Discount::getId).collect(Collectors.toList()));

        return discounts;
    }

    @Override
    public List<Discount> getDiscountsByServiceIdDetailed(long serviceId) {
        log.debug("Getting detailed discounts by serviceId: {}", serviceId);

        List<Discount> discounts = jdbcTemplate.query(sqlGetDiscountsByServiceIdDetailed, new Object[]{serviceId}, rowMapperDetailed);

        log.debug("Found discounts (by serviceId) with ids {}", discounts.stream().map(Discount::getId).collect(Collectors.toList()));

        return discounts;
    }

    @Override
    public List<Discount> getDiscountsForPeriodDetailed(Timestamp from, Timestamp to) {
        log.debug("Getting detailed discounts for period: {} - {}", from, to);

        List<Discount> discounts = jdbcTemplate.query(sqlGetDiscountsForPeriodDetailed, new Object[]{from, to}, rowMapperDetailed);

        log.debug("Found discounts (for period) with ids {}", discounts.stream().map(Discount::getId).collect(Collectors.toList()));

        return discounts;
    }

    @Override
    public List<Discount> getTripRelatedDiscountsByTripId(long tripId) {
        //TODO Add logs
        return jdbcTemplate.query(sqlGetTripRelatedDiscountsByTripId, new Object[]{tripId, tripId}, rowMapperDetailed);
    }
}
