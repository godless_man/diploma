package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dto.request.LoginRequest;
import com.netcracker.edu.backend.dto.request.SignUpRequest;
import com.netcracker.edu.backend.dto.response.ApiResponse;
import com.netcracker.edu.backend.enums.Authority;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.model.UserDetails;
import com.netcracker.edu.backend.security.JwtTokenProvider;
import com.netcracker.edu.backend.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import javax.mail.MessagingException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

import static com.netcracker.edu.backend.utils.Constants.ResponseEntities;
import static com.netcracker.edu.backend.utils.Constants.VerificationTokenStatus.TOKEN_EXPIRED;
import static com.netcracker.edu.backend.utils.Constants.VerificationTokenStatus.TOKEN_INVALID;

@Service
@Transactional
@Slf4j
public class AuthService {

    private UserService userService;
    private UserDetailsService userDetailsService;
    private EmailService emailService;
    private AuthenticationManager authenticationManager;
    private PasswordEncoder passwordEncoder;
    private JwtTokenProvider tokenProvider;

    @Autowired
    public AuthService(UserService userService, UserDetailsService userDetailsService, EmailService emailService,
                       AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, JwtTokenProvider tokenProvider) {
        this.userService = userService;
        this.userDetailsService = userDetailsService;
        this.emailService = emailService;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
    }

    @Transactional
    public ResponseEntity<ApiResponse> registerUser(SignUpRequest request) {

        log.debug("Registering user");

        if (userService.existsByUsername(request.getUsername())) {
            return ResponseEntities.BAD_REQ_USERNAME_TAKEN;
        }
        if (userService.existsByEmail(request.getEmail())) {
            return ResponseEntities.BAD_REQ_EMAIL_TAKEN;
        }

        User user = new User(Authority.valueOf(request.getRole()).getId(), request.getEmail(), request.getUsername(),
                passwordEncoder.encode(request.getPassword()), false);

        userService.createUser(user);

        UserDetails userDetails = new UserDetails(user.getId(), request.getFirstName(), request.getLastName(),
                Timestamp.from(Instant.now()));

        userDetails.setImageSrc(Constants.Gravatar.GRAVATAR_BASE_URL + getEmailHash(user.getEmail()));

        userDetailsService.createUserDetails(userDetails);

        createAndSendToken(user);

        return ResponseEntities.USER_REGISTERED_SUCCESSFULLY;
    }

    public String getEmailHash(String email) {
        return DigestUtils.md5DigestAsHex(email.trim().toLowerCase().getBytes());
    }

    @Transactional
    public ResponseEntity<ApiResponse> registerSpecialUser(SignUpRequest request) {

        log.debug("Registering special user");

        if (userService.existsByUsername(request.getUsername())) {
            return ResponseEntities.BAD_REQ_USERNAME_TAKEN;
        }
        if (userService.existsByEmail(request.getEmail())) {
            return ResponseEntities.BAD_REQ_EMAIL_TAKEN;
        }

        String newPassword = userService.getNewPassword();

        User user = new User(Authority.valueOf(request.getRole()).getId(), request.getEmail(), request.getUsername(),
                passwordEncoder.encode(newPassword), true);

        userService.createUser(user);

        UserDetails userDetails = new UserDetails(user.getId(), request.getFirstName(), request.getLastName(),
                Timestamp.from(Instant.now()));

        userDetails.setImageSrc(Constants.Gravatar.GRAVATAR_BASE_URL + getEmailHash(user.getEmail()));

        userDetailsService.createUserDetails(userDetails);

        try {
            emailService.sendEmailForSpecUser(user.getEmail(), user.getUsername(), newPassword, user.getAuthority());
        } catch (MessagingException e) {
            log.error("Error with sending email with special user for {}", user);
        }

        if (Objects.equals(user.getAuthority(), Constants.RoleName.ROLE_APPROVER)) {
            return ResponseEntities.APPROVER_REGISTERED_SUCCESSFULLY;
        } else {
            return ResponseEntities.PROVIDER_REGISTERED_SUCCESSFULLY;
        }
    }

    public String authenticateUser(LoginRequest request) {

        log.debug("Authenticating user");

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsernameOrEmail(),
                        request.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        if (userService.getUserByUsernameOrEmail(request.getUsernameOrEmail()).isActive()) {
            return tokenProvider.generateToken(authentication);
        } else {
            throw new AppException("Account is not activated.");
        }
    }

    private void createAndSendToken(User user) {

        log.debug("Creating and sending token");

        String token = UUID.randomUUID().toString();

        userService.createVerificationTokenForUser(user, token);

        try {
            emailService.sendEmailForRegConfirm(user, token);
        } catch (MessagingException e) {
            log.error("Error with sending registration confirm for {}", user);
        }
    }

    public ResponseEntity<ApiResponse> confirmRegistration(String token) {

        log.debug("Confirming user");

        String result = userService.validateVerificationToken(token);

        if (Objects.equals(result, TOKEN_INVALID) || Objects.equals(result, TOKEN_EXPIRED)) {
            return ResponseEntities.VERIFICATION_TOKEN_EXPIRED;
        }

        return ResponseEntities.REGISTRATION_CONFIRMED_SUCCESSFULLY;
    }

    public ResponseEntity<ApiResponse> recoverPassword(String email) {

        log.debug("Recovering password for {}", email);

        String newPassword = userService.resetPassword(email, passwordEncoder);

        try {
            emailService.sendEmailForPassRecovery(email, newPassword);
        } catch (MessagingException e) {
            log.error("Error with sending the message for {}", email);
        }

        return ResponseEntities.PASSWORD_RECOVERED_SUCCESSFULLY;
    }
}
