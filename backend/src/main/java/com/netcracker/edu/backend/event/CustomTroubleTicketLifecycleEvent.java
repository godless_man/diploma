package com.netcracker.edu.backend.event;

import com.netcracker.edu.backend.model.TroubleTicket;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CustomTroubleTicketLifecycleEvent extends CustomLifecycleEvent<TroubleTicket> {
    //Created cause of incompatibles of generics and listener methods argument

    public CustomTroubleTicketLifecycleEvent(long subjectId, TroubleTicket object, long oldStatusId, long newStatusId, long recipientId) {
        super(subjectId, object, oldStatusId, newStatusId, recipientId);
    }

    public CustomTroubleTicketLifecycleEvent(long subjectId, TroubleTicket object, long oldStatusId, long newStatusId) {
        super(subjectId, object, oldStatusId, newStatusId);
    }
}
