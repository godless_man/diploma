package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.Order;
import com.netcracker.edu.backend.model.OrderDetails;
import com.netcracker.edu.backend.model.Service;

import java.util.List;
import java.util.Optional;

public interface OrderDao extends GenericDao<Order> {

    List<Order> getOrdersWithPriceToday();

    List<Order> getOrdersWithPriceWeek();

    List<Order> getOrdersWithPriceMonth();

    List<Order> getOrdersOfUser(long id);

    List<OrderDetails> getDetailsOfOrder(long id);

    Optional<Long> getSalesByTrips(long id);

    Optional<Long> getSalesByServices(long id);

    Optional<Long> getSalesByWeek(long id);

    List<Service> getOrderByUserIdAndTypeId(Long userId, int i);

    void addToBasket(long serviceId, Long userId);

    void deleteFromBasket(long serviceId, Long userId);

    void completeOrder(Long userId);

    List<Long> getServicesId(Long userId);
}
