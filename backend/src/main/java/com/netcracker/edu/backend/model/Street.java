package com.netcracker.edu.backend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Street extends Identified {

    private String name;

    private long cityId;
    private City city;

    public Street(String name, long cityId) {
        this.name = name;
        this.cityId = cityId;
    }
}