package com.netcracker.edu.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Country extends Identified {

    private String name;
    private long coordinateId;

    public Country(String name) {
        this.name = name;
    }

    public Country(int id, String name) {
        this.name = name;
        this.id = id;
    }
}
