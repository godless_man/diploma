package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.User;

import java.util.List;
import java.util.Optional;

public interface UserDao extends GenericDao<User> {

    List<User> getAllDetailed();

    Optional<User> getDetailedUserById(long id);

    Optional<User> getDetailedUserByUsername(String username);

    Optional<User> getDetailedUserByEmail(String email);

    Optional<User> getDetailedUserByUsernameOrEmail(String usernameOrEmail);

    List<User> getDetailedUserByUserRole(String role);

    Optional<User> getUserByUsernameOrEmail(String usernameOrEmail);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    Long countUserTripPurchases(long userId, long serviceId);

    void insertSubscription(Long userId, long providerId);

    void deleteByUserId(Long id);

    boolean isSubscribed(Long userId, long providerId);

    List<Long> getSubscriptions(Long id);

    List<Long> getSubscribed(Long providerId);

    Long getSubscriptionCountForProvider(long providerId);
}
