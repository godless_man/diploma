package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.dto.request.ChangePasswordRequest;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.UserDetailsService;
import com.netcracker.edu.backend.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/account/summary")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class ProfileController {

    private UserService userService;
    private UserDetailsService userDetailsService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public ProfileController(UserService userService, UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    @ApiOperation(value = "loadUserAndDetailsForSummary", notes = "Loads summary for current user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "User with exact id not found")
    })
    @GetMapping
    public ResponseEntity<?> loadUserAndDetailsForSummary(@CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting user with details");

        User user = userService.getDetailedUserById(userPrincipal.getId());
        return ResponseEntity.ok(user);
    }

    @ApiOperation(value = "loadUserAndDetailsForSummaryById", notes = "Loads summary by user id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "User with exact id not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> loadUserAndDetailsForSummaryById(@PathVariable long id) {
        log.debug("Requesting user with details by id");

        User user = userService.getDetailedUserById(id);
        return ResponseEntity.ok(user);
    }

    @ApiOperation(value = "editUserProfile", notes = "Takes user with details object and updates exact objects in DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PutMapping("/edit")
    public ResponseEntity<?> editUserProfile(@Valid @RequestBody User user) {
        log.debug("Requesting user profile editing");

        userService.updateUser(user);
        userDetailsService.updateUserDetails(user.getDetails());
        return ResponseEntity.ok(userService.getDetailedUserById(user.getId()));
    }

    @ApiOperation(value = "changePasswordProfile", notes = "Takes ChangePasswordRequest object and updates exact user in DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PutMapping("/changePassword")
    public ResponseEntity<?> changePassword(@Valid @RequestBody ChangePasswordRequest request, @CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting user password changing");

        User user = request.getUser();

        if (passwordEncoder.matches(request.getOldPassword(), userPrincipal.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userService.updateUser(user);
        } else {
            log.error("Old password is not valid for user with id: {}", userPrincipal.getId());
            throw new AppException("Old password is not valid");
        }

        return ResponseEntity.ok(userService.getDetailedUserById(user.getId()));
    }
}
