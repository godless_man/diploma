package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Feedback;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.FeedbackColumns;

public class FeedbackMapper implements RowMapper<Feedback> {

    @Override
    public Feedback mapRow(ResultSet resultSet, int i) throws SQLException {
        Feedback feedback = new Feedback();

        feedback.setId(resultSet.getLong(FeedbackColumns.FEEDBACK_ID));
        feedback.setUserId(resultSet.getLong(FeedbackColumns.USER_ID));
        feedback.setServiceId(resultSet.getLong(FeedbackColumns.FEEDBACK_ID));
        feedback.setRating(resultSet.getLong(FeedbackColumns.RATING));
        feedback.setFeedbackMessage(resultSet.getString(FeedbackColumns.FEEDBACK_MESSAGE));
        feedback.setFeedbackDate(resultSet.getTimestamp(FeedbackColumns.FEEDBACK_DATE));

        return feedback;
    }
}