package com.netcracker.edu.backend.enums;

import lombok.Getter;

@Getter
public enum DiscountType {

    PERCENTAGE(1, "Percentage"),
    FIXED(2, "Fixed discount");

    private final long id;
    private final String discountTypeName;

    DiscountType(long id, String discountTypeName) {
        this.id = id;
        this.discountTypeName = discountTypeName;
    }

    public static String getNameFromId(long id) {

        for (DiscountType discountType : values()) {
            if (discountType.getId() == id) {
                return discountType.getDiscountTypeName();
            }
        }

        return "UNKNOWN";
    }


    public static long getIdFromName(String name) {

        for (DiscountType discountType : values()) {
            if (discountType.getDiscountTypeName().equals(name)) {
                return discountType.getId();
            }
        }

        return 0;
    }
}
