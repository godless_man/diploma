package com.netcracker.edu.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessage extends Identified {

    private long chatId;
    private long userId;
    private String message;
    private Timestamp messageTime;

    private String username;
    private long ownerId;
    private String email;
    private String role;

    public ChatMessage(long chatId, long userId, String message, Timestamp messageTime) {
        this.chatId = chatId;
        this.userId = userId;
        this.message = message;
        this.messageTime = messageTime;
    }

    public ChatMessage(long chatId, long userId, String message, Timestamp messageTime, String username) {
        this.chatId = chatId;
        this.userId = userId;
        this.message = message;
        this.messageTime = messageTime;
        this.username = username;
    }
}
