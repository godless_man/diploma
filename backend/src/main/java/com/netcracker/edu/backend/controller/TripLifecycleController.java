package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.dto.request.ChangeStatusRequest;
import com.netcracker.edu.backend.dto.request.ServiceMessageRequest;
import com.netcracker.edu.backend.model.Service;
import com.netcracker.edu.backend.model.ServiceMessage;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.ServiceMessageService;
import com.netcracker.edu.backend.service.TripService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.netcracker.edu.backend.utils.Constants.RoleName;

@RestController
@RequestMapping("/api/trip/spec")
@CrossOrigin("http://localhost:4200")
@Transactional
@Slf4j
public class TripLifecycleController {

    private ServiceMessageService serviceMessageService;
    private TripService tripService;

    @Autowired
    public TripLifecycleController(ServiceMessageService serviceMessageService, TripService tripService) {
        this.serviceMessageService = serviceMessageService;
        this.tripService = tripService;
    }

    @ApiOperation(value = "loadServiceMessageByServiceId", notes = "Loads service message by service id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/{id}")
    public ResponseEntity<ServiceMessage> loadServiceMessageByServiceId(@PathVariable long id) {
        log.debug("Requesting loading serviceMessage");

        return ResponseEntity.ok(serviceMessageService.getServiceMessageByServiceId(id));
    }

    @ApiOperation(value = "changeStatusByCarrier", notes = "Takes ChangeStatusRequest and changing lifecycle step of exact trip and services")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 400, message = "Got empty service")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @PutMapping("/carrier")
    public ResponseEntity<Service> changeStatusByCarrier(@Valid @RequestBody ChangeStatusRequest request) {
        log.debug("Requesting status changing by carrier with: {}", request);

        return ResponseEntity.ok(tripService.changeLifecycleStep(request, 0L));
    }

    @ApiOperation(value = "changeStatusByApprover", notes = "Takes ChangeStatusRequest and changing lifecycle step of exact trip and services")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 400, message = "Got empty service")
    })
    @Secured(RoleName.ROLE_APPROVER)
    @PutMapping("/approver")
    public ResponseEntity<Service> changeStatusByApprover(@CurrentUser UserPrincipal user, @Valid @RequestBody ChangeStatusRequest request) {
        log.debug("Requesting status changing by approver with: {}", request);

        return ResponseEntity.ok(tripService.changeLifecycleStep(request, user.getId()));
    }

    @ApiOperation(value = "postClarificationRequest",
            notes = "Takes ServiceMessageRequest and posting clarification request")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_APPROVER)
    @PostMapping
    public ResponseEntity<ServiceMessage> postClarificationRequest(@Valid @RequestBody ServiceMessageRequest request) {
        log.debug("Requesting status changing by approver with: {}", request);

        return ResponseEntity.ok(serviceMessageService.postUnderClarification(request));
    }
}
