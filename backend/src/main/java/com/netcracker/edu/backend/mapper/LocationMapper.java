package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Location;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.LocationColumns;

public class LocationMapper implements RowMapper<Location> {

    @Override
    public Location mapRow(ResultSet resultSet, int i) throws SQLException {
        Location location = new Location();

        location.setId(resultSet.getLong(LocationColumns.LOCATION_ID));
        location.setCountryId(resultSet.getLong(LocationColumns.COUNTRY_ID));
        location.setCityId(resultSet.getLong(LocationColumns.CITY_ID));
        location.setStreetId(resultSet.getLong(LocationColumns.STREET_ID));

        return location;
    }
}