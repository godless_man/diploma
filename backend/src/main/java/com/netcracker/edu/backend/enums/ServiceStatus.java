package com.netcracker.edu.backend.enums;

import lombok.Getter;

@Getter
public enum ServiceStatus {

    DRAFT(1, "Draft"),
    OPEN(2, "Open"),
    ASSIGNED(3, "Assigned"),
    PUBLISHED(4, "Published"),
    UNDER_CLARIFICATION(5, "Under clarification"),
    REMOVED(6, "Removed"),
    ARCHIVED(7, "Archived");

    private final long id;
    private final String serviceStatusName;

    ServiceStatus(long id, String serviceStatusName) {
        this.id = id;
        this.serviceStatusName = serviceStatusName;
    }

    public static String getNameFromId(long id) {

        for (ServiceStatus serviceStatus : values()) {
            if (serviceStatus.getId() == id) {
                return serviceStatus.getServiceStatusName();
            }
        }

        return "UNKNOWN";
    }

    public static long getIdFromName(String name) {

        for (ServiceStatus serviceStatus : values()) {
            if (serviceStatus.getServiceStatusName().equals(name)) {
                return serviceStatus.getId();
            }
        }

        return 0;
    }
}
