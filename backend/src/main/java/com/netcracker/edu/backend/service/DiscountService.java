package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.DiscountDao;
import com.netcracker.edu.backend.enums.DiscountType;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.Discount;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
@Slf4j
public class DiscountService {

    private DiscountDao discountDao;

    @Autowired
    public DiscountService(DiscountDao discountDao) {
        this.discountDao = discountDao;
    }

    public Discount getDiscountById(long id) {
        log.debug("Getting discount");

        return discountDao.getById(id)
                .orElseThrow(() -> {
                    log.error("Discount not found with id: {}", id);
                    return new AppException("Discount not found");
                });
    }

    public Discount createDiscount(Discount discount) {
        log.debug("Creating discount");

        if (discount.getTypeId() == 0 && discount.getType() != null) {
            mapDiscountType(discount);
        }

        return discountDao.insert(discount);
    }

    public void updateDiscount(Discount discount) {
        log.debug("Updating discount");


        if (discount.getTypeId() == 0 && discount.getType() != null) {
            mapDiscountType(discount);
        }

        discountDao.update(discount);
    }

    public void deleteDiscountById(long id) {
        log.debug("Deleting discount");

        discountDao.deleteById(id);
    }

    public void deleteDiscount(Discount discount) {
        log.debug("Deleting discount");

        discountDao.delete(discount);
    }

    public List<Discount> getAllDiscounts() {
        log.debug("Getting discounts");

        return discountDao.getAll();
    }

    public List<Discount> getDiscountsOfType(String type) {
        log.debug("Getting discounts");

        return discountDao.getDiscountsOfType(type);
    }

    public List<Discount> getDiscountsByServiceId(long serviceId) {
        log.debug("Getting discounts");

        return discountDao.getDiscountsByServiceId(serviceId);
    }

    public List<Discount> getDiscountsForPeriod(Timestamp from, Timestamp to) {
        log.debug("Getting discounts");

        return discountDao.getDiscountsForPeriod(from, to);
    }

    public List<Discount> getDiscountsOfTypeDetailed(String type) {
        log.debug("Getting discounts");

        return discountDao.getDiscountsOfTypeDetailed(type);
    }

    public List<Discount> getDiscountsByServiceIdDetailed(long serviceId) {
        log.debug("Getting discounts");

        return discountDao.getDiscountsByServiceIdDetailed(serviceId);
    }

    public List<Discount> getDiscountsForPeriodDetailed(Timestamp from, Timestamp to) {
        log.debug("Getting discounts");

        return discountDao.getDiscountsForPeriodDetailed(from, to);
    }

    public void mapDiscountType(Discount discount) {
        discount.setTypeId(DiscountType.getIdFromName(discount.getType()));
    }

    public List<Discount> getTripRelatedDiscountsByTripId(long tripId) {
        log.debug("Getting discounts for trip(id = {})", tripId);

        return discountDao.getTripRelatedDiscountsByTripId(tripId);
    }
}
