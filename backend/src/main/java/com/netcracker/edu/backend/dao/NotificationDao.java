package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.Notification;

import java.util.List;
import java.util.Optional;

public interface NotificationDao extends GenericDao<Notification> {

    Optional<Notification> getDetailedOfServiceById(long id);

    Optional<Notification> getDetailedOfTroubleTicketById(long id);

    Optional<Notification> getDetailedOfServiceByServiceId(long serviceId);

    Optional<Notification> getDetailedOfTroubleTicketByTroubleTicketId(long troubleTicketId);

    List<Notification> getAllDetailedOfServiceByUserId(long userId);

    List<Notification> getAllDetailedOfTroubleTicketByUserId(long userId);

    List<Notification> getAllDetailedOfServiceByUserIdNotRead(long userId);

    List<Notification> getAllDetailedOfServiceForProviderNotRead(long userId);

    List<Notification> getAllDetailedOfServiceForApproverNotRead(long userId);

    List<Notification> getAllDetailedOfTroubleTicketByUserIdNotRead(long userId);

    List<Notification> getAllDetailedOfTroubleTicketForUserNotRead(long userId);

    List<Notification> getAllDetailedOfTroubleTicketForApproverNotRead(long userId);

    int updateIfExistsByService(Notification notification);

    int updateIfExistsByTroubleTicket(Notification notification);

    void setReadTrueForAll();
}
