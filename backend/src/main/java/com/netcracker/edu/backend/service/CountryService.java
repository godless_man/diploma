package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.CountryDao;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.Country;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class CountryService {

    @Autowired
    private CountryDao countryDao;

    public Country getCountryByName(String name) {
        log.debug("Getting country");

        return countryDao.getCountryByName(name)
                .orElseThrow(() -> {
                    log.error("Country not found for name: {}", name);
                    return new AppException("Country not found for name: " + name);
                });
    }

    public List<Country> getAll() {
        return countryDao.getAll();
    }

    public Country getById(long id) {
        return countryDao.getById(id)
                .orElseThrow(() -> {
                    log.error("Country not found for id: {}", id);
                    return new AppException("Country not found for id: " + id);
                });
    }
}

