package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.TroubleTicketDao;
import com.netcracker.edu.backend.mapper.LongMapper;
import com.netcracker.edu.backend.mapper.TroubleTicketMapper;
import com.netcracker.edu.backend.model.TroubleTicket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class TroubleTicketDaoImpl extends GenericDaoImpl<TroubleTicket> implements TroubleTicketDao {

    private static final RowMapper<Long> longMapper = new LongMapper();

    @Value("${db.query.troubleTicket.insert}")
    private String sqlTroubleTicketInsert;

    @Value("${db.query.troubleTicket.update}")
    private String sqlTroubleTicketUpdate;

    @Value("${db.query.troubleTicket.getDetailedById}")
    private String sqlGetTroubleTicketDetailedById;

    @Value("${db.query.troubleTicket.getAllByUserId}")
    private String sqlGetTroubleTicketsByUserId;

    @Value("${db.query.troubleTicket.getAllByServiceIdAndUserId}")
    private String sqlGetTroubleTicketsByServiceIdAndUserId;

    @Value("${db.query.troubleTicket.getAllByApproverIdOrNotAssigned}")
    private String sqlGetTroubleTicketsByApproverIdOrNotAssigned;

    @Value("${db.query.troubleTicket.getAmountOfUnsolved}")
    private String sqlGetTroubleTicketsUnsolvedAmount;

    @Value("${db.query.troubleTicket.getAmountOfSolved}")
    private String sqlGetTroubleTicketsSolvedAmount;

    public TroubleTicketDaoImpl() {
        super(new TroubleTicketMapper(), TableName.TROUBLE_TICKET);
    }

    @Override
    protected String getInsertSql() {
        return sqlTroubleTicketInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, TroubleTicket entity) throws SQLException {
        int argNum = 1;

        statement.setLong(argNum++, entity.getServiceId());
        statement.setLong(argNum++, entity.getUserId());

        Long approverId = entity.getApproverId();
        if (approverId == null) {
            statement.setNull(argNum++, Types.NULL);
        } else {
            statement.setLong(argNum++, approverId);
        }

        statement.setLong(argNum++, entity.getStatusId());
        statement.setString(argNum++, entity.getHeadline());
        statement.setTimestamp(argNum++, entity.getApplyTime());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlTroubleTicketUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(TroubleTicket entity) {
        return new Object[]{entity.getServiceId(), entity.getUserId(), entity.getApproverId(), entity.getStatusId(), entity.getHeadline(), entity.getApplyTime()};
    }

    @Override
    public Optional<TroubleTicket> getTroubleTicketDetailedById(long id) {
        log.debug("Getting trouble ticket by id: {}", id);

        List<TroubleTicket> troubleTickets = jdbcTemplate.query(sqlGetTroubleTicketDetailedById, new Object[]{id}, rowMapper);

        log.debug("Found trouble tickets with ids: {}", troubleTickets.stream().map(TroubleTicket::getId).collect(Collectors.toList()));

        return getSingleElement(troubleTickets);
    }

    @Override
    public List<TroubleTicket> getTroubleTicketsByUserId(long id) {
        log.debug("Getting trouble tickets by userId: {}", id);

        List<TroubleTicket> troubleTickets = jdbcTemplate.query(sqlGetTroubleTicketsByUserId, new Object[]{id}, rowMapper);

        log.debug("Found trouble tickets with ids: {}", troubleTickets.stream().map(TroubleTicket::getId).collect(Collectors.toList()));

        return troubleTickets;
    }

    @Override
    public List<TroubleTicket> getTroubleTicketsByServiceIdAndUserId(long serviceId, long userId) {
        log.debug("Getting trouble tickets by serviceId: {}, for user with id - {}", serviceId, userId);

        List<TroubleTicket> troubleTickets = jdbcTemplate.query(sqlGetTroubleTicketsByServiceIdAndUserId, new Object[]{serviceId, userId}, rowMapper);

        log.debug("Found trouble tickets with ids: {}", troubleTickets.stream().map(TroubleTicket::getId).collect(Collectors.toList()));

        return troubleTickets;
    }

    @Override
    public List<TroubleTicket> getTroubleTicketsByApproverIdOrNotAssigned(long id) {
        log.debug("Getting trouble tickets by approverId: {}", id);

        List<TroubleTicket> troubleTickets = jdbcTemplate.query(sqlGetTroubleTicketsByApproverIdOrNotAssigned, new Object[]{id}, rowMapper);

        log.debug("Found trouble tickets with ids: {}", troubleTickets.stream().map(TroubleTicket::getId).collect(Collectors.toList()));

        return troubleTickets;
    }

    @Override
    public Optional<Long> countUnsolvedTroubleTickets() {
        log.debug("Getting unsolved trouble tickets amount");

        List<Long> count = jdbcTemplate.query(sqlGetTroubleTicketsUnsolvedAmount, longMapper);

        return getCountElement(count);
    }

    @Override
    public Optional<Long> countSolvedTroubleTickets() {
        log.debug("Getting solved trouble tickets amount");

        List<Long> count = jdbcTemplate.query(sqlGetTroubleTicketsSolvedAmount, longMapper);

        return getCountElement(count);
    }
}
