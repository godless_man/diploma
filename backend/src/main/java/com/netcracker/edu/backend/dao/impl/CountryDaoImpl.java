package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.CountryDao;
import com.netcracker.edu.backend.mapper.CountryMapper;
import com.netcracker.edu.backend.model.Country;
import com.netcracker.edu.backend.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class CountryDaoImpl extends GenericDaoImpl<Country> implements CountryDao {

    @Value("${db.query.country.insert}")
    private String sqlCountryInsert;

    @Value("${db.query.country.update}")
    private String sqlCountryUpdate;

    @Value("${db.query.country.getByName}")
    private String sqlGetCountryByName;

    public CountryDaoImpl() {
        super(new CountryMapper(), Constants.TableName.COUNTRY);
    }

    @Override
    protected String getInsertSql() {
        return sqlCountryInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Country entity) throws SQLException {
        int argNum = 1;
        statement.setString(argNum++, entity.getName());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlCountryUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(Country entity) {
        return new Object[]{entity.getName()};
    }

    @Override
    public Optional<Country> getCountryByName(String name) {
        log.debug("Getting country by name: {}", name);

        List<Country> country = jdbcTemplate.query(sqlGetCountryByName, new Object[]{name}, rowMapper);

        log.debug("Found countries (by name) with ids {}", country.stream().map(Country::getName).collect(Collectors.toList()));

        return getSingleElement(country);
    }
}

