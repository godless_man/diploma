package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.OrderDao;
import com.netcracker.edu.backend.dao.ServiceDao;
import com.netcracker.edu.backend.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@Slf4j
public class OrderService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private ServiceDao serviceDao;

    public List<Order> getOrdersWithPriceToday() {
        log.debug("Getting orders");

        return orderDao.getOrdersWithPriceToday();
    }

    public List<Order> getOrdersWithPriceWeek() {
        log.debug("Getting orders");

        return orderDao.getOrdersWithPriceWeek();
    }

    public List<Order> getOrdersWithPriceMonth() {
        log.debug("Getting orders");

        return orderDao.getOrdersWithPriceMonth();
    }

    public List<Order> getOrdersOfUser(long id) {
        log.debug("Getting orders of user");

        List<Order> orders = orderDao.getOrdersOfUser(id);

        for (Order order : orders) {
            order.setOrderDetails(orderDao.getDetailsOfOrder(order.getId()));
        }

        return orders;
    }

    public Long getSalesByTrips(long id) {
        log.debug("Getting sales");

        return orderDao.getSalesByTrips(id).orElse(0L);
    }

    public Long getSalesByServices(long id) {
        log.debug("Getting sales");

        return orderDao.getSalesByServices(id).orElse(0L);
    }

    public Long getSalesByWeek(long id) {
        log.debug("Getting sales");

        return orderDao.getSalesByWeek(id).orElse(0L);
    }

    public List<com.netcracker.edu.backend.model.Service> getBasketByUserId(Long userId) {
        log.debug("Getting basket");

        List<Long> servicesId = orderDao.getServicesId(userId);
        List<com.netcracker.edu.backend.model.Service> services = new ArrayList<>();
        for (Long sId :
                servicesId) {
            services.add(serviceDao.getTripsByIdDetailed(sId).orElse(null));
        }
        return services;
    }

    public void addToBasket(long serviceId, Long userId) {
        log.debug("Adding service to basket");

        orderDao.addToBasket(serviceId, userId);
    }

    public void deleteFromBasket(long serviceId, Long userId) {
        log.debug("Deleting service from basket");

        orderDao.deleteFromBasket(serviceId, userId);
    }

    public void completeOrder(Long userId) {
        log.debug("Completing order");

        orderDao.completeOrder(userId);
    }
}
