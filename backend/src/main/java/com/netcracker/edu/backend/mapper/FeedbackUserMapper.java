package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.enums.Authority;
import com.netcracker.edu.backend.model.Feedback;
import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.model.UserDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.*;

public class FeedbackUserMapper implements RowMapper<Feedback> {

    @Override
    public Feedback mapRow(ResultSet resultSet, int i) throws SQLException {
        Feedback feedback = new Feedback();

        feedback.setId(resultSet.getLong(FeedbackColumns.FEEDBACK_ID));
        feedback.setUserId(resultSet.getLong(FeedbackColumns.USER_ID));
        feedback.setServiceId(resultSet.getLong(FeedbackColumns.FEEDBACK_ID));
        feedback.setRating(resultSet.getLong(FeedbackColumns.RATING));
        feedback.setFeedbackMessage(resultSet.getString(FeedbackColumns.FEEDBACK_MESSAGE));
        feedback.setFeedbackDate(resultSet.getTimestamp(FeedbackColumns.FEEDBACK_DATE));

        User user = new User();

        user.setId(feedback.getUserId());
        user.setAuthorityId(resultSet.getLong(UserColumns.AUTHORITY_ID));
        user.setUsername(resultSet.getString(UserColumns.USERNAME));
        user.setEmail(resultSet.getString(UserColumns.EMAIL));
        user.setPassword(resultSet.getString(UserColumns.PASSWORD));
        user.setActive(resultSet.getBoolean(UserColumns.IS_ACTIVE));

        user.setAuthority(Authority.getNameFromId(user.getAuthorityId()));

        UserDetails userDetails = new UserDetails();

        userDetails.setId(user.getId());
        userDetails.setFirstName(resultSet.getString(UserDetailsColumns.FIRST_NAME));
        userDetails.setLastName(resultSet.getString(UserDetailsColumns.LAST_NAME));
        userDetails.setRegistrationDate(resultSet.getTimestamp(UserDetailsColumns.REGISTRATION_DATE));
        userDetails.setLocationId(resultSet.getLong(UserDetailsColumns.LOCATION_ID));
        userDetails.setImageSrc(resultSet.getString(UserDetailsColumns.IMAGE_SRC));

        user.setDetails(userDetails);

        feedback.setUser(user);

        return feedback;
    }
}