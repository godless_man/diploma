package com.netcracker.edu.backend.dto.request;

import com.netcracker.edu.backend.model.Service;
import lombok.*;

import javax.validation.constraints.NotBlank;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ServiceMessageRequest {

    private Service service;

    @NotBlank
    private String message;
}
