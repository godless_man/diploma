package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ServiceMessageDao;
import com.netcracker.edu.backend.dto.request.ServiceMessageRequest;
import com.netcracker.edu.backend.enums.ServiceStatus;
import com.netcracker.edu.backend.event.publisher.LifecycleEventPublisher;
import com.netcracker.edu.backend.model.ServiceMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;

@Service
@Transactional
@Slf4j
public class ServiceMessageService {

    private ServiceMessageDao serviceMessageDao;
    private TripService tripService;
    private LifecycleEventPublisher publisher;

    @Autowired
    public ServiceMessageService(ServiceMessageDao serviceMessageDao, TripService tripService, LifecycleEventPublisher publisher) {
        this.serviceMessageDao = serviceMessageDao;
        this.tripService = tripService;
        this.publisher = publisher;
    }

    public ServiceMessage createServiceMessage(ServiceMessage serviceMessage) {
        log.debug("Creating serviceMessage");

        return serviceMessageDao.insert(serviceMessage);
    }

    public ServiceMessage getServiceMessageByServiceId(long id) {
        log.debug("Getting serviceMessage");

        return serviceMessageDao.getServiceMessageByServiceId(id).orElse(new ServiceMessage());
    }

    public ServiceMessage postUnderClarification(ServiceMessageRequest request) {
        log.debug("Posting clarification request");

        long oldStatusId = request.getService().getStatusId();
        com.netcracker.edu.backend.model.Service result = tripService
                .changeStatusOfAllServices(ServiceStatus.UNDER_CLARIFICATION.getId(), request.getService());

        publisher.publishLifecycleEventNotification(result.getApproverId(), result, result.getStatusId(), oldStatusId, result.getProviderId());

        return createServiceMessage(new ServiceMessage(result.getId(), Timestamp.from(Instant.now()), request.getMessage()));
    }
}
