package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.CityDao;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.City;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class CityService {

    @Autowired
    private CityDao cityDao;

    public City getCityByName(String name, long countryId) {
        log.debug("Getting city");

        return cityDao.getCityByName(name, countryId)
                .orElseThrow(() -> {
                    log.error("City not found for name: {}", name);
                    return new AppException("City not found for name: " + name);
                });
    }

    public City getById(long id) {
        return cityDao.getById(id)
                .orElseThrow(() -> {
                    log.error("City not found id name: {}", id);
                    return new AppException("City not found for id: " + id);
                });
    }

    public List<City> getCitiesByCountryId(long id) {
        log.debug("Getting cites by country with id = {}", id);

        return cityDao.getCitiesByCountryId(id);
    }
}
