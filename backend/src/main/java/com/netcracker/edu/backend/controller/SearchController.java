package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.dto.request.SearchTripMultipleRequest;
import com.netcracker.edu.backend.dto.request.SearchTripRequest;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.CountryService;
import com.netcracker.edu.backend.service.TripService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/account/search")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class SearchController {

    @Autowired
    private CountryService countryService;

    @Autowired
    private TripService tripService;
    private String requesting_trips_by_discount;

    @ApiOperation(value = "getAllCountry", notes = "Loads all countries")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/country")
    public ResponseEntity<?> getAllCountry() {
        log.debug("Requesting all countries");

        return ResponseEntity.ok(countryService.getAll());
    }

    @ApiOperation(value = "getSearchedByRating", notes = "Loads searched by rating")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @PostMapping("/rating")
    public ResponseEntity<?> getSearchedByRating(@Valid @RequestBody SearchTripRequest searchTripRequest) {
        log.debug("Requesting trips by rating");

        return ResponseEntity.ok(tripService.searchByRating(searchTripRequest));
    }

    @ApiOperation(value = "getSearchedByDiscount", notes = "Loads searched by discount")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @PostMapping("/provider")
    public ResponseEntity<?> getSearchedByDiscount(@Valid @RequestBody SearchTripRequest searchTripRequest, @CurrentUser UserPrincipal currentUser) {
        log.debug(requesting_trips_by_discount);

        return ResponseEntity.ok(tripService.optimalSearch(searchTripRequest, currentUser.getId()));
    }

    @ApiOperation(value = "getSearchedByProvider", notes = "Loads searched by provider")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @PostMapping("/discount")
    public ResponseEntity<?> getSearchedByProvider(@Valid @RequestBody SearchTripRequest searchTripRequest) {
        log.debug("Requesting trips by provider");

        return ResponseEntity.ok(tripService.searchByDiscount(searchTripRequest));
    }

    @ApiOperation(value = "getSpecialSearchedByPrice", notes = "Loads searched by price")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @PostMapping("/price")
    public ResponseEntity<?> getSpecialSearchedByPrice(@Valid @RequestBody SearchTripMultipleRequest searchTripMultipleRequest) {
        log.debug("Requesting trips by price");

        return ResponseEntity.ok(tripService.searchedByPrice(searchTripMultipleRequest));
    }

    @ApiOperation(value = "getSpecialSearchedByLength", notes = "Loads searched by length")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @PostMapping("/length")
    public ResponseEntity<?> getSpecialSearchedByLength(@Valid @RequestBody SearchTripMultipleRequest searchTripMultipleRequest) {
        log.debug("{}", searchTripMultipleRequest.getDestinationCountry().length);
        log.debug("Requesting by length");
        return ResponseEntity.ok(tripService.searchedByLength(searchTripMultipleRequest));
    }

    @ApiOperation(value = "getSpecialSearchedByBundle", notes = "Loads searched by bundle")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @PostMapping("/bundle")
    public ResponseEntity<?> getSpecialSearchedByBundle(@Valid @RequestBody SearchTripMultipleRequest searchTripMultipleRequest) {
        log.debug("Requesting trips by bundle");
        return ResponseEntity.ok(tripService.searchedByBundle(searchTripMultipleRequest));
    }
}
