package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.ChatDao;
import com.netcracker.edu.backend.mapper.ChatMapper;
import com.netcracker.edu.backend.model.Chat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class ChatDaoImpl extends GenericDaoImpl<Chat> implements ChatDao {

    @Value("${db.query.chat.insert}")
    private String sqlChatInsert;

    @Value("${db.query.chat.update}")
    private String sqlChatUpdate;

    @Value("${db.query.chat.getByUserId}")
    private String sqlGetChatByUserId;

    @Value("${db.query.chat.getAllForApprover}")
    private String sqlGetAllChatsForApprover;

    public ChatDaoImpl() {
        super(new ChatMapper(), TableName.CHAT);
    }

    @Override
    public Optional<Chat> getChatByUserId(long id) {
        log.debug("Getting chat by user id: {}", id);

        List<Chat> chat = jdbcTemplate.query(sqlGetChatByUserId, new Object[]{id}, rowMapper);

        log.debug("Found chats with ids: {}", chat.stream().map(Chat::getId).collect(Collectors.toList()));

        return getSingleElement(chat);
    }

    @Override
    public List<Chat> getAllChatsForApprover(long id) {
        log.debug("Getting chats for approver with id: {}", id);

        List<Chat> chats = jdbcTemplate.query(sqlGetAllChatsForApprover, new Object[]{id}, rowMapper);

        log.debug("Found chats with ids: {}", chats.stream().map(Chat::getId).collect(Collectors.toList()));

        return chats;
    }

    @Override
    protected String getInsertSql() {
        return sqlChatInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Chat entity) throws SQLException {
        int argNum = 1;

        statement.setLong(argNum++, entity.getUserId());

        Long approverId = entity.getApproverId();
        if (approverId == null) {
            statement.setNull(argNum++, Types.NULL);
        } else {
            statement.setLong(argNum++, approverId);
        }

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlChatUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(Chat entity) {
        return new Object[]{entity.getUserId(), entity.getApproverId()};
    }
}
