package com.netcracker.edu.backend.websockets;

import com.netcracker.edu.backend.model.ChatMessage;
import com.netcracker.edu.backend.model.Message;
import com.netcracker.edu.backend.service.ChatMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
public class WebSocketController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private ChatMessageService chatMessageService;

    @MessageMapping("/globalChat")
    public void globalChatMessaging(@Valid Message message) {
        log.debug("Message in global chat from user with id = " + message.getUserId());
        messagingTemplate.convertAndSend("/messages/globalChat", message);
    }

    @Transactional
    @MessageMapping("/supportChat")
    public void chatMessagesInSupportChat(ChatMessage chatMessage) {
        log.debug("Message in support chat from user with id = " + chatMessage.getOwnerId());

        chatMessageService.createChatMessage(chatMessage);

        messagingTemplate.convertAndSend("/messages/chat", chatMessage);
        messagingTemplate.convertAndSend("/messages/chat/" + chatMessage.getOwnerId(), chatMessage);
    }
}
