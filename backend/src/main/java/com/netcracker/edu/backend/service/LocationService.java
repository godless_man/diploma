package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.model.City;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Slf4j
public class LocationService {

    private CountryService countryService;
    private CityService cityService;
    private CoordinateService coordinateService;

    @Autowired
    public LocationService(CountryService countryService, CityService cityService, CoordinateService coordinateService) {
        this.countryService = countryService;
        this.cityService = cityService;
        this.coordinateService = coordinateService;
    }

    public void mapLocation(City city) {
        log.debug("Mapping location for get id city and country in DB");

        if (city.getName() != null) {
            if (city.getCountry() != null && city.getCountry().getName() != null) {
                city.setCountryId(countryService.getCountryByName(city.getCountry().getName()).getId());
                city.getCountry().setId(city.getCountryId());
                city.setId(cityService.getCityByName(city.getName(), city.getCountryId()).getId());
            }
        } else if (city.getId() != 0 && city.getCountry() != null) {
            city.getCountry().setId(city.getCountryId());
        }

        city.setCoordinate(coordinateService.getCoordinateByCityId(city.getId()));
        city.setCoordinateId(city.getCoordinate().getId());
    }
}
