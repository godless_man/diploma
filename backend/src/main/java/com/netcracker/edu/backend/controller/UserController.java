package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.dto.request.SignUpRequest;
import com.netcracker.edu.backend.service.AuthService;
import com.netcracker.edu.backend.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.netcracker.edu.backend.utils.Constants.RoleName;

@RestController
@RequestMapping("/api/account/users")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class UserController {

    private UserService userService;
    private AuthService authService;

    @Autowired
    public UserController(UserService userService, AuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    @ApiOperation(value = "loadAllUsers", notes = "Loads all users list")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping()
    public ResponseEntity<?> loadAllUsers() {
        log.debug("Requesting all users");

        return ResponseEntity.ok(userService.getAllUsers());
    }

    @ApiOperation(value = "createSpecialUser", notes = "Takes SignUpRequest and registers special user")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Trip created successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_ADMIN)
    @PostMapping
    public ResponseEntity<?> createSpecialUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        log.debug("Requesting special user creating");

        return authService.registerSpecialUser(signUpRequest);
    }

    @ApiOperation(value = "getUser", notes = "Loads user by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "User not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable long id) {
        log.debug("Requesting user");

        return ResponseEntity.ok(userService.getUserById(id));
    }

    @ApiOperation(value = "deleteUser", notes = "Deletes user from DB by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_ADMIN)
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable long id) {
        log.debug("Requesting user deleting");

        userService.deleteUserById(id);
        return ResponseEntity.ok().build();
    }
}
