package com.netcracker.edu.backend.enums;

import lombok.Getter;

@Getter
public enum Authority {

    ROLE_ADMIN(1, "ROLE_ADMIN"),
    ROLE_APPROVER(2, "ROLE_APPROVER"),
    ROLE_PROVIDER(3, "ROLE_PROVIDER"),
    ROLE_USER(4, "ROLE_USER");

    private final long id;
    private final String authorityName;

    Authority(long id, String authorityName) {
        this.id = id;
        this.authorityName = authorityName;
    }

    public static String getNameFromId(long id) {

        for (Authority authority : values()) {
            if (authority.getId() == id) {
                return authority.getAuthorityName();
            }
        }

        return "UNKNOWN";
    }

    public static long getIdFromName(String name) {

        for (Authority authority : values()) {
            if (authority.getAuthorityName().equals(name)) {
                return authority.getId();
            }
        }

        return 0;
    }
}