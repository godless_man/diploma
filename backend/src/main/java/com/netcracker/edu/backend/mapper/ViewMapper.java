package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.View;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.ViewColumns;

public class ViewMapper implements RowMapper<View> {
    @Override
    public View mapRow(ResultSet resultSet, int i) throws SQLException {
        View view = new View();

        view.setId(resultSet.getLong(ViewColumns.VIEW_ID));
        view.setUserId(resultSet.getLong(ViewColumns.USER_ID));
        view.setServiceId(resultSet.getLong(ViewColumns.SERVICE_ID));
        view.setViewDate(resultSet.getTimestamp(ViewColumns.VIEW_DATE));

        return view;
    }
}
