package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.service.CountryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/countries")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class CountryController {

    @Autowired
    private CountryService countryService;

    @ApiOperation(value = "getAllCountries", notes = "Loads all countries")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping
    public ResponseEntity<?> getAllCountries() {
        log.debug("Requesting countries");

        return ResponseEntity.ok(countryService.getAll());
    }
}
