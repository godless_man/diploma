package com.netcracker.edu.backend.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApproverDashboardResponse {

    private WaitingDiagram waitingDiagram;
    private ProductivityDiagram productivityDiagram;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class WaitingDiagram {
        private long quantityOfUnapprovedServices;
        private long quantityOfUnansweredTroubleTickets;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ProductivityDiagram {
        private long quantityOfApprovedServices;
        private long quantityOfAnsweredTroubleTickets;
    }
}
