package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.ServiceMessage;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.*;

public class ServiceMessageMapper implements RowMapper<ServiceMessage> {

    @Override
    public ServiceMessage mapRow(ResultSet resultSet, int i) throws SQLException {
        ServiceMessage serviceMessage = new ServiceMessage();

        serviceMessage.setId(resultSet.getLong(ServiceMessageColumns.SERVICE_MESSAGE_ID));
        serviceMessage.setServiceId(resultSet.getLong(ServiceMessageColumns.SERVICE_ID));
        serviceMessage.setMessageTime(resultSet.getTimestamp(ServiceMessageColumns.MESSAGE_TIME));
        serviceMessage.setMessage(resultSet.getString(ServiceMessageColumns.MESSAGE));

        serviceMessage.setApproverId(resultSet.getLong(ServiceColumns.APPROVER_ID));
        serviceMessage.setApproverUsername(resultSet.getString(UserColumns.USERNAME));

        return serviceMessage;
    }
}
