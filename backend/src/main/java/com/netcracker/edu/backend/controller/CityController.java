package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.service.CityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/cities")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class CityController {

    @Autowired
    private CityService cityService;

    @ApiOperation(value = "getCitiesByCountryName", notes = "Loads cities by country")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("country/{id}")
    public ResponseEntity<?> getCitiesByCountryName(@PathVariable long id) {
        log.debug("Requesting cities by country with id {}", id);

        return ResponseEntity.ok(cityService.getCitiesByCountryId(id));
    }
}
