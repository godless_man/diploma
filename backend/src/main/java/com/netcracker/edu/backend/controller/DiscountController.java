package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.Discount;
import com.netcracker.edu.backend.service.DiscountService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.netcracker.edu.backend.utils.Constants.RoleName;

@RestController
@RequestMapping("/api/discounts")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class DiscountController {

    @Autowired
    private DiscountService discountService;

    @ApiOperation(value = "loadAllDiscounts", notes = "Loads all discounts list")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping
    public ResponseEntity<?> loadAllDiscounts() {
        return ResponseEntity.ok(discountService.getAllDiscounts());
    }

    @ApiOperation(value = "getDiscount", notes = "Takes discount id and loads object from DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Discount with exact id not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> getDiscount(@PathVariable long id) {
        log.debug("Requesting discount");

        return ResponseEntity.ok(discountService.getDiscountById(id));
    }

    @ApiOperation(value = "getTripRelatedDiscounts", notes = "Loads all discounts list related to exact trip")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/trip/{id}")
    public ResponseEntity<?> getTripRelatedDiscounts(@PathVariable long id) {
        log.debug("Requesting discounts for trip(id = {})", id);

        return ResponseEntity.ok(discountService.getTripRelatedDiscountsByTripId(id));
    }

    @ApiOperation(value = "getDetailedDiscounts", notes = "Takes discount id and loads object from DB with details")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Discount with exact id not found")
    })
    @GetMapping("/detailed/{id}")
    public ResponseEntity<?> getDetailedDiscounts(@PathVariable long id) {
        log.debug("Requesting discounts");

        return ResponseEntity.ok(discountService.getDiscountsByServiceIdDetailed(id));
    }

    @ApiOperation(value = "createDiscount", notes = "Takes discount object and inserts it to DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @PostMapping
    public ResponseEntity<?> createDiscount(@Valid @RequestBody Discount discount) {
        log.debug("Requesting discount creating");

        return ResponseEntity.ok(discountService.createDiscount(discount));
    }

    @ApiOperation(value = "updateDiscount", notes = "Takes discount object and updates exact object in DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @PutMapping
    public ResponseEntity<?> updateDiscount(@Valid @RequestBody Discount discount) {
        log.debug("Requesting discount updating");

        discountService.updateDiscount(discount);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "deleteDiscount", notes = "Takes discount id and deletes exact object from DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDiscount(@PathVariable long id) {
        log.debug("Requesting discount deleting");

        discountService.deleteDiscountById(id);
        return ResponseEntity.ok().build();
    }
}
