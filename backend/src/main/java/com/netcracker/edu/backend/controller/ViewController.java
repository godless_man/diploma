package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.View;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.ViewService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.netcracker.edu.backend.utils.Constants.ResponseEntities.VIEW_ADDED_SUCCESSFULLY;

@RestController
@RequestMapping("/api/views")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class ViewController {

    @Autowired
    private ViewService viewService;

    @ApiOperation(value = "addView", notes = "Adds view")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PostMapping
    public ResponseEntity<?> addView(@RequestBody long serviceId, @CurrentUser UserPrincipal currentUser) {
        log.debug("Inserting view(userId = {}, serviceId = {})", currentUser.getId(), serviceId);

        View createdView = viewService.addView(currentUser.getId(), serviceId);

        return VIEW_ADDED_SUCCESSFULLY;
    }

    @ApiOperation(value = "countViewsByTripId", notes = "Counts views for trip")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/trip/{id}")
    public ResponseEntity<?> countViewsByTripId(@PathVariable long id) {
        log.debug("Count views for trip(id = {})", id);

        return ResponseEntity.ok(viewService.countViewsByTripId(id));
    }
}
