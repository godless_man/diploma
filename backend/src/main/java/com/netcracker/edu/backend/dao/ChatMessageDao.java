package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.ChatMessage;

import java.util.List;

public interface ChatMessageDao extends GenericDao<ChatMessage> {

    List<ChatMessage> getAllChatMessagesByChatId(long id);
}
