package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.FeedbackDao;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.Feedback;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class FeedbackService {

    private FeedbackDao feedbackDao;
    private UserService userService;

    @Autowired
    public FeedbackService(FeedbackDao feedbackDao, UserService userService) {
        this.feedbackDao = feedbackDao;
        this.userService = userService;
    }

    public Feedback getFeedbackById(long id) {
        log.debug("Getting feedback(id = {}) in {} ", id, this.getClass().getName());

        return feedbackDao.getById(id)
                .orElseThrow(() -> {
                    log.error("Feedback with not found id: {}", id);
                    return new AppException("Feedback with id " + id + " not found");
                });
    }

    public List<Feedback> getAllFeedback() {
        log.debug("Getting all feedback in {} ", this.getClass().getName());

        return feedbackDao.getAll();
    }

    public void updateFeedback(Feedback feedback) {
        log.debug("Updating feedback(id = {}) in {} ", feedback.getId(), this.getClass().getName());

        feedbackDao.update(feedback);
    }

    public Feedback addFeedback(Feedback feedback) {
        log.debug("Inserting feedback(id = {}) in {} ", feedback.getId(), this.getClass().getName());

        return feedbackDao.insert(feedback);
    }

    public void deleteFeedbackById(long id) {
        log.debug("Deleting feedback(id = {}) in {} ", id, this.getClass().getName());

        feedbackDao.deleteById(id);
    }

    public void deleteFeedback(Feedback feedback) {
        log.debug("Deleting feedback(id = {}) in {} ", feedback.getId(), this.getClass().getName());

        feedbackDao.delete(feedback);
    }

    public boolean canUserGiveFeedback(long userId, long tripId) {
        log.debug("Checking whether user(id = {}) had purchased trip(id = {}) in {} ", userId, tripId, this.getClass().getName());

        long amountOfPurchases = userService.countUserTripPurchases(userId, tripId);
        log.debug("Amount of trip(id = {}) purchases by user(id = {}) in {}: {}", tripId, userId, this.getClass().getName(), amountOfPurchases);

        return (amountOfPurchases != 0);
    }

    public List<Feedback> getTripFeedback(long tripId) {
        log.debug("Getting all feedback for trip(id = {}) in {} ", tripId, this.getClass().getName());

        return feedbackDao.getTripFeedbackWithUsers(tripId);
    }
}
