package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.TroubleTicketDao;
import com.netcracker.edu.backend.dto.request.ChangeStatusRequest;
import com.netcracker.edu.backend.dto.request.NewTroubleTicketWithMessageRequest;
import com.netcracker.edu.backend.enums.TroubleTicketStatus;
import com.netcracker.edu.backend.event.publisher.LifecycleEventPublisher;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.exception.BadRequestException;
import com.netcracker.edu.backend.model.TroubleTicket;
import com.netcracker.edu.backend.model.TroubleTicketMessage;
import com.netcracker.edu.backend.security.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Service
@Slf4j
@Transactional
public class TroubleTicketService {

    private static final String TROUBLE_TICKETS_NEW_TICKET = "/troubleTickets/newTicket";
    private static final String TROUBLE_TICKETS_CHANGE_STATUS_FOR_APPROVER = "/troubleTickets/changeStatusForApprover/";
    private static final String TROUBLE_TICKETS_CHANGE_STATUS_FOR_USER = "/troubleTickets/changeStatusForUser/";

    private TroubleTicketDao troubleTicketDao;
    private TroubleTicketMessageService troubleTicketMessageService;
    private LifecycleEventPublisher publisher;
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    public TroubleTicketService(TroubleTicketDao troubleTicketDao, TroubleTicketMessageService troubleTicketMessageService,
                                LifecycleEventPublisher publisher, SimpMessagingTemplate messagingTemplate) {
        this.troubleTicketDao = troubleTicketDao;
        this.troubleTicketMessageService = troubleTicketMessageService;
        this.publisher = publisher;
        this.messagingTemplate = messagingTemplate;
    }

    public TroubleTicket getTroubleTicketDetailedById(long id) {
        log.debug("Getting troubleTicket");

        return troubleTicketDao.getTroubleTicketDetailedById(id)
                .orElseThrow(() -> {
                    log.error("TroubleTicket not found with id: {}", id);
                    return new AppException("TroubleTicket with id " + id + " not found");
                });
    }

    public TroubleTicket createTroubleTicket(TroubleTicket troubleTicket) {
        log.debug("Creating troubleTicket");

        return troubleTicketDao.insert(troubleTicket);
    }

    public void updateTroubleTicket(TroubleTicket troubleTicket) {
        log.debug("Updating troubleTicket");

        if (troubleTicket.getApproverId() == 0) {
            troubleTicket.setApproverId(null);
        }

        troubleTicketDao.update(troubleTicket);
    }

    public List<TroubleTicket> getTroubleTicketsByUserId(long id) {
        log.debug("Getting troubleTickets");

        return troubleTicketDao.getTroubleTicketsByUserId(id);
    }

    public List<TroubleTicket> getTroubleTicketsByServiceId(long serviceId, long userId) {
        log.debug("Getting troubleTickets");

        return troubleTicketDao.getTroubleTicketsByServiceIdAndUserId(serviceId, userId);
    }

    public List<TroubleTicket> getTroubleTicketsByApproverIdOrNotAssigned(long id) {
        log.debug("Getting troubleTickets");

        return troubleTicketDao.getTroubleTicketsByApproverIdOrNotAssigned(id);
    }

    public long countSolvedTroubleTickets() {
        log.debug("Counting troubleTickets");

        return troubleTicketDao.countSolvedTroubleTickets().orElse(0L);
    }

    public long countUnsolvedTroubleTickets() {
        log.debug("Counting troubleTickets");

        return troubleTicketDao.countUnsolvedTroubleTickets().orElse(0L);
    }

    public TroubleTicket initializeTroubleTicket(NewTroubleTicketWithMessageRequest request, UserPrincipal user) {
        log.debug("Initializing trouble ticket");

        TroubleTicket ticket = createTroubleTicket(new TroubleTicket(request.getServiceId(), user.getId(),
                TroubleTicketStatus.OPEN.getId(), request.getHeadline(), Timestamp.from(Instant.now())));

        troubleTicketMessageService.createTroubleTicketMessage(new TroubleTicketMessage(ticket.getId(),
                user.getId(), request.getMessage(), Timestamp.from(Instant.now())), null, user);

        ticket.setStatus(TroubleTicketStatus.getNameFromId(ticket.getStatusId()));

        publisher.publishLifecycleEventNotification(user.getId(), ticket, ticket.getStatusId(), 0L);
        messagingTemplate.convertAndSend(TROUBLE_TICKETS_NEW_TICKET, ticket);

        return ticket;
    }

    public TroubleTicket changeLifecycleStep(ChangeStatusRequest request, UserPrincipal user) {
        log.debug("Changing lifecycle step of: {}", request.getTroubleTicket());

        TroubleTicket ticket = request.getTroubleTicketOptional()
                .orElseThrow(() -> {
                    log.error("Got empty troubleTicket");
                    return new BadRequestException("Got empty troubleTicket");
                });

        long oldStatudId = ticket.getStatusId();
        long newStatusId = request.getStatusId();

        ticket.setStatusId(newStatusId);
        updateTroubleTicket(ticket);

        ticket.setStatus(TroubleTicketStatus.getNameFromId(newStatusId));

        if (newStatusId == TroubleTicketStatus.ANSWERED.getId() || newStatusId == TroubleTicketStatus.RATED.getId() || newStatusId == TroubleTicketStatus.FEEDBACK.getId()) {
            messagingTemplate.convertAndSend(TROUBLE_TICKETS_CHANGE_STATUS_FOR_APPROVER + ticket.getApproverId(), ticket);
            publisher.publishLifecycleEventNotification(user.getId(), ticket, newStatusId, oldStatudId, ticket.getApproverId());
        } else if (newStatusId == TroubleTicketStatus.REOPENED.getId()) {
            messagingTemplate.convertAndSend(TROUBLE_TICKETS_NEW_TICKET, ticket);
            publisher.publishLifecycleEventNotification(user.getId(), ticket, newStatusId, oldStatudId);
        } else {
            messagingTemplate.convertAndSend(TROUBLE_TICKETS_CHANGE_STATUS_FOR_USER + ticket.getUserId(), ticket);
            publisher.publishLifecycleEventNotification(user.getId(), ticket, newStatusId, oldStatudId, ticket.getUserId());
        }

        return ticket;
    }
}
