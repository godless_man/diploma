package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.TroubleTicketMessageDao;
import com.netcracker.edu.backend.enums.TroubleTicketStatus;
import com.netcracker.edu.backend.model.Notification;
import com.netcracker.edu.backend.model.TroubleTicket;
import com.netcracker.edu.backend.model.TroubleTicketMessage;
import com.netcracker.edu.backend.security.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@Transactional
public class TroubleTicketMessageService {

    private static final String TROUBLE_TICKETS_MESSAGE_FOR_USER = "/troubleTickets/messageForUser/";
    private static final String TROUBLE_TICKETS_MESSAGE_FOR_APPROVER = "/troubleTickets/messageForApprover/";

    private TroubleTicketMessageDao troubleTicketMessageDao;
    private NotificationService notificationService;
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    public TroubleTicketMessageService(TroubleTicketMessageDao troubleTicketMessageDao, NotificationService notificationService, SimpMessagingTemplate messagingTemplate) {
        this.troubleTicketMessageDao = troubleTicketMessageDao;
        this.notificationService = notificationService;
        this.messagingTemplate = messagingTemplate;
    }

    public TroubleTicketMessage createTroubleTicketMessage(TroubleTicketMessage troubleTicketMessage, TroubleTicket ticket, UserPrincipal user) {
        log.debug("Creating troubleTicketMessage");

        troubleTicketMessage = troubleTicketMessageDao.insert(troubleTicketMessage);

        Notification notification = new Notification(troubleTicketMessage.getUserId(), troubleTicketMessage.getTroubleTicketId(), TroubleTicket.class, "MESSAGE");

        if (ticket != null) {
            if (troubleTicketMessage.getUserId() == ticket.getUserId()) {
                notificationService.sendNotificationToUser(notification, ticket.getApproverId());
            } else {
                notificationService.sendNotificationToUser(notification, ticket.getUserId());
            }

            troubleTicketMessage.setUsername(user.getUsername());
            troubleTicketMessage.setEmail(user.getEmail());

            if (ticket.getStatusId() != TroubleTicketStatus.OPEN.getId() && ticket.getStatusId() != TroubleTicketStatus.REOPENED.getId()) {
                messagingTemplate.convertAndSend(TROUBLE_TICKETS_MESSAGE_FOR_USER + ticket.getUserId(), troubleTicketMessage);
                messagingTemplate.convertAndSend(TROUBLE_TICKETS_MESSAGE_FOR_APPROVER + ticket.getApproverId(), troubleTicketMessage);
            }
        }

        return troubleTicketMessage;
    }

    public void updateTroubleTicketMessage(TroubleTicketMessage troubleTicketMessage) {
        log.debug("Updating troubleTicketMessage");

        troubleTicketMessageDao.update(troubleTicketMessage);
    }

    public List<TroubleTicketMessage> getTroubleTicketMessagesByTroubleTicketId(long id) {
        log.debug("Getting troubleTicketMessages");

        return troubleTicketMessageDao.getTroubleTicketMessagesByTroubleTicketId(id);
    }
}
