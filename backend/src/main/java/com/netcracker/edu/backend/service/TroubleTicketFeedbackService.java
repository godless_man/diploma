package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.TroubleTicketFeedbackDao;
import com.netcracker.edu.backend.model.TroubleTicketFeedback;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
public class TroubleTicketFeedbackService {

    @Autowired
    private TroubleTicketFeedbackDao troubleTicketFeedbackDao;

    public TroubleTicketFeedback createTroubleTicketFeedback(TroubleTicketFeedback troubleTicketFeedback) {
        log.debug("Creating troubleTicketFeedback");

        if (troubleTicketFeedback.getRating() == 0) {
            troubleTicketFeedback.setRating(null);
        }

        return troubleTicketFeedbackDao.insert(troubleTicketFeedback);
    }
}
