package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ServiceDao;
import com.netcracker.edu.backend.exception.AppException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.netcracker.edu.backend.utils.Constants.ServiceStatus;
import static com.netcracker.edu.backend.utils.Constants.ServiceType;

@Service
@Transactional
@Slf4j
public class BundleService {

    @Autowired
    private ServiceDao serviceDao;
    private CoordinateService coordinateService;

    @Autowired
    public BundleService(ServiceDao serviceDao, CoordinateService coordinateService) {
        this.serviceDao = serviceDao;
        this.coordinateService = coordinateService;
    }

    public com.netcracker.edu.backend.model.Service getBundleById(long id) {
        return serviceDao.getBundleById(id)
                .orElseThrow(() -> new AppException("Bundle with id " + id + " not found"));
    }

    public com.netcracker.edu.backend.model.Service getBundleByIdDetailed(long id) {
        log.debug("Getting bundle");

        com.netcracker.edu.backend.model.Service bundle = serviceDao.getBundleByIdDetailed(id)
                .orElseThrow(() -> new AppException("Bundle with id " + id + " not found"));

        List<com.netcracker.edu.backend.model.Service> trips = serviceDao.getTripsByBundleId(id);

        bundle.setBundleTrips(trips);

        if (bundle.getLocation() != null) {
            bundle.getLocation().setCoordinate(coordinateService.getLocById(bundle.getId()));
            bundle.getDestination().setCoordinate(coordinateService.getDestById(bundle.getId()));
        }
        return bundle;
    }

    @Transactional
    public com.netcracker.edu.backend.model.Service createBundle(com.netcracker.edu.backend.model.Service bundle) {
        log.debug("Creating bundle");

        bundle.setType(ServiceType.BUNDLE);

        bundle.setLocationId(bundle.getBundleTrips().get(0).getLocationId());
        bundle.setDestinationId(bundle.getBundleTrips().get(bundle.getBundleTrips().size() - 1).getDestinationId());

        serviceDao.insert(bundle);

        for (com.netcracker.edu.backend.model.Service trip : bundle.getBundleTrips()) {
            trip.setLocation(trip.getLocation());
            trip.setDestination(trip.getDestination());
        }

        for (com.netcracker.edu.backend.model.Service trip : bundle.getBundleTrips()) {
            addBundleLink(bundle.getId(), trip.getId());
        }

        return bundle;
    }

    public void updateBundle(com.netcracker.edu.backend.model.Service bundle) {
        log.debug("Updating bundle");

        serviceDao.update(bundle);
    }

    public void deleteBundleById(long id) {
        log.debug("Deleting bundle by id: {}", id);

        serviceDao.deleteBundleLinks(id);
        serviceDao.deleteById(id);
    }

    public void deleteBundle(com.netcracker.edu.backend.model.Service service) {
        log.debug("Deleting bundle");

        serviceDao.delete(service);
    }

    public List<com.netcracker.edu.backend.model.Service> getAllBundles() {
        return serviceDao.getAllBundles();
    }

    public List<com.netcracker.edu.backend.model.Service> getAllBundlesDetailed() {
        return serviceDao.getAllBundlesDetailed();
    }

    public List<com.netcracker.edu.backend.model.Service> getAllBundlesOfStatus(String status) {
        log.debug("Getting bundles with status - {}", status);

        return serviceDao.getAllBundlesOfStatus(status);
    }

    public List<com.netcracker.edu.backend.model.Service> getBundlesByProviderId(long id) {
        log.debug("Getting bundles");

        return serviceDao.getBundlesByProviderId(id);
    }

    public List<com.netcracker.edu.backend.model.Service> getBundlesOfStatusByProviderId(String status, long id) {
        log.debug("Getting bundles");

        return serviceDao.getBundlesOfStatusByProviderId(status, id);
    }

    public long countAllBundles() {
        log.debug("Counting bundles");

        return serviceDao.countAllBundles().orElse(0L);
    }

    public void addBundleLink(long bundleId, long tripId) {
        log.debug("Adding bundle link");

        serviceDao.insertBundleLink(bundleId, tripId);
    }


    public List<com.netcracker.edu.backend.model.Service> getAllPublishedBundlesWithImg() {
        log.debug("Getting bundles");

        return serviceDao.getTypeStatusAndImg(ServiceType.BUNDLE, ServiceStatus.PUBLISHED);
    }
}
