package com.netcracker.edu.backend.job;

import com.netcracker.edu.backend.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UnderClarificationServiceRemovingScheduledJob {

    @Autowired
    private ServiceService serviceService;

    @Scheduled(cron = "0 0 1 * * *")
    public void removeNotClarifiedService() {
        serviceService.setRemovedForAllUnderClarificationServices();
    }
}
