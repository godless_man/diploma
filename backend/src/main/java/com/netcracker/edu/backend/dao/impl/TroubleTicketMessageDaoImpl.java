package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.TroubleTicketMessageDao;
import com.netcracker.edu.backend.mapper.TroubleTicketMessageMapper;
import com.netcracker.edu.backend.model.TroubleTicketMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class TroubleTicketMessageDaoImpl extends GenericDaoImpl<TroubleTicketMessage> implements TroubleTicketMessageDao {

    @Value("${db.query.troubleTicketMessage.insert}")
    private String sqlTroubleTicketMessageInsert;

    @Value("${db.query.troubleTicketMessage.update}")
    private String sqlTroubleTicketMessageUpdate;

    @Value("${db.query.troubleTicketMessage.getAllByTroubleTicketId}")
    private String sqlGetTroubleTicketMessagesByTroubleTicketId;

    public TroubleTicketMessageDaoImpl() {
        super(new TroubleTicketMessageMapper(), TableName.TROUBLE_TICKET_MESSAGE);
    }

    @Override
    protected String getInsertSql() {
        return sqlTroubleTicketMessageInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, TroubleTicketMessage entity) throws SQLException {
        int argNum = 1;

        statement.setLong(argNum++, entity.getTroubleTicketId());
        statement.setLong(argNum++, entity.getUserId());
        statement.setString(argNum++, entity.getMessage());
        statement.setTimestamp(argNum++, entity.getMessageTime());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlTroubleTicketMessageUpdate;
    }

    @Override
    public List<TroubleTicketMessage> getTroubleTicketMessagesByTroubleTicketId(long id) {
        log.debug("Getting trouble ticket messages by trouble ticket id: {}", id);

        List<TroubleTicketMessage> troubleTicketMessages = jdbcTemplate.query(sqlGetTroubleTicketMessagesByTroubleTicketId, new Object[]{id}, rowMapper);

        log.debug("Found trouble ticket messages with ids: {}", troubleTicketMessages.stream().map(TroubleTicketMessage::getId).collect(Collectors.toList()));

        return troubleTicketMessages;
    }

    @Override
    protected Object[] getArgsForUpdate(TroubleTicketMessage entity) {
        return new Object[]{entity.getTroubleTicketId(), entity.getUserId(), entity.getMessage(), entity.getMessageTime()};
    }
}
