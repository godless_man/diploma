package com.netcracker.edu.backend.dto.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewTroubleTicketWithMessageRequest {

    @NotNull
    private long serviceId;

    @NotBlank
    private String headline;

    @NotBlank
    private String message;
}
