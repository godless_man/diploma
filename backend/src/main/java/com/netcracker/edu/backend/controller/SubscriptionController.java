package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.SubscriptionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/subscribe")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class SubscriptionController {

    private SubscriptionService subscriptionService;

    @Autowired
    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @ApiOperation(value = "getSubscriptions", notes = "Loads all subscription list")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping
    public ResponseEntity<?> getSubscriptions(@CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting user's subscriptions");

        return ResponseEntity.ok(subscriptionService.getSubscriptions(userPrincipal.getId()));
    }

    @ApiOperation(value = "subscribe", notes = "Subscribes user to provider")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @PostMapping
    public ResponseEntity<?> subscribe(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody Object id) {
        log.debug("Requesting subscribing");

        return subscriptionService.subscribe(userPrincipal.getId(), Long.parseLong(id.toString()));
    }

    @ApiOperation(value = "isSubscribed", notes = "Checks is user subscribed")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> isSubscribed(@PathVariable long id, @CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting subscription check");

        return ResponseEntity.ok(subscriptionService.isSubscribed(userPrincipal.getId(), id));
    }

    @ApiOperation(value = "getSubscriptionCount", notes = "Gets provider's subscriptions")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/count/{id}")
    public ResponseEntity<?> getSubscriptionCount(@PathVariable long id) {
        log.debug("Requesting subscription check");

        return ResponseEntity.ok(subscriptionService.getSubscriptionCountForProvider(id));
    }

    @ApiOperation(value = "unsubscribe", notes = "Unsubscribes user from provider")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<?> unsubscribe(@PathVariable long id, @CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting unsubscribing");

        subscriptionService.unsubscribe(userPrincipal.getId());
        return ResponseEntity.ok().build();
    }
}
