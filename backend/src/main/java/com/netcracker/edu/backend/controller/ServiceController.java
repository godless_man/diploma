package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.Service;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.ServiceService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.netcracker.edu.backend.utils.Constants.RoleName;
import static com.netcracker.edu.backend.utils.Constants.ServiceType;

@RestController
@RequestMapping("/api/account/services")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @ApiOperation(value = "loadAllServices", notes = "Loads all services list")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping
    public ResponseEntity<?> loadAllServices() {
        log.debug("Requesting all services");

        return ResponseEntity.ok(serviceService.getAllServices());
    }

    @ApiOperation(value = "getService", notes = "Takes service id and loads object from DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Service with exact id not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> getService(@PathVariable long id) {
        log.debug("Requesting service");

        return ResponseEntity.ok(serviceService.getServiceById(id));
    }

    @ApiOperation(value = "getServicesByProviderId", notes = "Loads all services list by provider id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/provider")
    public ResponseEntity<?> getServicesByProviderId(@CurrentUser UserPrincipal currentUser) {
        log.debug("Requesting service");

        return ResponseEntity.ok(serviceService.getServicesByProviderId(currentUser.getId()));
    }

    @ApiOperation(value = "getPurchasedServices", notes = "Loads all purchased services list by user id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/purchased")
    public ResponseEntity<?> getPurchasedServices(@CurrentUser UserPrincipal user) {
        log.debug("Requesting purchased services");

        return ResponseEntity.ok(serviceService.getAllServicesPurchasedByUserWithUserId(user.getId()));
    }

    @ApiOperation(value = "createService", notes = "Takes service object and inserts it to DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @PostMapping
    public ResponseEntity<?> createService(@Valid @RequestBody Service service) {
        log.debug("Requesting service creating");

        service.setType(ServiceType.SERVICE); // dummy
        return ResponseEntity.ok(serviceService.createService(service));
    }

    @ApiOperation(value = "updateService", notes = "Takes service object and updates exact object in DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @PutMapping
    public ResponseEntity<?> updateService(@Valid @RequestBody Service service) {
        log.debug("Requesting service updating");

        serviceService.updateService(service);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "deleteService", notes = "Takes service id and deletes exact object from DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteService(@PathVariable long id) {
        log.debug("Requesting service deleting");

        serviceService.deleteServiceById(id);
        return ResponseEntity.ok().build();
    }
}
