package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.DashboardService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.netcracker.edu.backend.utils.Constants.RoleName;

@RestController
@RequestMapping("/api/account/dashboards")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    @ApiOperation(value = "loadAdminDashboardInfo", notes = "Loads admin dashboard info")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_ADMIN)
    @GetMapping("/admin")
    public ResponseEntity<?> loadAdminDashboardInfo(@CurrentUser UserPrincipal user) {
        log.debug("Requesting to get admin dashboard by id {}", user.getId());

        return ResponseEntity.ok(dashboardService.getAdminDashboardResponse());
    }

    @ApiOperation(value = "loadCarrierDashboardInfo", notes = "Loads carrier dashboard info")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @GetMapping("/carrier")
    public ResponseEntity<?> loadCarrierDashboardInfo(@CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting to get admin dashboard by id {}", userPrincipal.getId());

        return ResponseEntity.ok(dashboardService.getCarrierDashboardResponse(userPrincipal.getId()));
    }

    @ApiOperation(value = "loadCarrierDashboardInfo", notes = "Loads approver dashboard info")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_APPROVER)
    @GetMapping("/approver")
    public ResponseEntity<?> loadApproverDashboardInfo(@CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting to get admin dashboard by id {}", userPrincipal.getId());

        return ResponseEntity.ok(dashboardService.getApproverDashboardResponse(userPrincipal.getId()));
    }
}
