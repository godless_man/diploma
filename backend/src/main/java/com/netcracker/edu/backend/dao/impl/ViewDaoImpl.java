package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.ViewDao;
import com.netcracker.edu.backend.mapper.LongMapper;
import com.netcracker.edu.backend.mapper.ViewMapper;
import com.netcracker.edu.backend.model.View;
import com.netcracker.edu.backend.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
@Slf4j
public class ViewDaoImpl extends GenericDaoImpl<View> implements ViewDao {

    @Value("${db.query.view.insert}")
    private String sqlViewInsert;

    @Value("${db.query.view.update}")
    private String sqlViewUpdate;

    @Value("${db.query.view.countViewsByAllTrips}")
    private String sqlCountViewsByAllTrips;

    @Value("${db.query.view.countAllViews}")
    private String sqlCountAllViews;

    @Value("${db.query.view.countAllViewsByWeek}")
    private String sqlCountAllViewsByWeek;

    private static final RowMapper<Long> longMapper = new LongMapper();
    @Value("${db.query.view.countViewsByTripId}")
    private String sqlCountViewsByTripId;

    public ViewDaoImpl() {
        super(new ViewMapper(), Constants.TableName.VIEW);
    }

    @Override
    protected String getInsertSql() {
        return sqlViewInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, View entity) throws SQLException {
        int argNum = 1;
        statement.setLong(argNum++, entity.getUserId());
        statement.setLong(argNum++, entity.getServiceId());
        statement.setTimestamp(argNum++, entity.getViewDate());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlViewUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(View entity) {
        return new Object[]{entity.getUserId(), entity.getServiceId(), entity.getViewDate()};
    }

    @Override
    public Optional<Long> countViewsByTripId(long id) {
        log.debug("Counting views for trip(id = {})", id);

        List<Long> count = jdbcTemplate.query(sqlCountViewsByTripId, new Object[]{id}, longMapper);

        return getCountElement(count);
    }

    @Override
    public Optional<Long> countViewsByAllTrips(long id) {
        log.debug("count views by all trips for user id = {}", id);

        List<Long> count = jdbcTemplate.query(sqlCountViewsByAllTrips, new Object[]{id}, longMapper);

        return getCountElement(count);
    }

    @Override
    public Optional<Long> countAllViews(long id) {
        log.debug("count all views for user id = {}", id);

        List<Long> count = jdbcTemplate.query(sqlCountAllViews, new Object[]{id}, longMapper);

        return getCountElement(count);
    }

    @Override
    public Optional<Long> countAllViewsByWeek(long id) {
        log.debug("count all views by week for user id = {}", id);

        List<Long> count = jdbcTemplate.query(sqlCountAllViewsByWeek, new Object[]{id}, longMapper);

        return getCountElement(count);
    }
}

