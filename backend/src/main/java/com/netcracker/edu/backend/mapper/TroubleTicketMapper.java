package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.enums.TroubleTicketStatus;
import com.netcracker.edu.backend.model.TroubleTicket;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.TroubleTicketColumns;
import static com.netcracker.edu.backend.utils.Constants.TroubleTicketFeedbackColumns;

public class TroubleTicketMapper implements RowMapper<TroubleTicket> {

    @Override
    public TroubleTicket mapRow(ResultSet resultSet, int i) throws SQLException {
        TroubleTicket troubleTicket = new TroubleTicket();

        troubleTicket.setId(resultSet.getLong(TroubleTicketColumns.TROUBLE_TICKET_ID));
        troubleTicket.setServiceId(resultSet.getLong(TroubleTicketColumns.SERVICE_ID));
        troubleTicket.setUserId(resultSet.getLong(TroubleTicketColumns.USER_ID));
        troubleTicket.setApproverId(resultSet.getLong(TroubleTicketColumns.APPROVER_ID));
        troubleTicket.setStatusId(resultSet.getLong(TroubleTicketColumns.STATUS_ID));
        troubleTicket.setHeadline(resultSet.getString(TroubleTicketColumns.HEADLINE));
        troubleTicket.setApplyTime(resultSet.getTimestamp(TroubleTicketColumns.APPLY_TIME));

        troubleTicket.setStatus(TroubleTicketStatus.getNameFromId(resultSet.getLong(TroubleTicketColumns.STATUS_ID)));

        troubleTicket.setRating(resultSet.getInt(TroubleTicketFeedbackColumns.RATING));
        troubleTicket.setText(resultSet.getString(TroubleTicketFeedbackColumns.TEXT));

        return troubleTicket;
    }
}
