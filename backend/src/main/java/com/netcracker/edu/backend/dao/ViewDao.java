package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.View;

import java.util.Optional;

public interface ViewDao extends GenericDao<View> {

    Optional<Long> countViewsByTripId(long id);

    Optional<Long> countViewsByAllTrips(long id);

    Optional<Long> countAllViews(long id);

    Optional<Long> countAllViewsByWeek(long id);
}
