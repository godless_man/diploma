package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Chat;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.ChatColumns;

public class ChatMapper implements RowMapper<Chat> {

    @Override
    public Chat mapRow(ResultSet resultSet, int i) throws SQLException {
        Chat chat = new Chat();

        chat.setId(resultSet.getLong(ChatColumns.CHAT_ID));
        chat.setUserId(resultSet.getLong(ChatColumns.USER_ID));
        chat.setApproverId(resultSet.getLong(ChatColumns.APPROVER_ID));
        chat.setUsername(resultSet.getString(ChatColumns.USERNAME));

        return chat;
    }
}
