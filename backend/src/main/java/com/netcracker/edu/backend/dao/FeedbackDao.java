package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.Feedback;

import java.util.List;

public interface FeedbackDao extends GenericDao<Feedback> {

    List<Feedback> getTripFeedbackWithUsers(long tripId);
}
