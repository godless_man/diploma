package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Street;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.StreetColumns;

public class StreetMapper implements RowMapper<Street> {
    @Override
    public Street mapRow(ResultSet resultSet, int i) throws SQLException {
        Street street = new Street();

        street.setId(resultSet.getLong(StreetColumns.STREET_ID));
        street.setName(resultSet.getString(StreetColumns.NAME));
        street.setCityId(resultSet.getLong(StreetColumns.CITY_ID));

        return street;
    }
}