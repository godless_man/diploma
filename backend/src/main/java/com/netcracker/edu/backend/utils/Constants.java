package com.netcracker.edu.backend.utils;

import com.netcracker.edu.backend.dto.response.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public interface Constants {

    interface ResponseEntities {

        ResponseEntity<ApiResponse> BAD_REQ_USERNAME_TAKEN = new ResponseEntity<>(
                new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST
        );

        ResponseEntity<ApiResponse> BAD_REQ_EMAIL_TAKEN = new ResponseEntity<>(
                new ApiResponse(false, "Email Address already in use!"), HttpStatus.BAD_REQUEST
        );

        ResponseEntity<ApiResponse> USER_REGISTERED_SUCCESSFULLY = new ResponseEntity<>(
                new ApiResponse(true, "User registered successfully"), HttpStatus.CREATED
        );

        ResponseEntity<ApiResponse> VERIFICATION_TOKEN_EXPIRED = new ResponseEntity<>(
                new ApiResponse(false, "Verification token expired"), HttpStatus.BAD_REQUEST
        );

        ResponseEntity<ApiResponse> REGISTRATION_CONFIRMED_SUCCESSFULLY = new ResponseEntity<>(
                new ApiResponse(true, "Illuminati confirmed successfully"), HttpStatus.CREATED
        );

        ResponseEntity<ApiResponse> APPROVER_REGISTERED_SUCCESSFULLY = new ResponseEntity<>(
                new ApiResponse(true, "Approver registered successfully"), HttpStatus.CREATED
        );

        ResponseEntity<ApiResponse> PROVIDER_REGISTERED_SUCCESSFULLY = new ResponseEntity<>(
                new ApiResponse(true, "Provider registered successfully"), HttpStatus.CREATED
        );

        ResponseEntity<ApiResponse> TRIP_CREATED_SUCCESSFULLY = new ResponseEntity<>(
                new ApiResponse(true, "Trip created successfully"), HttpStatus.CREATED
        );

        ResponseEntity<ApiResponse> FEEDBACK_ADDED_SUCCESSFULLY = new ResponseEntity<>(
                new ApiResponse(true, "Feedback added successfully"), HttpStatus.CREATED
        );

        ResponseEntity<ApiResponse> VIEW_ADDED_SUCCESSFULLY = new ResponseEntity<>(
                new ApiResponse(true, "View added successfully"), HttpStatus.CREATED
        );

        ResponseEntity<ApiResponse> PASSWORD_RECOVERED_SUCCESSFULLY = new ResponseEntity<>(
                new ApiResponse(true, "Password recovered successfully"), HttpStatus.OK
        );
        ResponseEntity<ApiResponse> SUBSCRIBED_SUCCESSFULLY = new ResponseEntity<>(
                new ApiResponse(true, "Subscribed successfully"), HttpStatus.CREATED
        );
    }

    interface TableName {
        String USER = "\"USER\"";
        String USER_DETAILS = "user_details";
        String AUTHORITY = "authority";
        String SERVICE = "service";
        String SERVICE_TYPE = "service_type";
        String SERVICE_STATUS = "service_status";
        String TOKEN = "token";
        String DISCOUNT = "discount";
        String DISCOUNT_TYPE = "discount_type";
        String LOCATION = "location";
        String COUNTRY = "country";
        String CITY = "city";
        String STREET = "street";
        String ORDER = "\"ORDER\"";
        String SERVICE_MESSAGE = "service_message";
        String NOTIFICATION = "notification";
        String TROUBLE_TICKET = "trouble_ticket";
        String TROUBLE_TICKET_MESSAGE = "trouble_ticket_message";
        String TROUBLE_TICKET_FEEDBACK = "trouble_ticket_feedback";
        String FEEDBACK = "service_feedback";
        String CHAT = "chat";
        String CHAT_MESSAGE = "chat_message";
        String VIEW = "view";
        String COORDINATES = "coordinates";
        String SUBSCRIPTION = "subscription";
    }

    interface UserColumns {
        String USER_ID = "id";
        String DETAILED_USER_ID = "user_id";
        String AUTHORITY_ID = "authority_id";
        String USERNAME = "username";
        String EMAIL = "email";
        String PASSWORD = "password";
        String IS_ACTIVE = "is_active";
    }

    interface UserDetailsColumns {
        String USER_ID = "id";
        String DETAILED_USER_ID = "user_details_id";
        String FIRST_NAME = "first_name";
        String LAST_NAME = "last_name";
        String REGISTRATION_DATE = "registration_date";
        String LOCATION_ID = "location_id";
        String DETAILED_LOCATION_ID = "user_location_id";
        String IMAGE_SRC = "image_src";
    }

    interface AuthorityColumns {
        String AUTHORITY_ID = "id";
        String DETAILED_AUTHORITY_ID = "authority_id";
        String NAME = "authority_name";
    }

    interface ServiceColumns {
        String SERVICE_ID = "id";
        String DETAILED_SERVICE_ID = "service_id";
        String TYPE_ID = "type_id";
        String DETAILED_TYPE_ID = "service_type_id";
        String NAME = "service_name";
        String APPROVER_ID = "approver_id";
        String PROVIDER_ID = "provider_id";
        String STATUS_ID = "status_id";
        String LOCATION_ID = "location_id";
        String DESTINATION_ID = "destination_id";
        String NUMBER_OF_PEOPLE = "number_of_people";
        String PRICE = "price";
        String DESCRIPTION = "description";
        String IMAGE_SRC = "image_src";

        //Additional
        String TICKET_AMOUNT = "ticket_amount";
        String ONE_WAY = "one_way";
    }

    interface VerificationTokenColumns {
        String ID = "id";
        String DETAILED_ID = "verification_token_id";
        String TOKEN = "token";
        String EXPIRE_DATE = "expire_date";
        String USER_ID = "user_id";
    }

    interface VerificationTokenStatus {
        String TOKEN_INVALID = "invalidToken";
        String TOKEN_EXPIRED = "expired";
        String TOKEN_VALID = "valid";
    }

    interface ServiceStatusColumns {
        String SERVICE_STATUS_ID = "id";
        String NAME = "service_status_name";
    }

    interface ServiceTypeColumns {
        String SERVICE_TYPE_ID = "id";
        String NAME = "service_type_name";
    }

    interface DiscountColumns {
        String DISCOUNT_ID = "id";
        String DETAILED_DISCOUNT_ID = "discount_id";
        String SERVICE_ID = "service_id";
        String TYPE_ID = "type_id";
        String DETAILED_TYPE_ID = "discount_type_id";
        String AMOUNT = "amount";
        String START_DATE = "start_date";
        String END_DATE = "end_date";
    }

    interface DiscountTypeColumns {
        String DISCOUNT_TYPE_ID = "id";
        String NAME = "discount_type_name";
    }

    interface LocationColumns {
        String LOCATION_ID = "id";
        String DETAILED_LOCATION_ID = "location_id";
        String COUNTRY_ID = "country_id";
        String CITY_ID = "city_id";
        String STREET_ID = "street_id";
    }

    interface CountryColumns {
        String COUNTRY_ID = "id";
        String DETAILED_COUNTRY_ID = "county_id";
        String NAME = "country_name";
        String COORDINATE_ID = "coordinate_id";
    }

    interface CityColumns {
        String CITY_ID = "id";
        String DETAILED_CITY_ID = "city_id";
        String NAME = "city_name";
        String COUNTRY_ID = "country_id";
        String COORDINATE_ID = "coordinate_id";
    }

    interface StreetColumns {
        String STREET_ID = "id";
        String DETAILED_STREET_ID = "street_id";
        String NAME = "street_name";
        String CITY_ID = "city_id";
    }

    interface OrderColumns {
        String ORDER_ID = "id";
        String DETAILED_ORDER_ID = "order_id";
        String ORDER_DATE = "order_date";
        String USER_ID = "user_id";
        String TYPE_ID = "type_id";
        String DETAILED_TYPE_ID = "order_type_id";
        String PRICE = "price";
    }

    interface ServiceMessageColumns {
        String SERVICE_MESSAGE_ID = "id";
        String DETAILED_SERVICE_MESSAGE_ID = "service_message_id";
        String SERVICE_ID = "service_id";
        String MESSAGE_TIME = "message_time";
        String MESSAGE = "message";
    }

    interface NotificationColumns {
        String NOTIFICATION_ID = "id";
        String DETAILED_NOTIFICATION_ID = "notification_id";
        String USER_ID = "user_id";
        String SERVICE_ID = "service_id";
        String TROUBLE_TICKET_ID = "trouble_ticket_id";
        String MESSAGE = "message";
        String NOTIFICATION_TIME = "notification_time";
        String IS_READ = "is_read";
    }

    interface TroubleTicketColumns {
        String TROUBLE_TICKET_ID = "id";
        String DETAILED_TROUBLE_TICKET_ID = "trouble_ticket_id";
        String SERVICE_ID = "service_id";
        String USER_ID = "user_id";
        String APPROVER_ID = "approver_id";
        String STATUS_ID = "status_id";
        String HEADLINE = "headline";
        String APPLY_TIME = "apply_time";

        //Additional
        String TROUBLE_TICKET_STATUS_NAME = "trouble_ticket_status_name";
    }


    interface FeedbackColumns {
        String FEEDBACK_ID = "id";
        String FEEDBACK_DETAILED_ID = "feedback_id";
        String USER_ID = "user_id";
        String SERVICE_ID = "service_id";
        String RATING = "rating";
        String FEEDBACK_MESSAGE = "feedback_message";
        String FEEDBACK_DATE = "feedback_date";
    }

    interface TroubleTicketMessageColumns {
        String TROUBLE_TICKET_MESSAGE_ID = "id";
        String TROUBLE_TICKET_ID = "trouble_ticket_id";
        String USER_ID = "user_id";
        String MESSAGE = "message";
        String MESSAGE_TIME = "message_time";
    }

    interface PlacesAlias {
        String LOCATION_CITY_ID = "location_city_id";
        String LOCATION_CITY_NAME = "location_city_name";
        String LOCATION_COUNTRY_ID = "location_country_id";
        String LOCATION_COUNTRY_NAME = "location_country_name";
        String DESTINATION_CITY_ID = "destination_city_id";
        String DESTINATION_CITY_NAME = "destination_city_name";
        String DESTINATION_COUNTRY_ID = "destination_country_id";
        String DESTINATION_COUNTRY_NAME = "destination_country_name";
    }

    interface CoordinateColumns {
        String COORDINATE_ID = "coordinate_id";
        String COORDINATE_LATITUDE = "latitude";
        String COORDINATE_LONGITUDE = "longitude";
    }

    interface ViewColumns {
        String VIEW_ID = "id";
        String USER_ID = "user_id";
        String SERVICE_ID = "service_id";
        String VIEW_DATE = "view_date";
    }

    interface TroubleTicketFeedbackColumns {
        String TROUBLE_TICKET_FEEDBACK_ID = "id";
        String RATING = "rating";
        String TEXT = "text";
    }

    interface ChatColumns {
        String CHAT_ID = "id";
        String USER_ID = "user_id";
        String APPROVER_ID = "approver_id";
        String USERNAME = "username";
    }

    interface ChatMessageColumns {
        String CHAT_MESSAGE_ID = "id";
        String CHAT_ID = "chat_id";
        String USER_ID = "user_id";
        String MESSAGE = "message";
        String MESSAGE_TIME = "message_time";

        String USERNAME = "username";
        String OWNER_ID = "owner_id";
        String EMAIL = "email";
    }


    //made to enum
    interface RoleName {
        String ROLE_USER = "ROLE_USER";
        String ROLE_ADMIN = "ROLE_ADMIN";
        String ROLE_APPROVER = "ROLE_APPROVER";
        String ROLE_PROVIDER = "ROLE_PROVIDER";
    }

    //made to enum
    interface ServiceType {
        String TRIP = "Trip";
        String SERVICE = "Service";
        String BUNDLE = "Bundle";
        String SUGGESTION = "Suggestion";
    }

    //made to enum
    interface ServiceStatus {
        String DRAFT = "Draft";
        String OPEN = "Open";
        String ASSIGNED = "Assigned";
        String PUBLISHED = "Published";
        String UNDER_CLARIFICATION = "Under clarification";
        String REMOVED = "Removed";
        String ARCHIVED = "Archived";
    }

    interface EmailNotificationConstants {
        String REG_CONFIRM_TITLE = "Registration Confirmation";
        String PASS_RECOVERY_TITLE = "Password recovery";
        String SPECIAL_USER_APPROVER_TITLE = "Your approver's credentials";
        String SPECIAL_USER_PROVIDER_TITLE = "Your provider's credentials";
        String PROVIDER_TITLE = "Provider you follow published new trip";
        //        String CONFIRMATION_URL = "https://tripgod.herokuapp.com/#/registrationConfirm/";
        String CONFIRMATION_URL = "http://localhost:8080/#/registrationConfirm/";
        //        String TRIP_URL = "https://tripgod.herokuapp.com/#/trip/";
        String TRIP_URL = "https://localhost:8080/#/trip/";
        String TRIPGOD_EMAIL = "tripgod.team@gmail.com";
        String TEMPLATENAME_REGISTRATION_CONFIRM_EMAIL = "regConfirmTemplate";
        String TEMPLATENAME_PASSWORD_RECOVERY_EMAIL = "passRecoveryTemplate";
        String TEMPLATENAME_REGISTRATION_SPECIAL_USER_EMAIL = "regSpecUserTemplate";
        String TEMPLATENAME_PROVIDER_EMAIL = "providerEmailTemplate";
    }

    interface Gravatar {
        String GRAVATAR_BASE_URL = "https://www.gravatar.com/avatar/";
    }

    interface TokenExpirationTime {
        String EXPIRATION = "1440";
    }

    interface PasswordCharacters {
        String ALL_PASSWORD_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
    }

    interface GenericColumnNames {
        String COUNT = "count";
    }

    interface DashboardInterval {
        String PER_DAY = "Per day";
        String PER_WEEK = "Per week";
        String PER_MONTH = "Per month";
    }

    interface SubscriptionColumns {
        String USER_ID = "user_id";
        String PROVIDER_ID = "provider_id";
    }
}
