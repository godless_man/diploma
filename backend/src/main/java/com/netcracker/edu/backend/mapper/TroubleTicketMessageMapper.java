package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.TroubleTicketMessage;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.TroubleTicketMessageColumns;
import static com.netcracker.edu.backend.utils.Constants.UserColumns;

public class TroubleTicketMessageMapper implements RowMapper<TroubleTicketMessage> {

    @Override
    public TroubleTicketMessage mapRow(ResultSet resultSet, int i) throws SQLException {
        TroubleTicketMessage troubleTicketMessage = new TroubleTicketMessage();

        troubleTicketMessage.setId(resultSet.getLong(TroubleTicketMessageColumns.TROUBLE_TICKET_MESSAGE_ID));
        troubleTicketMessage.setTroubleTicketId(resultSet.getLong(TroubleTicketMessageColumns.TROUBLE_TICKET_ID));
        troubleTicketMessage.setUserId(resultSet.getLong(TroubleTicketMessageColumns.USER_ID));
        troubleTicketMessage.setMessage(resultSet.getString(TroubleTicketMessageColumns.MESSAGE));
        troubleTicketMessage.setMessageTime(resultSet.getTimestamp(TroubleTicketMessageColumns.MESSAGE_TIME));

        troubleTicketMessage.setUsername(resultSet.getString(UserColumns.USERNAME));
        troubleTicketMessage.setEmail(resultSet.getString(UserColumns.EMAIL));

        return troubleTicketMessage;
    }
}
