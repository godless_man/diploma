package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.enums.DiscountType;
import com.netcracker.edu.backend.enums.ServiceStatus;
import com.netcracker.edu.backend.enums.ServiceType;
import com.netcracker.edu.backend.model.City;
import com.netcracker.edu.backend.model.Country;
import com.netcracker.edu.backend.model.Discount;
import com.netcracker.edu.backend.model.Service;
import com.netcracker.edu.backend.utils.Constants;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ServiceRatingMapper implements RowMapper<Service> {

    @Override
    public Service mapRow(ResultSet resultSet, int i) throws SQLException {
        Service service = new Service();

        service.setId(resultSet.getLong(Constants.ServiceColumns.DETAILED_SERVICE_ID));
        service.setTypeId(resultSet.getLong(Constants.ServiceColumns.DETAILED_TYPE_ID));
        service.setName(resultSet.getString(Constants.ServiceColumns.NAME));
        service.setApproverId(resultSet.getLong(Constants.ServiceColumns.APPROVER_ID));
        service.setProviderId(resultSet.getLong(Constants.ServiceColumns.PROVIDER_ID));
        service.setStatusId(resultSet.getLong(Constants.ServiceColumns.STATUS_ID));
        service.setLocationId(resultSet.getLong(Constants.ServiceColumns.LOCATION_ID));
        service.setDestinationId(resultSet.getLong(Constants.ServiceColumns.DESTINATION_ID));
        service.setNumberOfPeople(resultSet.getLong(Constants.ServiceColumns.NUMBER_OF_PEOPLE));
        service.setPrice(resultSet.getBigDecimal(Constants.ServiceColumns.PRICE));
        service.setDescription(resultSet.getString(Constants.ServiceColumns.DESCRIPTION));
        service.setImgSrc(resultSet.getString(Constants.ServiceColumns.IMAGE_SRC));
        service.setOneWay(resultSet.getBoolean(Constants.ServiceColumns.ONE_WAY));

        service.setStatus(ServiceStatus.getNameFromId(service.getStatusId()));

        service.setType(ServiceType.getNameFromId(service.getTypeId()));

        service.setRating(resultSet.getBigDecimal("serv_rating"));

        City location = new City();

        location.setId(resultSet.getLong(Constants.PlacesAlias.LOCATION_CITY_ID));
        location.setName(resultSet.getString(Constants.PlacesAlias.LOCATION_CITY_NAME));
        location.setCountryId(resultSet.getLong(Constants.PlacesAlias.LOCATION_COUNTRY_ID));

        City destination = new City();
        destination.setId(resultSet.getLong(Constants.PlacesAlias.DESTINATION_CITY_ID));
        destination.setName(resultSet.getString(Constants.PlacesAlias.DESTINATION_CITY_NAME));
        destination.setCountryId(resultSet.getLong(Constants.PlacesAlias.DESTINATION_COUNTRY_ID));

        service.setLocation(location);
        service.setDestination(destination);

        Country locationCountry = new Country();

        locationCountry.setId(location.getCountryId());
        locationCountry.setName(resultSet.getString(Constants.PlacesAlias.LOCATION_COUNTRY_NAME));

        Country destinationCountry = new Country();

        destinationCountry.setId(destination.getCountryId());
        destinationCountry.setName(resultSet.getString(Constants.PlacesAlias.DESTINATION_COUNTRY_NAME));

        service.getLocation().setCountry(locationCountry);
        service.getDestination().setCountry(destinationCountry);

        Discount discount = new Discount();

        discount.setId(resultSet.getLong(Constants.DiscountColumns.DETAILED_DISCOUNT_ID));
        discount.setServiceId(service.getId());
        discount.setTypeId(resultSet.getLong(Constants.DiscountColumns.DETAILED_TYPE_ID));
        discount.setAmount(resultSet.getDouble(Constants.DiscountColumns.AMOUNT));
        discount.setStartDate(resultSet.getTimestamp(Constants.DiscountColumns.START_DATE));
        discount.setEndDate(resultSet.getTimestamp(Constants.DiscountColumns.END_DATE));

        discount.setType(DiscountType.getNameFromId(discount.getTypeId()));

        if (discount.getAmount() == 0) {
            service.setDiscount(null);
        } else {
            service.setDiscount(discount);
        }

        return service;
    }
}
