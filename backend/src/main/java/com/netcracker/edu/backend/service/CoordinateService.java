package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.CoordinateDao;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.Coordinate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Slf4j
public class CoordinateService {

    @Autowired
    private CoordinateDao coordinateDao;

    public Coordinate getLocById(long id) {
        return coordinateDao.getLocationByServiceId(id)
                .orElseThrow(() -> {
                    log.error("Coordinate location not found for service id: {}", id);
                    return new AppException("Coordinate location not found for service id: " + id);
                });
    }

    public Coordinate getDestById(long id) {
        return coordinateDao.getDestinationByServiceId(id)
                .orElseThrow(() -> {
                    log.error("Coordinate destination not found for service id: {}", id);
                    return new AppException("Coordinate destination not found for service id: " + id);
                });
    }

    public Coordinate getCoordinateByCityId(long id) {
        return coordinateDao.getCoordinateByCityId(id)
                .orElseThrow(() -> {
                    log.error("Coordinate not found for city id: {}", id);
                    return new AppException("Coordinate not found for city id: " + id);
                });
    }
}
