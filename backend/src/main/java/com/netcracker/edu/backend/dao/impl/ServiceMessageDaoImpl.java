package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.ServiceMessageDao;
import com.netcracker.edu.backend.mapper.ServiceMessageMapper;
import com.netcracker.edu.backend.model.ServiceMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class ServiceMessageDaoImpl extends GenericDaoImpl<ServiceMessage> implements ServiceMessageDao {

    @Value("${db.query.serviceMessage.insert}")
    private String sqlServiceMessageInsert;

    @Value("${db.query.serviceMessage.getServiceMessageByServiceId}")
    private String sqlGetServiceMessageByServiceId;

    public ServiceMessageDaoImpl() {
        super(new ServiceMessageMapper(), TableName.SERVICE_MESSAGE);
    }

    @Override
    protected String getInsertSql() {
        return sqlServiceMessageInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, ServiceMessage entity) throws SQLException {
        int argNum = 1;
        statement.setLong(argNum++, entity.getServiceId());
        statement.setTimestamp(argNum++, entity.getMessageTime());
        statement.setString(argNum++, entity.getMessage());

        return statement;
    }

    @Override
    public Optional<ServiceMessage> getServiceMessageByServiceId(long id) {
        log.debug("Getting serviceMessage by serviceId: {}", id);

        List<ServiceMessage> serviceMessage = jdbcTemplate.query(sqlGetServiceMessageByServiceId, new Object[]{id}, rowMapper);

        log.debug("Found serviceMessages (by serviceId) with ids {}", serviceMessage.stream().map(ServiceMessage::getId).collect(Collectors.toList()));

        if (serviceMessage.size() > 1) {
            return Optional.of(serviceMessage.get(serviceMessage.size() - 1));
        }
        return getSingleElement(serviceMessage);
    }

    @Override
    protected String getUpdateSql() {
        return null;
    }

    @Override
    protected Object[] getArgsForUpdate(ServiceMessage entity) {
        return new Object[0];
    }
}
