package com.netcracker.edu.backend.service;

import com.google.common.collect.ImmutableMap;
import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;

import static com.netcracker.edu.backend.utils.Constants.EmailNotificationConstants.*;

@Service
@Slf4j
@Transactional
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Qualifier("templateEngine")
    @Autowired
    private SpringTemplateEngine templateEngine;

    @Async("asyncExecutor")
    public void sendEmailForRegConfirm(User user, String token) throws MessagingException {
        log.debug("Sending email for registration confirm");

        prepareAndSendEmail(user.getEmail(), ImmutableMap.of("message", CONFIRMATION_URL + token),
                REG_CONFIRM_TITLE, TEMPLATENAME_REGISTRATION_CONFIRM_EMAIL);
    }

    @Async("asyncExecutor")
    public void sendProvidersEmail(String email, Long tripId) throws MessagingException {
        log.debug("Sending email for registration confirm");

        prepareAndSendEmail(email, ImmutableMap.of("message", TRIP_URL + tripId),
                PROVIDER_TITLE, TEMPLATENAME_PROVIDER_EMAIL);
    }

    @Async("asyncExecutor")
    public void sendEmailForPassRecovery(String email, String password) throws MessagingException {
        log.debug("Sending email for password recovery");

        prepareAndSendEmail(email, ImmutableMap.of("password", password),
                PASS_RECOVERY_TITLE, TEMPLATENAME_PASSWORD_RECOVERY_EMAIL);
    }

    @Async("asyncExecutor")
    public void sendEmailForSpecUser(String email, String username, String password, String roleName) throws MessagingException {
        log.debug("Sending email to special user");

        if (Objects.equals(roleName, Constants.RoleName.ROLE_APPROVER)) {
            prepareAndSendEmail(email, ImmutableMap.of(
                    "username", username,
                    "password", password
            ), SPECIAL_USER_APPROVER_TITLE, TEMPLATENAME_REGISTRATION_SPECIAL_USER_EMAIL);
        } else {
            prepareAndSendEmail(email, ImmutableMap.of(
                    "username", username,
                    "password", password
            ), SPECIAL_USER_PROVIDER_TITLE, TEMPLATENAME_REGISTRATION_SPECIAL_USER_EMAIL);
        }
    }

    private void prepareAndSendEmail(String email, Map<String, Object> values, String title, String templateName) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        Context context = new Context();
        context.setVariables(values);
        String html = templateEngine.process(templateName, context);

        helper.setTo(email);
        helper.setText(html, true);
        helper.setSubject(title);
        helper.setFrom(TRIPGOD_EMAIL);

        mailSender.send(message);
    }
}
