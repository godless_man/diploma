package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.Feedback;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.FeedbackService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.netcracker.edu.backend.utils.Constants.ResponseEntities.FEEDBACK_ADDED_SUCCESSFULLY;
import static com.netcracker.edu.backend.utils.Constants.RoleName;

@RestController
@RequestMapping("/api/feedback")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class FeedbackController {

    @Autowired
    private FeedbackService feedbackService;

    @ApiOperation(value = "getAllFeedback", notes = "Loads all feedback list")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping
    public ResponseEntity<?> getAllFeedback() {
        log.debug("Getting all feedback in {}: ", this.getClass().getName());

        return ResponseEntity.ok(feedbackService.getAllFeedback());
    }

    @ApiOperation(value = "getFeedback", notes = "Takes feedback id and loads object from DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Feedback with exact id not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> getFeedback(@PathVariable long id) {
        log.debug("Getting feedback(id = {}) in {}: ", id, this.getClass().getName());

        return ResponseEntity.ok(feedbackService.getFeedbackById(id));
    }

    @ApiOperation(value = "checkFeedbackPermit", notes = "Returns permit (true/false) to give feedback")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/trip/checkFeedbackPermit/{tripId}")
    public ResponseEntity<?> checkFeedbackPermit(@PathVariable long tripId, @CurrentUser UserPrincipal currentUser) {
        if (currentUser != null) {
            log.debug("Checking feedback permit of user(id = {}) for trip(id = {}) in {}: ", currentUser.getId(), tripId, this.getClass().getName());

            return ResponseEntity.ok(feedbackService.canUserGiveFeedback(currentUser.getId(), tripId));
        } else {
            return ResponseEntity.ok(false);
        }
    }

    @ApiOperation(value = "getAllFeedback", notes = "Loads all feedback list of exact trip")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/trip/{tripId}")
    public ResponseEntity<?> getTripFeedback(@PathVariable long tripId) {
        log.debug("Getting feedback for trip(id = {}) in {}: ", tripId, this.getClass().getName());

        return ResponseEntity.ok(feedbackService.getTripFeedback(tripId));
    }

    @ApiOperation(value = "addFeedback", notes = "Takes feedback object and inserts it to DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_USER)
    @PostMapping
    public ResponseEntity<?> addFeedback(@Valid @RequestBody Feedback feedback, @CurrentUser UserPrincipal currentUser) {
        log.debug("Inserting feedback(id = {}) in {}: ", feedback.getId(), this.getClass().getName());

        feedback.setUserId(currentUser.getId());

        feedbackService.addFeedback(feedback);

        return FEEDBACK_ADDED_SUCCESSFULLY;
    }

    @ApiOperation(value = "updateFeedback", notes = "Takes feedback object and updates exact object in DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_USER)
    @PutMapping
    public ResponseEntity<?> updateFeedback(@Valid @RequestBody Feedback feedback) {
        log.debug("Updating feedback(id = {}) in {}: ", feedback.getId(), this.getClass().getName());

        feedbackService.updateFeedback(feedback);

        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "deleteFeedback", notes = "Takes feedback id and deletes exact object from DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_USER)
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteFeedback(@PathVariable long id) {
        log.debug("Deleting feedback(id = {}) in {}: ", id, this.getClass().getName());

        feedbackService.deleteFeedbackById(id);

        return ResponseEntity.ok().build();
    }
}
