package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.TokenDao;
import com.netcracker.edu.backend.mapper.TokenMapper;
import com.netcracker.edu.backend.model.Token;
import com.netcracker.edu.backend.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class TokenDaoImpl extends GenericDaoImpl<Token> implements TokenDao {

    @Value("${db.query.token.insert}")
    private String sqlTokenInsert;
    @Value("${db.query.token.findByToken}")
    private String sqlFindByToken;

    public TokenDaoImpl() {
        super(new TokenMapper(), TableName.TOKEN);
    }

    @Override
    public Optional<Token> findByToken(String token) {
        log.debug("Getting token: {}", token);

        List<Token> tokens = jdbcTemplate.query(sqlFindByToken, new Object[]{token}, rowMapper);

        log.debug("Found tokens with ids {}", tokens.stream().map(Token::getId).collect(Collectors.toList()));

        return getSingleElement(tokens);
    }

    @Override
    public Token findByUser(User user) {
        return null;
    }

    @Override
    public Stream<Token> findAllByExpiryDateLessThan(Date now) {
        return null;
    }

    @Override
    public void deleteByExpiryDateLessThan(Date now) {

    }

    @Override
    protected String getInsertSql() {
        return sqlTokenInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Token entity) throws SQLException {
        int argNum = 1;
        statement.setString(argNum++, entity.getToken());
        statement.setLong(argNum++, entity.getUserID());
        statement.setTimestamp(argNum++, entity.getExpiryDate());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return null;
    }

    @Override
    protected Object[] getArgsForUpdate(Token entity) {
        return new Object[0];
    }
}
