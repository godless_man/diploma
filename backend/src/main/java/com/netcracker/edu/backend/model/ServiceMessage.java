package com.netcracker.edu.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceMessage extends Identified {

    @NotNull
    private long serviceId;
    private Timestamp messageTime;
    @NotBlank
    private String message;

    private long approverId;
    private String approverUsername;

    public ServiceMessage(long serviceId, Timestamp messageTime, String message) {
        this.serviceId = serviceId;
        this.messageTime = messageTime;
        this.message = message;
    }
}
