package com.netcracker.edu.backend.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomLifecycleEvent<T> {

    private long subjectId;
    private T object;
    private long oldStatusId;
    private long newStatusId;
    private long recipientId;

    public CustomLifecycleEvent(long subjectId, T object, long oldStatusId, long newStatusId) {
        this.subjectId = subjectId;
        this.object = object;
        this.oldStatusId = oldStatusId;
        this.newStatusId = newStatusId;
    }
}
