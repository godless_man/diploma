package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Country;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.CountryColumns;

public class CountryMapper implements RowMapper<Country> {
    @Override
    public Country mapRow(ResultSet resultSet, int i) throws SQLException {
        Country country = new Country();

        country.setId(resultSet.getLong(CountryColumns.COUNTRY_ID));
        country.setName(resultSet.getString(CountryColumns.NAME));

        return country;
    }
}
