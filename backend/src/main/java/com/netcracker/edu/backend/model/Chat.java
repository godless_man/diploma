package com.netcracker.edu.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Chat extends Identified {

    private long userId;
    private Long approverId;

    private String username;

    public Chat(long userId) {
        this.userId = userId;
    }

    public Chat(long userId, Long approverId) {
        this.userId = userId;
        this.approverId = approverId;
    }

    public Chat(long userId, String username) {
        this.userId = userId;
        this.username = username;
    }
}
