package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.OrderDao;
import com.netcracker.edu.backend.mapper.LongMapper;
import com.netcracker.edu.backend.mapper.OrderDetailsMapper;
import com.netcracker.edu.backend.mapper.OrderMapper;
import com.netcracker.edu.backend.model.Order;
import com.netcracker.edu.backend.model.OrderDetails;
import com.netcracker.edu.backend.model.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class OrderDaoImpl extends GenericDaoImpl<Order> implements OrderDao {

    private static final RowMapper<OrderDetails> orderDetailedMapper = new OrderDetailsMapper();

    @Value("${db.query.order.getOrderWithPriceToday}")
    private String sqlGetOrdersWithPriceToday;

    @Value("${db.query.order.getOrderWithPriceWeek}")
    private String sqlGetOrdersWithPriceWeek;

    @Value("${db.query.order.getOrderWithPriceMonth}")
    private String sqlGetOrdersWithPriceMonth;

    @Value("${db.query.order.getOrdersOfUser}")
    private String sqlGetOrdersOfUser;

    @Value("${db.query.order.getSalesByTrips}")
    private String sqlGetSalesByTrips;

    @Value("${db.query.order.getSalesByServices}")
    private String sqlGetSalesByServices;

    @Value("${db.query.order.getSalesByWeek}")
    private String sqlGetSalesByWeek;

    @Value("${db.query.order.getBasketIdByUserId}")
    private String sqlGetBasketIdByUserId;

    @Value("${db.query.order.getServicesForBasket}")
    private String sqlGetServicesForBasket;

    @Value("${db.query.order.createBasket}")
    private String sqlCreateBasket;

    @Value("${db.query.order.addToBasket}")
    private String sqlAddToBasket;

    @Value("${db.query.order.deleteFromBasket}")
    private String sqlDeleteFromBasket;

    @Value("${db.query.order.completeOrder}")
    private String sqlCompleteOrder;

    @Value("${db.query.order.getDetailsOfOrder}")
    private String sqlGetDetailsOfOrder;

    private static final RowMapper<Long> longMapper = new LongMapper();

    public OrderDaoImpl() {
        super(new OrderMapper(), TableName.ORDER);
    }

    @Override
    protected String getInsertSql() {
        return null;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Order entity) {
        return null;
    }

    @Override
    protected String getUpdateSql() {
        return null;
    }

    @Override
    protected Object[] getArgsForUpdate(Order entity) {
        return new Object[0];
    }

    @Override
    public List<Order> getOrdersWithPriceToday() {
        return jdbcTemplate.query(sqlGetOrdersWithPriceToday, rowMapper);
    }

    @Override
    public List<Order> getOrdersWithPriceWeek() {
        return jdbcTemplate.query(sqlGetOrdersWithPriceWeek, rowMapper);
    }

    @Override
    public List<Order> getOrdersWithPriceMonth() {
        return jdbcTemplate.query(sqlGetOrdersWithPriceMonth, rowMapper);
    }

    @Override
    public List<Order> getOrdersOfUser(long id) {
        log.debug("Getting orders by user id: {}", id);

        List<Order> orders = jdbcTemplate.query(sqlGetOrdersOfUser, new Object[]{id}, rowMapper);

        log.debug("Found orders with id: {}", orders.stream().map(Order::getId).collect(Collectors.toList()));

        return orders;
    }

    @Override
    public List<OrderDetails> getDetailsOfOrder(long id) {
        log.debug("Getting details of order: {}", id);

        List<OrderDetails> orderDetails = jdbcTemplate.query(sqlGetDetailsOfOrder, new Object[]{id}, orderDetailedMapper);

        log.debug("Found orders with id: {}", orderDetails.stream().map(OrderDetails::getServiceId).collect(Collectors.toList()));

        return orderDetails;
    }

    @Override
    public Optional<Long> getSalesByTrips(long id) {
        log.debug("Getting sales by trips with providerId: {}", id);

        List<Long> sales = jdbcTemplate.query(sqlGetSalesByTrips, new Object[] {id}, longMapper);

        return getCountElement(sales);
    }

    @Override
    public Optional<Long> getSalesByServices(long id) {
        log.debug("Getting sales by services with providerId: {}", id);

        List<Long> sales = jdbcTemplate.query(sqlGetSalesByServices, new Object[] {id}, longMapper);

        return getCountElement(sales);
    }

    @Override
    public Optional<Long> getSalesByWeek(long id) {
        log.debug("Getting sales by week with providerId: {}", id);

        List<Long> sales = jdbcTemplate.query(sqlGetSalesByWeek, new Object[] {id}, longMapper);

        return getCountElement(sales);
    }

    @Override
    public List<Service> getOrderByUserIdAndTypeId(Long userId, int i) {
        return null;
    }

    @Override
    public void addToBasket(long serviceId, Long userId) {
        log.debug("Adding service with id {} to basket of user with id {}", serviceId, userId);

        if (getBasketIdByUserId(userId).orElse(0L) == 0L) {
            createBasket(userId);
        }

        Long orderId = getBasketIdByUserId(userId).orElse(0L);
        jdbcTemplate.update(sqlAddToBasket, orderId, serviceId);
    }

    @Override
    public void deleteFromBasket(long serviceId, Long userId) {
        log.debug("Deleting service with id {} from basket of user with id {}", serviceId, userId);

        Long orderId = getBasketIdByUserId(userId).orElse(0L);
        jdbcTemplate.update(sqlDeleteFromBasket, orderId, serviceId);
    }

    @Override
    public void completeOrder(Long userId) {
        log.debug("Completing order of user with id {}", userId);
        jdbcTemplate.update(sqlCompleteOrder, userId);
    }

    @Override
    public List<Long> getServicesId(Long userId) {
        log.debug("Getting services list of user with id {}", userId);

        List<Long> services = jdbcTemplate.query(sqlGetServicesForBasket, new Object[]{userId}, new LongMapper());

        log.debug("Found services  with ids {}", services);

        return services;
    }

    private Optional<Long> getBasketIdByUserId(Long userId) {
        log.debug("Getting services list of user with id {}", userId);

        List<Long> basket = jdbcTemplate.query(sqlGetBasketIdByUserId, new Object[]{userId}, new LongMapper());

        log.debug("Found basket with id {}", basket);

        return getCountElement(basket);
    }

    private void createBasket(Long userId) {
        jdbcTemplate.update(sqlCreateBasket, Timestamp.from(Instant.now()), userId);
    }
}
