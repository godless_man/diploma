package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ServiceDao;
import com.netcracker.edu.backend.dto.request.ChangeStatusRequest;
import com.netcracker.edu.backend.dto.request.SearchTripMultipleRequest;
import com.netcracker.edu.backend.dto.request.SearchTripRequest;
import com.netcracker.edu.backend.enums.ServiceStatus;
import com.netcracker.edu.backend.enums.ServiceType;
import com.netcracker.edu.backend.event.publisher.LifecycleEventPublisher;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.exception.BadRequestException;
import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.security.UserPrincipal;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static com.netcracker.edu.backend.dto.response.AdminDashboardResponse.CountryTrips;
import static com.netcracker.edu.backend.utils.Constants.RoleName;

@Service
@Slf4j
@Transactional
public class TripService {

    private ServiceDao serviceDao;
    private ServiceService serviceService;
    private SuggestionService suggestionService;
    private LocationService locationService;
    private DiscountService discountService;
    private LifecycleEventPublisher publisher;
    private CoordinateService coordinateService;
    private SubscriptionService subscriptionService;
    private EmailService emailService;

    @Value("${db.service.searchByCountry}")
    private String sqlSearchByCountry;

    @Autowired
    public TripService(ServiceDao serviceDao, ServiceService serviceService, SuggestionService suggestionService,
                       LocationService locationService, DiscountService discountService, LifecycleEventPublisher publisher, CoordinateService coordinateService, SubscriptionService subscriptionService, EmailService emailService) {
        this.serviceDao = serviceDao;
        this.serviceService = serviceService;
        this.suggestionService = suggestionService;
        this.locationService = locationService;
        this.discountService = discountService;
        this.publisher = publisher;
        this.coordinateService = coordinateService;
        this.subscriptionService = subscriptionService;
        this.emailService = emailService;
    }

    public com.netcracker.edu.backend.model.Service getTripByIdDetailedWithSuggestions(long id) {
        log.debug("Getting trip");

        com.netcracker.edu.backend.model.Service trip = serviceDao.getTripByIdDetailed(id)
                .orElseThrow(() -> {
                    log.error("Trip with id {} not found", id);
                    return new AppException("Trip with id " + id + " not found");
                });

        List<com.netcracker.edu.backend.model.Service> suggestions = serviceDao.getSuggestionsByTripId(id);

        for (com.netcracker.edu.backend.model.Service suggestion : suggestions) {

            List<com.netcracker.edu.backend.model.Service> services =
                    serviceDao.getServicesBySuggestionId(suggestion.getId());

            suggestion.setServices(services);
        }

        trip.setSuggestions(suggestions);

        if (trip.getLocation() != null) {
            trip.getLocation().setCoordinate(coordinateService.getLocById(trip.getId()));
            trip.getDestination().setCoordinate(coordinateService.getDestById(trip.getId()));
        }
        return trip;
    }

    public com.netcracker.edu.backend.model.Service getTripById(long id) {
        log.debug("Getting trip");

        return serviceDao.getTripById(id)
                .orElseThrow(() -> {
                    log.error("Trip not found with id: {}", id);
                    return new AppException("Trip with id " + id + " not found");
                });
    }

    public com.netcracker.edu.backend.model.Service createTrip(com.netcracker.edu.backend.model.Service trip) {
        log.debug("Creating trip");

        return serviceDao.insert(trip);
    }

    public void updateTrip(com.netcracker.edu.backend.model.Service trip) {
        log.debug("Updating trip");

        serviceDao.update(trip);
    }

    public void deleteTripById(long id) {
        log.debug("Deleting trip");

        serviceDao.deleteById(id);
    }

    public void deleteTrip(com.netcracker.edu.backend.model.Service trip) {
        serviceDao.delete(trip);
    }

    public List<com.netcracker.edu.backend.model.Service> getAllTrips() {
        log.debug("Getting trips");

        return serviceDao.getAllTrips();
    }

    public List<com.netcracker.edu.backend.model.Service> getAllTripsOfStatus(String status) {
        log.debug("Getting trips");

        return serviceDao.getAllTripsOfStatus(status);
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsByProviderId(long id) {
        log.debug("Getting trips");

        return serviceDao.getTripsByProviderId(ServiceType.TRIP.getServiceTypeName(), id);
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsByProviderIdWithLocationsAndStatus(long id) {
        log.debug("Getting trips");

        return serviceDao.getTripsByProviderIdWithLocationAndStatus(id);
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsByApproverIdOrNotAssignedWithLocationsAndStatus(long id) {
        log.debug("Getting trips");

        return serviceDao.getTripsByApproverIdOrNotAssignedWithLocationAndStatus(id);
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsDependingOnAuthority(UserPrincipal user) {
        String currentAuthority = user.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .findFirst().orElse("UNKNOWN");

        List<com.netcracker.edu.backend.model.Service> trips = new ArrayList<>();

        if (currentAuthority.equals(RoleName.ROLE_PROVIDER)) {
            trips = getTripsByProviderIdWithLocationsAndStatus(user.getId());
        } else if (currentAuthority.equals(RoleName.ROLE_APPROVER)) {
            trips = getTripsByApproverIdOrNotAssignedWithLocationsAndStatus(user.getId());
        }

        return trips;
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsOfStatusByProviderId(String status, long id) {
        log.debug("Getting trips");

        return serviceDao.getTripsOfStatusByProviderId(status, id);
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsByApproverId(long id) {
        log.debug("Getting trips");

        return serviceDao.getTripsByApproverId(id);
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsOfStatusByApproverId(String status, long id) {
        log.debug("Getting trips");

        return serviceDao.getTripsOfStatusByApproverId(status, id);
    }

    public long countAllTrips() {
        log.debug("Counting trips");

        return serviceDao.countAllTrips().orElse(0L);
    }

    public List<CountryTrips> getTripsAmountByCountry() {
        log.debug("Getting trips");

        return serviceDao.getTripsAmountByCountry();
    }

    public List<com.netcracker.edu.backend.model.Service> getAllPublishedTripsWithDetails() {
        log.debug("Getting published trips");

        return serviceDao.getPublishedWithLocationsAndStatus(ServiceType.TRIP.getServiceTypeName());
    }

    public com.netcracker.edu.backend.model.Service createOrUpdateTrip(com.netcracker.edu.backend.model.Service trip, long userId) {
        log.debug("Creating (or updating) trip");

        final long oldStatusId = trip.getStatusId();

        String statusToSet;

        if (ServiceStatus.DRAFT.getServiceStatusName().equals(trip.getStatus())) {
            statusToSet = ServiceStatus.DRAFT.getServiceStatusName();
        } else {
            statusToSet = ServiceStatus.OPEN.getServiceStatusName();
        }

        trip.setStatus(statusToSet);
        restoreMappings(trip); // Setting id's from string values

        if (trip.getId() == 0) {
            trip = serviceDao.insert(trip);
        } else {
            serviceDao.update(trip);
        }

        for (com.netcracker.edu.backend.model.Service suggestion : trip.getSuggestions()) {

            suggestion.setStatus(statusToSet);
            suggestion.setProviderId(trip.getProviderId());

            suggestion.setLocationId(trip.getLocationId());
            suggestionService.restoreMappings(suggestion); // Setting id's from string values

            if (suggestion.getId() == 0) {
                suggestion = serviceDao.insert(suggestion);
                suggestionService.addSuggestionLink(suggestion.getId(), trip.getId());
            } else {
                serviceDao.update(suggestion);
            }

            for (com.netcracker.edu.backend.model.Service service : suggestion.getServices()) {

                service.setStatus(statusToSet);
                service.setProviderId(suggestion.getProviderId());

                service.setLocationId(trip.getLocationId());
                serviceService.restoreMappings(service); // Setting id's from string values

                if (service.getId() == 0) {
                    service = serviceDao.insert(service);
                    suggestionService.addSuggestionLink(suggestion.getId(), service.getId());
                } else {
                    serviceDao.update(service);
                }

            }
        }
        setApproverIdOfAllServices(0L, trip); // Setting approver id to 0 null everywhere

        publisher.publishLifecycleEventNotification(userId, trip, trip.getStatusId(), oldStatusId);

        return trip;
    }

    public void restoreMappings(com.netcracker.edu.backend.model.Service trip) {
        log.debug("Restoring mappings service");

        if (trip.getApproverId() != null && trip.getApproverId() == 0) {
            trip.setApproverId(null);
        }

        trip.setStatusId(ServiceStatus.getIdFromName(trip.getStatus()));

        trip.setTypeId(ServiceType.TRIP.getId());

        locationService.mapLocation(trip.getLocation());
        trip.setLocationId(trip.getLocation().getId());

        locationService.mapLocation(trip.getDestination());
        trip.setDestinationId(trip.getDestination().getId());
    }

    public List<com.netcracker.edu.backend.model.Service> getAllTripsSortedByImg() {
        log.debug("Getting trips sorted by img");

        int i = 0; //index of picture
        List<com.netcracker.edu.backend.model.Service> trips = serviceDao.getAllTrips();
        for (com.netcracker.edu.backend.model.Service trip : trips) {
            if (trip.getImgSrc() != null) {

                int index = trips.indexOf(trip);
                trips.set(index, trips.get(i));
                trips.set(i, trip);
                i++;
            }
        }

        return trips;
    }

    public com.netcracker.edu.backend.model.Service setApproverIdOfAllServices(Long approverId, com.netcracker.edu.backend.model.Service trip) {
        log.debug("Setting approverId of all services to id: {}", approverId);

        if (Objects.equals(approverId, 0L)) {
            approverId = null;
        }

        trip.setApproverId(approverId);
        for (com.netcracker.edu.backend.model.Service suggestion : trip.getSuggestions()) {
            suggestion.setApproverId(approverId);
            if (suggestion.getDestinationId() != null && suggestion.getDestinationId() == 0L) {
                suggestion.setDestinationId(null);
            }
            serviceService.updateService(suggestion);
            for (com.netcracker.edu.backend.model.Service service : suggestion.getServices()) {
                service.setApproverId(approverId);
                if (service.getDestinationId() != null && service.getDestinationId() == 0L) {
                    service.setDestinationId(null);
                }
                serviceService.updateService(service);
            }
        }
        serviceService.updateService(trip);
        return trip;
    }

    public com.netcracker.edu.backend.model.Service changeStatusOfAllServices(long statusId, com.netcracker.edu.backend.model.Service trip) {
        log.debug("Setting statusId of all services to id: {}", statusId);

        trip.setStatusId(statusId);
        if (Objects.equals(trip.getApproverId(), 0L)) {
            trip.setApproverId(null);
        }
        for (com.netcracker.edu.backend.model.Service suggestion : trip.getSuggestions()) {
            suggestion.setStatusId(statusId);
            if (suggestion.getDestinationId() != null && suggestion.getDestinationId() == 0) {
                suggestion.setDestinationId(null);
            }
            if (Objects.equals(suggestion.getApproverId(), 0L)) {
                suggestion.setApproverId(null);
            }
            serviceService.updateService(suggestion);
            for (com.netcracker.edu.backend.model.Service service : suggestion.getServices()) {
                if (Objects.equals(service.getApproverId(), 0L)) {
                    service.setApproverId(null);
                }
                if (statusId != ServiceStatus.ARCHIVED.getId()) {
                    service.setStatusId(statusId);
                }
                if (service.getDestinationId() != null && service.getDestinationId() == 0) {
                    service.setDestinationId(null);
                }
                serviceService.updateService(service);
            }
        }
        serviceService.updateService(trip);
        return trip;
    }


    public com.netcracker.edu.backend.model.Service changeLifecycleStep(ChangeStatusRequest request, Long approverId) {
        log.debug("Changing lifecycle step of: {}", request.getService());

        com.netcracker.edu.backend.model.Service trip = request.getServiceOptional()
                .orElseThrow(() -> {
                    log.error("Got empty service");
                    return new BadRequestException("Got empty service");
                });

        long oldStatusId = trip.getStatusId();
        long newStatusId = request.getStatusId();

        trip = setApproverIdOfAllServices(approverId, trip);
        trip = changeStatusOfAllServices(newStatusId, trip);

        if (approverId == 0) {
            publisher.publishLifecycleEventNotification(trip.getProviderId(), trip, newStatusId, oldStatusId);
        } else {
            publisher.publishLifecycleEventNotification(trip.getApproverId(), trip, newStatusId, oldStatusId, trip.getProviderId());
        }

        if (newStatusId == ServiceStatus.PUBLISHED.getId()) {
            long tripId = trip.getId();
            List<User> users = subscriptionService.getSubscribedUsers(trip.getProviderId());
            for (User user : users) {
                try {
                    emailService.sendProvidersEmail(user.getEmail(), tripId);
                } catch (MessagingException e) {
                    log.error("ERROR while sending message for subscribers: " + e.getMessage());
                }
            }
        }

        return trip;
    }

    public List<com.netcracker.edu.backend.model.Service> searchByRating(SearchTripRequest searchTripRequest) {
        log.debug("Searching best trip in rating by location {} and {}", searchTripRequest.getLocationCountry(), searchTripRequest.getDestinationCountry());

        int limit = 5; //limit for trips in page and in select

        return serviceDao.getSearchByRating(searchTripRequest, limit);
    }

    public List<com.netcracker.edu.backend.model.Service> searchByDiscount(SearchTripRequest searchTripRequest) {
        log.debug("Searching best trip in discount by location {} and {}", searchTripRequest.getLocationCountry(), searchTripRequest.getDestinationCountry());
        int limit = 5; //limit for trips in page and in select
        return serviceDao.getSearchByDiscount(searchTripRequest, limit);
    }

    public List<List<com.netcracker.edu.backend.model.Service>> searchedByLength(SearchTripMultipleRequest searchTripMultipleRequest) {
        List<List<com.netcracker.edu.backend.model.Service>> result = roadSearch(searchTripMultipleRequest, true);

        result.sort((firstService, secondService) -> {

            Double firstDistance = 0D;
            Double secondDistant = 0D;

            for (com.netcracker.edu.backend.model.Service trip : firstService
            ) {

                firstDistance += getDistance(trip);
            }
            for (com.netcracker.edu.backend.model.Service trip : secondService
            ) {

                secondDistant += getDistance(trip);
            }

            return firstDistance.compareTo(secondDistant);
        });
        return result;
    }

    public List<List<com.netcracker.edu.backend.model.Service>> searchedByBundle(SearchTripMultipleRequest searchTripMultipleRequest) {

        roadSearch(searchTripMultipleRequest, true);
        return roadSearch(searchTripMultipleRequest, true);
    }

    private double getDistance(com.netcracker.edu.backend.model.Service trip) {
        if (trip.getLocation() != null) {
            trip.getLocation().setCoordinate(coordinateService.getLocById(trip.getId()));
            trip.getDestination().setCoordinate(coordinateService.getDestById(trip.getId()));
        }
        double ax = trip.getLocation().getCoordinate().getLatitude();
        double ay = trip.getLocation().getCoordinate().getLongitude();
        double bx = trip.getDestination().getCoordinate().getLatitude();
        double by = trip.getDestination().getCoordinate().getLongitude();
        return Math.sqrt(Math.pow(bx - ax, 2) + Math.pow(by - ay, 2));
    }

    //Return list of list most appropriate trips
    public List<List<com.netcracker.edu.backend.model.Service>> roadSearch(SearchTripMultipleRequest searchTripMultipleRequest, boolean oneWay) {

        int priceFrom = searchTripMultipleRequest.getPriceFrom();
        int priceTo = searchTripMultipleRequest.getPriceTo();
        int numOfPeople = searchTripMultipleRequest.getNumberOfPeople();
        String locCountry = searchTripMultipleRequest.getLocationCountry().getName();
        String[] destinationCountry = new String[searchTripMultipleRequest.getDestinationCountry().length];
        for (int i = 0; i < searchTripMultipleRequest.getDestinationCountry().length; i++) {
            destinationCountry[i] = searchTripMultipleRequest.getDestinationCountry()[i].getName();
        }

        int destinationSize = destinationCountry.length;

        //concat sql for select
        String request = completeSqlSearchByCountryStringWithBuffer(destinationSize, oneWay);

        //all country names for request
        String[] allCountryList;

        if (oneWay) {
            allCountryList = new String[destinationSize * 2 + 1];
        } else {
            allCountryList = new String[destinationCountry.length * 2 + 2];
        }

        System.arraycopy(destinationCountry, 0, allCountryList, 0, destinationSize);

        allCountryList[destinationSize] = locCountry;
        for (int i = 0; i < destinationSize; i++) {
            allCountryList[destinationSize + 1 + i] = destinationCountry[i];
        }
        if (!oneWay) {
            allCountryList[destinationSize * 2] = locCountry;
        }

        Object[] objects = new Object[allCountryList.length + 3];
        System.arraycopy(allCountryList, 0, objects, 3, allCountryList.length);
        //

        objects[0] = numOfPeople;
        objects[1] = priceFrom;
        objects[2] = priceTo;

        //select from db
        List<com.netcracker.edu.backend.model.Service> allTrips = serviceDao.getSpecialSearch(request, objects);


        int locId = (int) searchTripMultipleRequest.getLocationCountry().getId();
        List<Long> destId = new ArrayList<>();
        for (int i = 0; i < searchTripMultipleRequest.getDestinationCountry().length; i++) {
            destId.add(searchTripMultipleRequest.getDestinationCountry()[i].getId());
        }

        int length = allTrips.size();
        double[][] graph = createAdjacencyMatrix(length, allTrips);

        //insert into start array start trips(trips with appropriate location)
        List<Way> allRoads = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            if (allTrips.get(i).getLocation().getCountry().getId() == locId) {
                Way locPoint = new Way();
                locPoint.getId().add(i);
                allRoads.add(locPoint);
            }
        }

        List<Way> resultRoads = new ArrayList<>();

        int i = 0;
        getAllRoads(resultRoads, allRoads, i, length, graph, allTrips, locId, destId);

        //checking where there are the bigger num of destination
        int maxDestination = checkRoadWithMaxDestination(resultRoads, destId, allTrips);
        List<List<com.netcracker.edu.backend.model.Service>> resultSet = getRoadsWithAllDestination(resultRoads, maxDestination, allTrips);

        //check for trip with smaller route
        int minLocNum = getMinLocationNumber(resultSet);
        resultSet = getRoadsWithAllDestinationAndMinLocation(resultSet, minLocNum);
        resultSet = getAllListWithoutDublicate(resultSet);


        return resultSet;
    }

    private int checkRoadWithMaxDestination(List<Way> resultRoads, List<Long> destId, List<com.netcracker.edu.backend.model.Service> allTrips) {
        int maxDestination = 0;

        for (Way resultRoad : resultRoads) {
            List<Long> currDestination = new ArrayList<>();
            for (int j = 0; j < resultRoad.getId().size(); j++) {
                //log.debug("     {}  {} - {}", resultRoads.get(k).getId().get(j), allTrips.get(resultRoads.get(k).getId().get(j)).getLocation().getCountry().getName(), allTrips.get(resultRoads.get(k).getId().get(j)).getDestination().getCountry().getName());
                if (destId.contains(allTrips.get(resultRoad.getId().get(j)).getDestination().getCountry().getId())) {
                    if (!(currDestination.contains(allTrips.get(resultRoad.getId().get(j)).getDestination().getCountry().getId()))) {
                        currDestination.add(allTrips.get(resultRoad.getId().get(j)).getDestination().getCountry().getId());
                    }
                }
                //resultRoads.get(k).getId().set(j, (int) allTrips.get(resultRoads.get(k).getId().get(j)).getId());
                resultRoad.setValue(currDestination.size());
                if (currDestination.size() > maxDestination) {
                    maxDestination = currDestination.size();
                }
            }
        }

        return maxDestination;
    }

    private List<List<com.netcracker.edu.backend.model.Service>> getAllListWithoutDublicate(List<List<com.netcracker.edu.backend.model.Service>> allTrips) {
        List<List<com.netcracker.edu.backend.model.Service>> resultSet = new ArrayList<>();

        for (List<com.netcracker.edu.backend.model.Service> service : allTrips) {
            if (!(contains(resultSet, service))) {
                resultSet.add(service);
            }
        }

        return resultSet;
    }

    private boolean contains(List<List<com.netcracker.edu.backend.model.Service>> allTrips, List<com.netcracker.edu.backend.model.Service> curr) {
        for (List<com.netcracker.edu.backend.model.Service> trip : allTrips) {
            int num = 0;
            for (com.netcracker.edu.backend.model.Service service : curr)
                if (trip.contains(service)) {
                    num++;
                }
            if (num == curr.size()) {
                return true;
            }
        }
        return false;
    }

    private int getMinLocationNumber(List<List<com.netcracker.edu.backend.model.Service>> allTrips) {
        int minDestination = 999;
        for (List<com.netcracker.edu.backend.model.Service> allTrip : allTrips) {
            if (allTrip.size() < minDestination) {
                minDestination = allTrip.size();
            }
        }
        return minDestination;
    }

    private List<List<com.netcracker.edu.backend.model.Service>> getRoadsWithAllDestinationAndMinLocation(List<List<com.netcracker.edu.backend.model.Service>> allTrips, int min) {
        List<List<com.netcracker.edu.backend.model.Service>> resultSet = new ArrayList<>();
        for (List<com.netcracker.edu.backend.model.Service> service : allTrips) {
            if (service.size() == min) {
                resultSet.add(service);
            }
        }
        return resultSet;
    }


    private List<List<com.netcracker.edu.backend.model.Service>> getRoadsWithAllDestination(List<Way> resultRoads, int maxDestination, List<com.netcracker.edu.backend.model.Service> allTrips) {
        List<List<com.netcracker.edu.backend.model.Service>> resultSet = new ArrayList<>();
        for (Way resultRoad : resultRoads) {
            if (resultRoad.getValue() == maxDestination) {
                resultSet.add(new ArrayList<>());
                for (int j = 0; j < resultRoad.getId().size(); j++) {
                    //resultSet.get(k).add(allTrips.get(resultRoads.get(k).getId().get(j)));
                    resultSet.get(resultSet.size() - 1).add(allTrips.get(resultRoad.getId().get(j)));
                }
            }
        }

        return resultSet;
    }

    //Breadth-first search with remembering the way
    private void getAllRoads(List<Way> resultRoads, List<Way> allRoads, int i, int length, double[][] graph, List<com.netcracker.edu.backend.model.Service> allTrips, long locId, List<Long> destId) {
        resultRoads.addAll(allRoads);

        while (allRoads.size() - i > 1) {
            Way curr = allRoads.get(i);
            //check if there are adjacent vertices
            int lastVertex = curr.getId().get(curr.getId().size() - 1);
            int numOfVertex = 0;
            for (int j = 0; j < length; j++) {

                if (graph[lastVertex][j] != 0.0) {

                    //if found cycle
                    if (curr.getId().contains(j)) {
                        resultRoads.add(curr);
                    } else if (checkForCicle(curr, destId, allTrips, j)) {
                        resultRoads.add(curr);
                    } else if (checkForAllDestination(curr, destId, allTrips, locId)) {
                        resultRoads.add(curr);
                    } else {

                        Way newWay = new Way();

                        //copy the path
                        for (int z = 0; z < curr.getId().size(); z++) {
                            newWay.getId().add(curr.getId().get(z));
                        }
                        newWay.getId().add(j);

                        //count sum
                        newWay.setValue(curr.getValue() + graph[lastVertex][j]);

                        //show that there was vertex and its not last node
                        numOfVertex++;

                        if (destId.contains(allTrips.get(j).getDestination().getCountry().getId())) {
                            resultRoads.add(newWay);
                        }

                        allRoads.add(newWay);
                    }
                }
            }

            //if adjacent vertices were not - added as a result
            if (numOfVertex == 0) {
                resultRoads.add(curr);
            }

            i++;
        }
    }

    //checking for 2 times in one destination
    private boolean checkForCicle(Way curr, List<Long> destId, List<com.netcracker.edu.backend.model.Service> allTrips, int j) {
        List<Long> usedDestination = new ArrayList<>();
        for (int c = 0; c < curr.getId().size(); c++) {
            if (destId.contains(allTrips.get(curr.getId().get(c)).getDestination().getCountry().getId())) {
                usedDestination.add(allTrips.get(curr.getId().get(c)).getDestination().getCountry().getId());
            }
        }
        return usedDestination.contains(allTrips.get(j).getDestination().getCountry().getId());
    }


    private boolean checkForAllDestination(Way curr, List<Long> destId, List<com.netcracker.edu.backend.model.Service> allTrips, long locId) {
        List<Long> usedDestination = new ArrayList<>();
        for (int c = 0; c < curr.getId().size(); c++) {
            if (destId.contains(allTrips.get(curr.getId().get(c)).getDestination().getCountry().getId())) {
                if (!usedDestination.contains(allTrips.get(curr.getId().get(c)).getDestination().getCountry().getId())) {
                    usedDestination.add(allTrips.get(curr.getId().get(c)).getDestination().getCountry().getId());

                }
            }
        }

        return usedDestination.size() >= destId.size() || allTrips.get(curr.getId().get(curr.getId().size() - 1)).getDestination().getCountry().getId() == (locId);
    }

    private double[][] createAdjacencyMatrix(int length, List<com.netcracker.edu.backend.model.Service> allTrips) {
        double[][] graph = new double[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {

                if (allTrips.get(i).getDestination().getCountry().getId() == allTrips.get(j).getLocation().getCountry().getId()) {
                    graph[i][j] = allTrips.get(i).getPrice().doubleValue();
                }
            }
        }
        return graph;
    }


    public String completeSqlSearchByCountryStringWithBuffer(int numOfDestinationCountry, boolean oneWay) {
        StringBuilder sqlSearchString = new StringBuilder(sqlSearchByCountry + " ");
        for (int i = 0; i < numOfDestinationCountry; i++) {
            sqlSearchString.insert(sqlSearchString.length() - 1, "?, ");
        }
        sqlSearchString.insert(sqlSearchString.length() - 1, "?) OR dest_country.country_name IN ( ");

        for (int i = 0; i < (numOfDestinationCountry - 1); i++) {
            sqlSearchString.insert(sqlSearchString.length() - 1, "?, ");
        }

        if (oneWay) {
            sqlSearchString.insert(sqlSearchString.length() - 1, "?)) ");
        } else {
            sqlSearchString.insert(sqlSearchString.length() - 1, "?,?)) ");
        }

        return sqlSearchString.toString();

    }

    public List<com.netcracker.edu.backend.model.Service> optimalSearch(SearchTripRequest searchTripRequest, long userId) {
        log.debug("Searching best trip in discount by location {} and {}", searchTripRequest.getLocationCountry(), searchTripRequest.getDestinationCountry());
        int limit = 100; //limit for trips in page and in select
        List<com.netcracker.edu.backend.model.Service> ratingTrip = serviceDao.getSearchByRating(searchTripRequest, limit);

        log.debug("Searching best approver by user_id {}", userId);

        long providerId = 0;
        if (userId != 0) {
            providerId = serviceDao.getSearchByYourRaitingProviderId(userId).orElse((long) 0);

            if (providerId == 0) {
                providerId = serviceDao.getSearchByNumOfOrderProviderId(userId).orElse((long) 0);
            }
        }

        List<com.netcracker.edu.backend.model.Service> result = new ArrayList<>();

        if (providerId != 0) {
            for (com.netcracker.edu.backend.model.Service trip : ratingTrip) {
                if (trip.getProviderId() == providerId) {
                    result.add(trip);
                }
            }
            if (result.size() > 5) {
                return result.subList(0, 4);
            }
            return result;

        } else {
            ratingTrip.sort(
                    Comparator.comparing(
                            firstService -> firstService.getRating()
                                    .multiply(new BigDecimal(100)).subtract(firstService.getPrice())));

        }
        return ratingTrip;
    }

    public List<List<com.netcracker.edu.backend.model.Service>> searchedByPrice(SearchTripMultipleRequest searchTripMultipleRequest) {

        List<List<com.netcracker.edu.backend.model.Service>> result = roadSearch(searchTripMultipleRequest, true);

        result.sort((firstService, secondService) -> {
            BigDecimal firstPrice = new BigDecimal(0);
            BigDecimal secondPrice = new BigDecimal(0);

            for (com.netcracker.edu.backend.model.Service trip : firstService
            ) {
                firstPrice = firstPrice.add(trip.getPrice());
            }

            for (com.netcracker.edu.backend.model.Service trip : secondService
            ) {
                secondPrice = secondPrice.add(trip.getPrice());
            }
            return firstPrice.compareTo(secondPrice);
        });
        return result;
    }

    @Data
    static class Way {
        private List<Integer> id = new ArrayList<>();
        private double value;
    }
}
