package com.netcracker.edu.backend.dto.request;

import lombok.*;

import javax.validation.constraints.NotNull;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchTripRequest {

    private String locationCountry;

    private String destinationCountry;

    @NotNull
    private int priceFrom;

    @NotNull
    private int priceTo;

    @NotNull
    private int numberOfPeople;
}
