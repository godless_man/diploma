package com.netcracker.edu.backend.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CarrierDashboardResponse {

    private List<Sales> sales;
    private List<Views> views;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Sales {
        private String salesBy;
        private Long salesAmount;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Views {
        private String viewsBy;
        private Long viewsAmount;
    }
}
