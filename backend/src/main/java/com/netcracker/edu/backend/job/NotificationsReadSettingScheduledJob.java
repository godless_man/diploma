package com.netcracker.edu.backend.job;

import com.netcracker.edu.backend.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class NotificationsReadSettingScheduledJob {

    @Autowired
    private NotificationService notificationService;

    @Scheduled(cron = "0 0 1 * * *")
    public void readNotifications() {
        notificationService.setReadTrueForAllNotifications();
    }
}
