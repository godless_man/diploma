package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.NotificationDao;
import com.netcracker.edu.backend.mapper.NotificationMapper;
import com.netcracker.edu.backend.model.Notification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class NotificationDaoImpl extends GenericDaoImpl<Notification> implements NotificationDao {

    @Value("${db.query.notification.insert}")
    private String sqlNotificationInsert;

    @Value("${db.query.notification.update}")
    private String sqlNotificationUpdate;

    @Value("${db.query.notification.getDetailedOfServiceById}")
    private String sqlGetDetailedNotificationOfServiceById;

    @Value("${db.query.notification.getDetailedOfTroubleTicketById}")
    private String sqlGetDetailedNotificationOfTroubleTicketById;

    @Value("${db.query.notification.getDetailedOfServiceByServiceId}")
    private String sqlGetDetailedNotificationOfServiceByServiceId;

    @Value("${db.query.notification.getDetailedOfTroubleTicketByTroubleTicketId}")
    private String sqlGetDetailedNotificationOfTroubleTicketByTroubleTicketId;

    @Value("${db.query.notification.getAllDetailedOfServiceByUserId}")
    private String sqlGetAllNotificationsDetailedOfServiceByUserId;

    @Value("${db.query.notification.getAllDetailedOfTroubleTicketByUserId}")
    private String sqlGetAllNotificationsDetailedOfTroubleTicketByUserId;

    @Value("${db.query.notification.getAllDetailedOfServiceByUserIdNotRead}")
    private String sqlGetAllNotificationsDetailedOfServiceByUserIdNotRead;

    @Value("${db.query.notification.getAllDetailedOfTroubleTicketByUserIdNotRead}")
    private String sqlGetAllNotificationsDetailedOfTroubleTicketByUserIdNotRead;

    @Value("${db.query.notification.getAllDetailedOfServiceForProviderNotRead}")
    private String sqlGetAllNotificationsDetailedOfServiceForProviderNotRead;

    @Value("${db.query.notification.getAllDetailedOfServiceForApproverNotRead}")
    private String sqlGetAllNotificationsDetailedOfServiceForApproverNotRead;

    @Value("${db.query.notification.getAllDetailedOfTroubleTicketForUserNotRead}")
    private String sqlGetAllNotificationsDetailedOfTroubleTicketForUserNotRead;

    @Value("${db.query.notification.getAllDetailedOfTroubleTicketForApproverNotRead}")
    private String sqlGetAllNotificationsDetailedOfTroubleTicketForApproverNotRead;

    @Value("${db.query.notification.updateAllSetReadTrue}")
    private String sqlUpdateAllNotificationsSetReadTrue;

    @Value("${db.query.notification.updateIfExistsByServiceId}")
    private String sqlUpdateIfExistsByServiceId;

    @Value("${db.query.notification.updateIfExistsByTroubleTicketId}")
    private String sqlUpdateIfExistsByTroubleTicketId;

    public NotificationDaoImpl() {
        super(new NotificationMapper(), TableName.NOTIFICATION);
    }

    @Override
    protected String getInsertSql() {
        return sqlNotificationInsert;
    }


    @Override
    public Optional<Notification> getDetailedOfServiceById(long id) {
        log.debug("Getting notification by id: {}", id);

        List<Notification> notification = jdbcTemplate.query(sqlGetDetailedNotificationOfServiceById, new Object[]{id}, rowMapper);

        log.debug("Found notifications (by id) with ids {}", notification.stream().map(Notification::getId).collect(Collectors.toList()));

        return getSingleElement(notification);
    }

    @Override
    public Optional<Notification> getDetailedOfTroubleTicketById(long id) {
        log.debug("Getting notification by id: {}", id);

        List<Notification> notification = jdbcTemplate.query(sqlGetDetailedNotificationOfTroubleTicketById, new Object[]{id}, rowMapper);

        log.debug("Found notifications (by id) with ids {}", notification.stream().map(Notification::getId).collect(Collectors.toList()));

        return getSingleElement(notification);
    }

    @Override
    public Optional<Notification> getDetailedOfServiceByServiceId(long serviceId) {
        log.debug("Getting notification by serviceId: {}", serviceId);

        List<Notification> notification = jdbcTemplate.query(sqlGetDetailedNotificationOfServiceByServiceId, new Object[]{serviceId}, rowMapper);

        log.debug("Found notifications (by serviceId) with ids {}", notification.stream().map(Notification::getId).collect(Collectors.toList()));

        return getSingleElement(notification);
    }

    @Override
    public Optional<Notification> getDetailedOfTroubleTicketByTroubleTicketId(long troubleTicketId) {
        log.debug("Getting notification by troubleTicketId: {}", troubleTicketId);

        List<Notification> notification = jdbcTemplate.query(sqlGetDetailedNotificationOfTroubleTicketByTroubleTicketId, new Object[]{troubleTicketId}, rowMapper);

        log.debug("Found notifications (by troubleTicketId) with ids {}", notification.stream().map(Notification::getId).collect(Collectors.toList()));

        return getSingleElement(notification);
    }

    @Override
    public List<Notification> getAllDetailedOfServiceByUserId(long userId) {
        log.debug("Getting notifications of service by userId: {}", userId);

        List<Notification> notifications = jdbcTemplate.query(sqlGetAllNotificationsDetailedOfServiceByUserId, new Object[]{userId}, rowMapper);

        log.debug("Found notifications of service (by userId) with ids {}", notifications.stream().map(Notification::getId).collect(Collectors.toList()));

        return notifications;
    }

    @Override
    public List<Notification> getAllDetailedOfTroubleTicketByUserId(long userId) {
        log.debug("Getting notifications of troubleTicket by userId: {}", userId);

        List<Notification> notifications = jdbcTemplate.query(sqlGetAllNotificationsDetailedOfTroubleTicketByUserId, new Object[]{userId}, rowMapper);

        log.debug("Found notifications of troubleTicket (by userId) with ids {}", notifications.stream().map(Notification::getId).collect(Collectors.toList()));

        return notifications;
    }

    @Override
    public List<Notification> getAllDetailedOfServiceByUserIdNotRead(long userId) {
        log.debug("Getting notifications of service not read by userId: {}", userId);

        List<Notification> notifications = jdbcTemplate.query(sqlGetAllNotificationsDetailedOfServiceByUserIdNotRead, new Object[]{userId}, rowMapper);

        log.debug("Found notifications of service (by userId not read) with ids {}", notifications.stream().map(Notification::getId).collect(Collectors.toList()));

        return notifications;
    }

    @Override
    public List<Notification> getAllDetailedOfTroubleTicketByUserIdNotRead(long userId) {
        log.debug("Getting notifications of troubleTicket not read by userId: {}", userId);

        List<Notification> notifications = jdbcTemplate.query(sqlGetAllNotificationsDetailedOfTroubleTicketByUserIdNotRead, new Object[]{userId}, rowMapper);

        log.debug("Found notifications of troubleTicket (by userId not read) with ids {}", notifications.stream().map(Notification::getId).collect(Collectors.toList()));

        return notifications;
    }

    @Override
    public List<Notification> getAllDetailedOfServiceForProviderNotRead(long userId) {
        log.debug("Getting notifications of service not read for provider with id: {}", userId);

        List<Notification> notifications = jdbcTemplate.query(sqlGetAllNotificationsDetailedOfServiceForProviderNotRead, new Object[]{userId, userId}, rowMapper);

        log.debug("Found notifications of service (for provider not read) with ids {}", notifications.stream().map(Notification::getId).collect(Collectors.toList()));

        return notifications;
    }

    @Override
    public List<Notification> getAllDetailedOfServiceForApproverNotRead(long userId) {
        log.debug("Getting notifications of service not read for approver with id: {}", userId);

        List<Notification> notifications = jdbcTemplate.query(sqlGetAllNotificationsDetailedOfServiceForApproverNotRead, new Object[]{userId, userId}, rowMapper);

        log.debug("Found notifications of service (for approver not read) with ids {}", notifications.stream().map(Notification::getId).collect(Collectors.toList()));

        return notifications;
    }

    @Override
    public List<Notification> getAllDetailedOfTroubleTicketForUserNotRead(long userId) {
        log.debug("Getting notifications of troubleTicket not read for user with id: {}", userId);

        List<Notification> notifications = jdbcTemplate.query(sqlGetAllNotificationsDetailedOfTroubleTicketForUserNotRead, new Object[]{userId, userId}, rowMapper);

        log.debug("Found notifications of troubleTicket (for user not read) with ids {}", notifications.stream().map(Notification::getId).collect(Collectors.toList()));

        return notifications;
    }

    @Override
    public List<Notification> getAllDetailedOfTroubleTicketForApproverNotRead(long userId) {
        log.debug("Getting notifications of troubleTicket not read for approver with id: {}", userId);

        List<Notification> notifications = jdbcTemplate.query(sqlGetAllNotificationsDetailedOfTroubleTicketForApproverNotRead, new Object[]{userId, userId}, rowMapper);

        log.debug("Found notifications of troubleTicket (for approver not read) with ids {}", notifications.stream().map(Notification::getId).collect(Collectors.toList()));

        return notifications;
    }

    @Override
    public void setReadTrueForAll() {
        log.debug("Updating notifications setting read for all");

        jdbcTemplate.update(sqlUpdateAllNotificationsSetReadTrue);
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Notification entity) throws SQLException {
        int argNum = 1;
        statement.setLong(argNum++, entity.getUserId());
        if (entity.getServiceId() == null) {
            statement.setNull(argNum++, Types.INTEGER);
            statement.setLong(argNum++, entity.getTroubleTicketId());
        } else {
            statement.setLong(argNum++, entity.getServiceId());
            statement.setNull(argNum++, Types.INTEGER);
        }
        statement.setTimestamp(argNum++, entity.getNotificationTime());
        statement.setBoolean(argNum++, entity.isRead());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlNotificationUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(Notification entity) {
        return new Object[]{entity.getUserId(), entity.getServiceId(), entity.getTroubleTicketId(), entity.getNotificationTime(), entity.isRead()};
    }

    @Override
    public int updateIfExistsByService(Notification notification) {
        log.debug("Updating notification if exists with serviceId: {}", notification.getServiceId());

        return jdbcTemplate.update(sqlUpdateIfExistsByServiceId,
                notification.getUserId(), Timestamp.from(Instant.now()), notification.getServiceId());
    }

    @Override
    public int updateIfExistsByTroubleTicket(Notification notification) {
        log.debug("Updating notification if exists with troubleTicketId: {}", notification.getTroubleTicketId());

        return jdbcTemplate.update(sqlUpdateIfExistsByTroubleTicketId,
                notification.getUserId(), Timestamp.from(Instant.now()), notification.getMessage(), notification.getTroubleTicketId());
    }
}
