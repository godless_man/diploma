package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Notification;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.*;

public class NotificationMapper implements RowMapper<Notification> {

    @Override
    public Notification mapRow(ResultSet resultSet, int i) throws SQLException {
        Notification notification = new Notification();

        notification.setId(resultSet.getLong(NotificationColumns.NOTIFICATION_ID));
        notification.setUserId(resultSet.getLong(NotificationColumns.USER_ID));
        notification.setServiceId(resultSet.getLong(NotificationColumns.SERVICE_ID));
        notification.setTroubleTicketId(resultSet.getLong(NotificationColumns.TROUBLE_TICKET_ID));
        notification.setMessage(resultSet.getString(NotificationColumns.MESSAGE));
        notification.setNotificationTime(resultSet.getTimestamp(NotificationColumns.NOTIFICATION_TIME));
        notification.setRead(resultSet.getBoolean(NotificationColumns.IS_READ));

        notification.setUsername(resultSet.getString(UserColumns.USERNAME));
        notification.setServiceName(resultSet.getString(ServiceColumns.NAME));
        notification.setServiceTypeName(resultSet.getString(ServiceTypeColumns.NAME));
        notification.setServiceStatus(resultSet.getString(ServiceStatusColumns.NAME));
        notification.setTroubleTicketStatus(resultSet.getString(TroubleTicketColumns.TROUBLE_TICKET_STATUS_NAME));

        return notification;
    }
}
