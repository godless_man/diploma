package com.netcracker.edu.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Notification extends Identified {

    private long userId;
    private Long serviceId;
    private Long troubleTicketId;
    private String message;
    private Timestamp notificationTime;
    private boolean isRead;

    private String username;
    private String serviceName;
    private String serviceTypeName;
    private String serviceStatus;
    private String troubleTicketStatus;

    public Notification(long userId, long objectId, Class object) {
        this.userId = userId;
        if (object.equals(Service.class)) {
            this.serviceId = objectId;
            this.troubleTicketId = null;
        } else {
            this.serviceId = null;
            this.troubleTicketId = objectId;
        }
        this.notificationTime = Timestamp.from(Instant.now());
        this.isRead = false;
    }

    public Notification(long userId, long objectId, Class object, String message) {
        this.userId = userId;
        if (object.equals(Service.class)) {
            this.serviceId = objectId;
            this.troubleTicketId = null;
        } else {
            this.serviceId = null;
            this.troubleTicketId = objectId;
        }
        this.message = message;
        this.notificationTime = Timestamp.from(Instant.now());
        this.isRead = false;
    }

    public Notification(long userId, long objectId, String message, String username) {
        this.userId = userId;
        this.serviceId = objectId;
        this.message = message;
        this.username = username;
        this.notificationTime = Timestamp.from(Instant.now());
    }
}
