package com.netcracker.edu.backend.event;

import com.netcracker.edu.backend.model.Service;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CustomServiceLifecycleEvent extends CustomLifecycleEvent<Service> {
    //Created cause of incompatibles of generics and listener methods argument

    public CustomServiceLifecycleEvent(long subjectId, Service object, long oldStatusId, long newStatusId, long recipientId) {
        super(subjectId, object, oldStatusId, newStatusId, recipientId);
    }

    public CustomServiceLifecycleEvent(long subjectId, Service object, long oldStatusId, long newStatusId) {
        super(subjectId, object, oldStatusId, newStatusId);
    }
}
