package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.dto.request.SearchTripRequest;
import com.netcracker.edu.backend.model.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import static com.netcracker.edu.backend.dto.response.AdminDashboardResponse.CountryTrips;

public interface ServiceDao extends GenericDao<Service> {

    List<Service> getAllTrips();

    Optional<Service> getTripById(long id);

    Optional<Service> getTripByIdDetailed(long id);

    List<Service> getSuggestionsByTripId(long id);

    List<Service> getServicesBySuggestionId(long id);

    List<Service> getAllTripsOfStatus(String status);

    List<Service> getTripsByProviderId(String serviceTypeName, long id);

    List<Service> getTripsOfStatusByProviderId(String status, long id);

    List<Service> getTripsByApproverId(long id);

    List<Service> getTripsOfStatusByApproverId(String status, long id);

    List<CountryTrips> getTripsAmountByCountry();

    List<Service> getAllServices();

    Optional<Service> getServiceById(long id);

    List<Service> getAllServicesOfStatus(String status);

    List<Service> getServicesByProviderId(long id);

    List<Service> getServicesOfStatusByProviderId(String status, long id);

    List<Service> getAllBundles();

    List<Service> getAllBundlesDetailed();

    Optional<Service> getBundleById(long id);

    Optional<Service> getBundleByIdDetailed(long id);

    List<Service> getTripsByBundleId(long id);

    List<Service> getAllBundlesOfStatus(String status);

    List<Service> getBundlesByProviderId(long id);

    List<Service> getBundlesOfStatusByProviderId(String status, long id);

    void deleteBundleLinks(long id);

    Optional<Long> countAllTrips();

    Optional<Long> countAllServices();

    Optional<Long> countAllBundles();

    void insertSuggestionLink(long suggestionId, long serviceId);

    List<Service> getTripsByProviderIdWithLocationAndStatus(long id);

    List<Service> getTripsByApproverIdOrNotAssignedWithLocationAndStatus(long id);

    void insertBundleLink(long bundleId, long serviceId);

    List<Service> getSalesBySomePeriod(Timestamp start, Timestamp end, String serviceType);

    List<Service> getTypeStatusAndImg(String serviceTypeName, String serviceStatusName);

    List<Service> getTripByCountriesPeopleAndPrice(String locationListCountries, String destinationListCountries, int numberOfPeople, int priceMin, int priceMax);

    List<Service> getPublishedWithLocationsAndStatus(String serviceType);

    void setRemovedForAllUnderClarification();

    List<Service> getAllServicesPurchasedByUserWithUserId(long id);

    Optional<Long> getSearchByYourRaitingProviderId(long userId);

    List<Service> getSearchByRating(SearchTripRequest searchTripRequest, int limit);

    List<Service> getSearchByDiscount(SearchTripRequest searchTripRequest, int limit);

    Optional<Long> getSearchByNumOfOrderProviderId(long userId);

    List<Service> getSpecialSearch(String request, Object[] objects);

    Optional<Service> getTripsByIdDetailed(long id);

    Optional<Long> countPublishedServices();

    Optional<Long> countUnPublishedServices();
}
