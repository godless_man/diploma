package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.dto.request.ChangeStatusRequest;
import com.netcracker.edu.backend.dto.request.NewTroubleTicketWithMessageRequest;
import com.netcracker.edu.backend.enums.Authority;
import com.netcracker.edu.backend.model.TroubleTicket;
import com.netcracker.edu.backend.model.TroubleTicketFeedback;
import com.netcracker.edu.backend.model.TroubleTicketMessage;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.TroubleTicketFeedbackService;
import com.netcracker.edu.backend.service.TroubleTicketMessageService;
import com.netcracker.edu.backend.service.TroubleTicketService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static com.netcracker.edu.backend.utils.Constants.RoleName;

@RestController
@RequestMapping("/api/account/troubleTickets")
@CrossOrigin("http://localhost:4200")
@Transactional
@Slf4j
public class TroubleTicketController {

    private TroubleTicketService troubleTicketService;
    private TroubleTicketMessageService troubleTicketMessageService;
    private TroubleTicketFeedbackService troubleTicketFeedbackService;

    @Autowired
    public TroubleTicketController(TroubleTicketService troubleTicketService, TroubleTicketMessageService troubleTicketMessageService,
                                   TroubleTicketFeedbackService troubleTicketFeedbackService) {
        this.troubleTicketService = troubleTicketService;
        this.troubleTicketMessageService = troubleTicketMessageService;
        this.troubleTicketFeedbackService = troubleTicketFeedbackService;
    }

    @ApiOperation(value = "loadTroubleTicketById", notes = "Loads detailed trouble ticket by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "TroubleTicket not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<TroubleTicket> loadTroubleTicketById(@PathVariable long id) {
        log.debug("Requesting loading troubleTicket");

        return ResponseEntity.ok(troubleTicketService.getTroubleTicketDetailedById(id));
    }

    @ApiOperation(value = "loadTroubleTicketsByServiceId", notes = "Loads detailed trouble tickets by service id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured({RoleName.ROLE_USER, RoleName.ROLE_APPROVER, RoleName.ROLE_PROVIDER})
    @GetMapping("/service/{id}")
    public ResponseEntity<List<TroubleTicket>> loadTroubleTicketsByServiceId(@PathVariable long id, @CurrentUser UserPrincipal user) {
        log.debug("Requesting loading troubleTickets");

        if (user.getAuthorities().contains(new SimpleGrantedAuthority(Authority.ROLE_APPROVER.getAuthorityName()))) {
            return ResponseEntity.ok(troubleTicketService.getTroubleTicketsByApproverIdOrNotAssigned(user.getId()));
        } else {
            return ResponseEntity.ok(troubleTicketService.getTroubleTicketsByServiceId(id, user.getId()));
        }
    }

    @ApiOperation(value = "loadTroubleTicketMessagesByTroubleTicketId", notes = "Loads trouble ticket messages by ticket id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured({RoleName.ROLE_USER, RoleName.ROLE_APPROVER, RoleName.ROLE_PROVIDER})
    @GetMapping("/messages/{id}")
    public ResponseEntity<List<TroubleTicketMessage>> loadTroubleTicketMessagesByTroubleTicketId(@PathVariable long id) {
        log.debug("Requesting loading troubleTicket messages");

        return ResponseEntity.ok(troubleTicketMessageService.getTroubleTicketMessagesByTroubleTicketId(id));
    }

    @ApiOperation(value = "changeTicketStatus", notes = "Takes ChangeStatusRequest and changing lifecycle step of exact ticket")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured({RoleName.ROLE_USER, RoleName.ROLE_APPROVER, RoleName.ROLE_PROVIDER})
    @PutMapping
    public ResponseEntity<?> changeTicketStatus(@CurrentUser UserPrincipal user, @Valid @RequestBody ChangeStatusRequest request) {
        log.debug("Requesting updating troubleTicket");

        troubleTicketService.changeLifecycleStep(request, user);

        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "postMessage", notes = "Takes ticket message and posting it")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured({RoleName.ROLE_USER, RoleName.ROLE_APPROVER, RoleName.ROLE_PROVIDER})
    @PostMapping("/messages/{id}")
    public ResponseEntity<TroubleTicketMessage> postMessage(@RequestBody String message, @PathVariable long id,
                                                            @CurrentUser UserPrincipal user) {
        log.debug("Requesting posting troubleTicket message");


        return ResponseEntity.ok(troubleTicketMessageService.createTroubleTicketMessage(
                new TroubleTicketMessage(id, user.getId(), message, Timestamp.from(Instant.now())),
                troubleTicketService.getTroubleTicketDetailedById(id), user));
    }

    @ApiOperation(value = "createTroubleTicketWithFirstMessage",
            notes = "Takes NewTroubleTicketWithMessageRequest and inserts it to DB. Also posting first message")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Trip created successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured({RoleName.ROLE_USER, RoleName.ROLE_PROVIDER})
    @PostMapping
    public ResponseEntity<?> createTroubleTicketWithFirstMessage(@Valid @RequestBody NewTroubleTicketWithMessageRequest request,
                                                                 @CurrentUser UserPrincipal user) {
        log.debug("Requesting creating troubleTicket with first message");

        return ResponseEntity.ok(troubleTicketService.initializeTroubleTicket(request, user));
    }

    @ApiOperation(value = "postFeedback", notes = "Takes ticket feedback and posting it")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured({RoleName.ROLE_USER, RoleName.ROLE_PROVIDER})
    @PostMapping("/feedback")
    public ResponseEntity<TroubleTicketFeedback> postFeedback(@Valid @RequestBody TroubleTicketFeedback troubleTicketFeedback) {
        log.debug("Requesting posting troubleTicket feedback");

        return ResponseEntity.ok(troubleTicketFeedbackService.createTroubleTicketFeedback(troubleTicketFeedback));
    }
}
