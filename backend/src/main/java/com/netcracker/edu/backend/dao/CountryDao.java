package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.Country;

import java.util.Optional;

public interface CountryDao extends GenericDao<Country> {

    Optional<Country> getCountryByName(String name);
}
