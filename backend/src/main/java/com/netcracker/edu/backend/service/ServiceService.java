package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ServiceDao;
import com.netcracker.edu.backend.enums.ServiceStatus;
import com.netcracker.edu.backend.enums.ServiceType;
import com.netcracker.edu.backend.exception.AppException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class ServiceService {

    private ServiceDao serviceDao;
    private LocationService locationService;

    @Autowired
    public ServiceService(ServiceDao serviceDao, LocationService locationService) {
        this.serviceDao = serviceDao;
        this.locationService = locationService;
    }

    public com.netcracker.edu.backend.model.Service getServiceById(long id) {
        log.debug("Getting service");

        return serviceDao.getById(id)
                .orElseThrow(() -> {
                    log.error("Service not found with id: {}", id);
                    return new AppException("Service with id " + id + " not found");
                });
    }

    public com.netcracker.edu.backend.model.Service createService(com.netcracker.edu.backend.model.Service service) {
        return serviceDao.insert(service);
    }

    public void updateService(com.netcracker.edu.backend.model.Service service) {
        log.debug("Updating service");

        serviceDao.update(service);
    }

    public void deleteServiceById(long id) {
        log.debug("Deleting service");

        serviceDao.deleteById(id);
    }

    public void deleteService(com.netcracker.edu.backend.model.Service service) {
        log.debug("Deleting service");

        serviceDao.delete(service);
    }

    public List<com.netcracker.edu.backend.model.Service> getAllServices() {
        log.debug("Getting services");

        return serviceDao.getAllServices();
    }

    public List<com.netcracker.edu.backend.model.Service> getAllServicesOfStatus(String status) {
        log.debug("Getting services");

        return serviceDao.getAllServicesOfStatus(status);
    }

    public List<com.netcracker.edu.backend.model.Service> getServicesByProviderId(long id) {
        log.debug("Getting services");

        return serviceDao.getServicesByProviderId(id);
    }

    public List<com.netcracker.edu.backend.model.Service> getServicesOfStatusByProviderId(String status, long id) {
        log.debug("Getting services");

        return serviceDao.getServicesOfStatusByProviderId(status, id);
    }

    public long countAllServices() {
        log.debug("Counting services");

        return serviceDao.countAllServices().orElse(0L);
    }

    public void restoreMappings(com.netcracker.edu.backend.model.Service service) {
        log.debug("Restoring mappings");

        if (service.getApproverId() != null && service.getApproverId() == 0) {
            service.setApproverId(null);
        }

        service.setStatusId(ServiceStatus.getIdFromName(service.getStatus()));

        service.setTypeId(ServiceType.SERVICE.getId());

        locationService.mapLocation(service.getLocation());
        service.setDestinationId(null);
    }

    public void setRemovedForAllUnderClarificationServices() {
        log.debug("Updating service");

        serviceDao.setRemovedForAllUnderClarification();
    }

    public List<com.netcracker.edu.backend.model.Service> getAllServicesPurchasedByUserWithUserId(long id) {
        log.debug("Getting services");

        return serviceDao.getAllServicesPurchasedByUserWithUserId(id);
    }


    public Long countPublishedServices() {
        log.debug("Counting published services");

        return serviceDao.countPublishedServices()
                .orElseThrow(() -> {
                    log.error("Unexpected services error");
                    return new AppException("Unexpected services error");
                });
    }

    public Long countUnPublishedServices() {
        log.debug("Counting unpublished services");

        return serviceDao.countUnPublishedServices()
                .orElseThrow(() -> {
                    log.error("Unexpected services error");
                    return new AppException("Unexpected services error");
                });
    }
}
