package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.Service;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.TripService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.netcracker.edu.backend.utils.Constants.RoleName;
import static com.netcracker.edu.backend.utils.Constants.ServiceStatus;

@RestController
@RequestMapping("/api/account/trips")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class TripController {

    @Autowired
    private TripService tripService;

    @ApiOperation(value = "loadAllTrips", notes = "Loads all trips list")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping
    public ResponseEntity<?> loadAllTrips() {
        log.debug("Requesting all trips");

        List<Service> trips = tripService.getAllTrips();

        return ResponseEntity.ok(trips);
    }

    @ApiOperation(value = "loadAllTripsForCurrentUser", notes = "Loads all trips for current user depending on authority")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/currentUser")
    public ResponseEntity<?> loadAllTripsForCurrentUser(@CurrentUser UserPrincipal currentUser) {
        log.debug("Requesting all trips");

        List<Service> trips = tripService.getTripsDependingOnAuthority(currentUser);

        return ResponseEntity.ok(trips);
    }

    @ApiOperation(value = "getAllByProviderId", notes = "Loads all trips for provider's page by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/forProvider/{id}")
    public ResponseEntity<?> getAllByProviderId(@PathVariable long id) {
        log.debug("Requesting all trips for provider");

        List<Service> trips = tripService.getTripsByProviderId(id);

        return ResponseEntity.ok(trips);
    }

    @ApiOperation(value = "getTrip", notes = "Takes trip id and loads object from DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Service with exact id not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> getTrip(@PathVariable long id) {
        log.debug("Requesting trip");

        return ResponseEntity.ok(tripService.getTripByIdDetailedWithSuggestions(id));
    }

    @ApiOperation(value = "createOrUpdateTrip", notes = "Takes service object and inserts/updates it in DB")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Trip created successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @PostMapping
    public ResponseEntity<?> createOrUpdateTrip(@Valid @RequestBody Service trip, @CurrentUser UserPrincipal currentUser) {
        log.debug("Requesting trip creating (or updating)");

        trip.setProviderId(currentUser.getId());

        Service newTrip = tripService.createOrUpdateTrip(trip, currentUser.getId());

        return ResponseEntity.ok(newTrip);
    }

    @ApiOperation(value = "updateTrip", notes = "Takes trip object and updates exact object in DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @PutMapping
    public ResponseEntity<?> updateTrip(@Valid @RequestBody Service trip) {
        log.debug("Requesting trip updating");

        tripService.updateTrip(trip);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "deleteTrip", notes = "Takes trip id and deletes exact object from DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTrip(@PathVariable long id) {
        log.debug("Requesting trip deleting");

        tripService.deleteTripById(id);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "loadAllTripSortedByImg", notes = "Loads all trips with image")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/withImg")
    public ResponseEntity<?> loadAllTripSortedByImg() {
        log.debug("Requesting trips sorted by img");

        List<Service> trips = tripService.getAllTripsSortedByImg();
        return ResponseEntity.ok(trips);
    }

    @ApiOperation(value = "loadAllTripStatusPublished", notes = "Load all trips of status published")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/published")
    public ResponseEntity<?> loadAllTripStatusPublished() {
        log.debug("Requesting trips sorted status published");

        List<Service> trips = tripService.getAllTripsOfStatus(ServiceStatus.PUBLISHED);
        return ResponseEntity.ok(trips);
    }

    @ApiOperation(value = "loadAllServicesWithDetail", notes = "Loads all service with details")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/withDetail")
    public ResponseEntity<?> loadAllServicesWithDetail() {
        log.debug("Requesting trips with details");

        return ResponseEntity.ok(tripService.getAllPublishedTripsWithDetails());
    }
}
