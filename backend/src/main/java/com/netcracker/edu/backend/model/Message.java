package com.netcracker.edu.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {

    private String username;
    @NotNull
    private long userId;
    private String role;
    @NotBlank
    private String message;
    private Timestamp time;
    private long destinationId;
    private String email;
}
