package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.CoordinateDao;
import com.netcracker.edu.backend.mapper.CoordinateMapper;
import com.netcracker.edu.backend.model.Coordinate;
import com.netcracker.edu.backend.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
@Slf4j
public class CoordinateDaoImpl extends GenericDaoImpl<Coordinate> implements CoordinateDao {

    @Value("${db.query.coordinates.getDestinationCoordinateByServiceId}")
    private String sqlGetDestinationCoordinateByServiceId;

    @Value("${db.query.coordinates.getLocationCoordinateByServiceId}")
    private String sqlGetLocationCoordinateByServiceId;

    @Value("${db.query.coordinates.getCoordinateByCityId}")
    private String sqlGetCoordinateByCityId;

    public CoordinateDaoImpl() {
        super(new CoordinateMapper(), Constants.TableName.COORDINATES);
    }

    @Override
    protected String getInsertSql() {
        return null;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Coordinate entity) throws SQLException {
        return null;
    }

    @Override
    protected String getUpdateSql() {
        return null;
    }

    @Override
    protected Object[] getArgsForUpdate(Coordinate entity) {
        return new Object[0];
    }

    public Optional<Coordinate> getLocationByServiceId(long id) {
        List<Coordinate> loc = jdbcTemplate.query(sqlGetLocationCoordinateByServiceId, new Object[]{id}, rowMapper);
        return getSingleElement(loc);
    }

    public Optional<Coordinate> getDestinationByServiceId(long id) {
        List<Coordinate> dest = jdbcTemplate.query(sqlGetDestinationCoordinateByServiceId, new Object[]{id}, rowMapper);
        return getSingleElement(dest);
    }

    @Override
    public Optional<Coordinate> getCoordinateByCityId(long id) {
        List<Coordinate> coordinate = jdbcTemplate.query(sqlGetCoordinateByCityId, new Object[]{id}, rowMapper);
        return getSingleElement(coordinate);
    }
}
