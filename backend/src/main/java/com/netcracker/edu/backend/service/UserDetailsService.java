package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.UserDetailsDao;
import com.netcracker.edu.backend.model.UserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class UserDetailsService {

    @Autowired
    private UserDetailsDao userDetailsDao;

    public UserDetails createUserDetails(UserDetails userDetails) {
        log.debug("Creating userDetails");

        return userDetailsDao.insert(userDetails);
    }

    public void deleteUserDetailsById(long id) {
        log.debug("Deleting userDetails");

        userDetailsDao.deleteById(id);
    }

    public void deleteUserDetails(UserDetails userDetails) {
        log.debug("Deleting userDetails");

        userDetailsDao.delete(userDetails);
    }

    public void updateUserDetails(UserDetails userDetails) {
        log.debug("Updating userDetails");

        userDetailsDao.update(userDetails);
    }

    public List<UserDetails> getAllUserDetails() {
        log.debug("Getting all userDetails");

        return userDetailsDao.getAll();
    }
}
