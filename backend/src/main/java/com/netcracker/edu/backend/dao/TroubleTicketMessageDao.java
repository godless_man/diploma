package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.TroubleTicketMessage;

import java.util.List;

public interface TroubleTicketMessageDao extends GenericDao<TroubleTicketMessage> {

    List<TroubleTicketMessage> getTroubleTicketMessagesByTroubleTicketId(long id);
}
