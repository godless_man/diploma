package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ServiceDao;
import com.netcracker.edu.backend.enums.ServiceStatus;
import com.netcracker.edu.backend.enums.ServiceType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Slf4j
public class SuggestionService {

    private ServiceDao serviceDao;
    private LocationService locationService;

    @Autowired
    public SuggestionService(ServiceDao serviceDao, LocationService locationService) {
        this.serviceDao = serviceDao;
        this.locationService = locationService;
    }

    public void addSuggestionLink(long suggestionId, long serviceId) {
        log.debug("Adding suggestion link");

        serviceDao.insertSuggestionLink(suggestionId, serviceId);
    }

    public void restoreMappings(com.netcracker.edu.backend.model.Service suggestion) {
        log.debug("Restoring mappings");

        if (suggestion.getApproverId() != null && suggestion.getApproverId() == 0) {
            suggestion.setApproverId(null);
        }

        suggestion.setStatusId(ServiceStatus.getIdFromName(suggestion.getStatus()));

        suggestion.setTypeId(ServiceType.SUGGESTION.getId());

        locationService.mapLocation(suggestion.getLocation());
        suggestion.setDestinationId(null);
    }
}
