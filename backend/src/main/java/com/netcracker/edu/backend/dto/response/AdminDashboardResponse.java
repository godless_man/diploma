package com.netcracker.edu.backend.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminDashboardResponse {

    private List<Distribution> servicesDistribution;
    private List<UsersPerYear> usersPerYear;
    private List<CountryTrips> countryTrips;
    private List<UsersPerYear> carriersPerYear;
    private List<CostsPerInterval> costsPerInterval;
    private List<TroubleTicketStatistic> troubleTicketStatistic;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Distribution {
        private String serviceType;
        private Long distAmount;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UsersPerYear {
        private String year;
        private Long usersAmount;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CountryTrips {
        private String countryName;
        private Long amount;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CostsPerInterval {
        private String interval;
        private Long cost;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TroubleTicketStatistic {
        private String state;
        private Long amount;
    }
}
