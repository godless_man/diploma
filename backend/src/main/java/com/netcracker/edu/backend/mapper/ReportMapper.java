package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.dto.response.AdminReportResponse;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReportMapper implements RowMapper<AdminReportResponse> {
    @Override
    public AdminReportResponse mapRow(ResultSet resultSet, int i) throws SQLException {

        AdminReportResponse adminReportResponse = new AdminReportResponse();

        adminReportResponse.setLocation(resultSet.getString("city_name"));

        float price = resultSet.getFloat("sumPrice");
        float fixedDiscount = resultSet.getFloat("fixed_discount");
        float percentageDiscount = resultSet.getFloat("percentageDiscount");

        adminReportResponse.setPrice(price - fixedDiscount - percentageDiscount);
        adminReportResponse.setNumberTour(resultSet.getInt("number_of_purchases"));

        return adminReportResponse;
    }
}
