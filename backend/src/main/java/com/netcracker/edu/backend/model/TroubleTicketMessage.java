package com.netcracker.edu.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TroubleTicketMessage extends Identified {

    private long troubleTicketId;
    private long userId;
    private String message;
    private Timestamp messageTime;

    private String username;
    private String email;

    public TroubleTicketMessage(long troubleTicketId, long userId, String message, Timestamp messageTime) {
        this.troubleTicketId = troubleTicketId;
        this.userId = userId;
        this.message = message;
        this.messageTime = messageTime;
    }
}
