package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.Token;
import com.netcracker.edu.backend.model.User;

import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;

public interface TokenDao extends GenericDao<Token> {

    Optional<Token> findByToken(String token);

    Token findByUser(User user);

    Stream<Token> findAllByExpiryDateLessThan(Date now);

    void deleteByExpiryDateLessThan(Date now);
}
