package com.netcracker.edu.backend.enums;

import lombok.Getter;

@Getter
public enum TroubleTicketStatus {

    OPEN(1, "Open"),
    IN_PROGRESS(2, "In progress"),
    ANSWERED(3, "Answered"),
    REOPENED(4, "Reopened"),
    RATED(5, "Rated"),
    FEEDBACK(6, "Feedback");

    private final long id;
    private final String troubleTicketStatusName;

    TroubleTicketStatus(long id, String troubleTicketStatusName) {
        this.id = id;
        this.troubleTicketStatusName = troubleTicketStatusName;
    }

    public static String getNameFromId(long id) {

        for (TroubleTicketStatus serviceStatus : values()) {
            if (serviceStatus.getId() == id) {
                return serviceStatus.getTroubleTicketStatusName();
            }
        }

        return "UNKNOWN";
    }

    public static long getIdFromName(String name) {

        for (TroubleTicketStatus serviceStatus : values()) {
            if (serviceStatus.getTroubleTicketStatusName().equals(name)) {
                return serviceStatus.getId();
            }
        }

        return 0;
    }
}
