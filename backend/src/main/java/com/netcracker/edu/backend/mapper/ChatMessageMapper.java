package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.ChatMessage;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.ChatMessageColumns;

public class ChatMessageMapper implements RowMapper<ChatMessage> {

    @Override
    public ChatMessage mapRow(ResultSet resultSet, int i) throws SQLException {
        ChatMessage chatMessage = new ChatMessage();

        chatMessage.setId(resultSet.getLong(ChatMessageColumns.CHAT_MESSAGE_ID));
        chatMessage.setChatId(resultSet.getLong(ChatMessageColumns.CHAT_ID));
        chatMessage.setUserId(resultSet.getLong(ChatMessageColumns.USER_ID));
        chatMessage.setMessage(resultSet.getString(ChatMessageColumns.MESSAGE));
        chatMessage.setMessageTime(resultSet.getTimestamp(ChatMessageColumns.MESSAGE_TIME));

        chatMessage.setUsername(resultSet.getString(ChatMessageColumns.USERNAME));
        chatMessage.setOwnerId(resultSet.getLong(ChatMessageColumns.OWNER_ID));
        chatMessage.setEmail(resultSet.getString(ChatMessageColumns.EMAIL));

        return chatMessage;
    }
}
