package com.netcracker.edu.backend.event.publisher;

import com.netcracker.edu.backend.event.CustomServiceLifecycleEvent;
import com.netcracker.edu.backend.event.CustomTroubleTicketLifecycleEvent;
import com.netcracker.edu.backend.model.Service;
import com.netcracker.edu.backend.model.TroubleTicket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
@Slf4j
public class LifecycleEventPublisher {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public <T> void publishLifecycleEventNotification(long subjectId, T object, long newStatusId, long oldStatusId) {
        log.debug("Publishing event for object: {}", object);

        if (object.getClass().equals(Service.class)) {
            applicationEventPublisher.publishEvent(new CustomServiceLifecycleEvent(subjectId, (Service) object,
                    oldStatusId, newStatusId));
        } else if (object.getClass().equals(TroubleTicket.class)) {
            applicationEventPublisher.publishEvent(new CustomTroubleTicketLifecycleEvent(subjectId, (TroubleTicket) object,
                    oldStatusId, newStatusId));
        }
    }

    public <T> void publishLifecycleEventNotification(long subjectId, T object, long newStatusId, long oldStatusId, long recipientId) {
        log.debug("Publishing event for object: {}", object);

        if (object.getClass().equals(Service.class)) {
            applicationEventPublisher.publishEvent(new CustomServiceLifecycleEvent(subjectId, (Service) object,
                    oldStatusId, newStatusId, recipientId));
        } else if (object.getClass().equals(TroubleTicket.class)) {
            applicationEventPublisher.publishEvent(new CustomTroubleTicketLifecycleEvent(subjectId, (TroubleTicket) object,
                    oldStatusId, newStatusId, recipientId));
        }
    }
}
