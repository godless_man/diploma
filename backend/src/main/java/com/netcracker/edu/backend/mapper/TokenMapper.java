package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Token;
import com.netcracker.edu.backend.utils.Constants;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TokenMapper implements RowMapper<Token> {

    @Override
    public Token mapRow(ResultSet resultSet, int i) throws SQLException {
        Token token = new Token();

        token.setId(resultSet.getLong(Constants.VerificationTokenColumns.ID));
        token.setToken(resultSet.getString(Constants.VerificationTokenColumns.TOKEN));
        token.setUserID(resultSet.getLong(Constants.VerificationTokenColumns.USER_ID));
        token.setExpiryDate(resultSet.getTimestamp(Constants.VerificationTokenColumns.EXPIRE_DATE));
        return token;
    }
}
