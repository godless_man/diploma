package com.netcracker.edu.backend.mapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.dto.response.AdminDashboardResponse.CountryTrips;
import static com.netcracker.edu.backend.utils.Constants.CountryColumns;
import static com.netcracker.edu.backend.utils.Constants.GenericColumnNames;

public class CountryTripsMapper implements RowMapper<CountryTrips> {
    @Override
    public CountryTrips mapRow(ResultSet resultSet, int i) throws SQLException {

        CountryTrips countryTrips = new CountryTrips();

        countryTrips.setCountryName(resultSet.getString(CountryColumns.NAME));
        countryTrips.setAmount(resultSet.getLong(GenericColumnNames.COUNT));

        return countryTrips;
    }
}
