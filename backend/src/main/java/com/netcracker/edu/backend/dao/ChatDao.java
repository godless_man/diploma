package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.Chat;

import java.util.List;
import java.util.Optional;

public interface ChatDao extends GenericDao<Chat> {

    Optional<Chat> getChatByUserId(long id);

    List<Chat> getAllChatsForApprover(long id);
}
