package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.Notification;
import com.netcracker.edu.backend.service.NotificationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.netcracker.edu.backend.utils.Constants.RoleName;

@RestController
@RequestMapping("/api/notifications")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @ApiOperation(value = "getAllNotReadNotificationsForApprover", notes = "Loads all not read notifications for approver")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_APPROVER)
    @GetMapping("/approver/{id}")
    public ResponseEntity<?> getAllNotReadNotificationsForApprover(@PathVariable long id) {
        log.debug("Getting all not read notification for approver in {}: ", this.getClass().getName());

        List<Notification> servicesNotifications = notificationService.getAllNotificationsDetailedOfServiceForApproverNotRead(id);
        List<Notification> troubleTicketsNotifications = notificationService.getAllNotificationsDetailedOfTroubleTicketForApproverNotRead(id);
        List<Notification> allNotifications = Stream.concat(servicesNotifications.stream(), troubleTicketsNotifications.stream()).collect(Collectors.toList());
        return ResponseEntity.ok(allNotifications);
    }

    @ApiOperation(value = "getAllNotReadNotificationsForProvider", notes = "Loads all not read notifications for provider")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_PROVIDER)
    @GetMapping("/provider/{id}")
    public ResponseEntity<?> getAllNotReadNotificationsForProvider(@PathVariable long id) {
        log.debug("Getting all not read notification for provider in {}: ", this.getClass().getName());

        return ResponseEntity.ok(notificationService.getAllNotificationsDetailedOfServiceForProviderNotRead(id));
    }

    @ApiOperation(value = "getAllNotReadNotificationsForUser", notes = "Loads all not read notifications for user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @Secured(RoleName.ROLE_USER)
    @GetMapping("/user/{id}")
    public ResponseEntity<?> getAllNotReadNotificationsForUser(@PathVariable long id) {
        log.debug("Getting all not read notification for user in {}: ", this.getClass().getName());

        return ResponseEntity.ok(notificationService.getAllNotificationsDetailedOfTroubleTicketForUserNotRead(id));
    }
}
