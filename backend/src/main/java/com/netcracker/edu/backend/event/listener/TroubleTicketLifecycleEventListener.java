package com.netcracker.edu.backend.event.listener;

import com.netcracker.edu.backend.event.CustomTroubleTicketLifecycleEvent;
import com.netcracker.edu.backend.exception.BadRequestException;
import com.netcracker.edu.backend.model.Notification;
import com.netcracker.edu.backend.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
@Slf4j
public class TroubleTicketLifecycleEventListener {

    @Autowired
    private NotificationService notificationService;

    private static final String WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE = "Wrong state changing due to lifecycle";

    /* *
     * CREATION STEP
     * */

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.wrongCreationCheck(#event)")
    public void onWrongCreationEvent(CustomTroubleTicketLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.openStepCheck(#event)")
    public void onOpenEvent(CustomTroubleTicketLifecycleEvent event) {
        log.debug("Listening onOpenEvent");
        notificationService.sendNotificationToApprovers(buildNotification(event));
    }

    //----------------------------------------

    /* *
     * ASSIGNMENT STEP
     * */

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.wrongAfterOpenStepCheck(#event)")
    public void onWrongAfterOpenEvent(CustomTroubleTicketLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.inProgressStepCheck(#event)")
    public void onInProgressEvent(CustomTroubleTicketLifecycleEvent event) {
        log.debug("Listening onInProgressEvent");
        notificationService.sendNotificationToUser(buildNotification(event), event.getRecipientId());
    }

    //----------------------------------------

    /* *
     * ANSWERED STEP
     * */

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.wrongAfterInProgressStepCheck(#event)")
    public void onWrongAfterInProgressEvent(CustomTroubleTicketLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.answeredStepCheck(#event)")
    public void onAnsweredEvent(CustomTroubleTicketLifecycleEvent event) {
        log.debug("Listening onAnsweredEvent");
        notificationService.sendNotificationToApprover(buildNotification(event), event.getRecipientId());
    }

    //----------------------------------------

    /* *
     * FEEDBACK/REOPEN STEP
     * */

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.wrongAfterAnsweredStepCheck(#event)")
    public void onWrongAfterAnsweredEvent(CustomTroubleTicketLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.reopenedStepCheck(#event)")
    public void onReopenedEvent(CustomTroubleTicketLifecycleEvent event) {
        log.debug("Listening onReopenedEvent");
        notificationService.sendNotificationToApprovers(buildNotification(event));
    }

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.feedbackStepCheck(#event)")
    public void onRatedOrFeedbackEvent(CustomTroubleTicketLifecycleEvent event) {
        log.debug("Listening onRatedOrFeedbackEvent");
        notificationService.sendNotificationToApprover(buildNotification(event), event.getRecipientId());
    }

    //----------------------------------------

    /* *
     * REASSIGNMENT STEP
     * */

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.wrongAfterReopenedStepCheck(#event)")
    public void onWrongAfterReopenedEvent(CustomTroubleTicketLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.reassignedStepCheck(#event)")
    public void onInProgressAfterReopenedEvent(CustomTroubleTicketLifecycleEvent event) {
        log.debug("Listening onInProgressAfterReopenedEvent");
        notificationService.sendNotificationToUser(buildNotification(event), event.getRecipientId());
    }

    //----------------------------------------

    /* *
     * ADDITIONAL STEP
     * */

    @EventListener(condition = "@listenerTroubleTicketLifecycleConditionService.wrongAfterFeedbackStepCheck(#event)")
    public void onWrongAfterRatedOrFeedbackEvent(CustomTroubleTicketLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    //---------------------------------------

    private Notification buildNotification(CustomTroubleTicketLifecycleEvent event) {
        return new Notification(event.getSubjectId(), event.getObject().getId(), event.getObject().getClass());
    }
}
