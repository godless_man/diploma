package com.netcracker.edu.backend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Location extends Identified {

    private long countryId;
    private Long cityId;
    private Long streetId;

    private String country;
    private String city;
    private String street;

    public Location(long countryId) {
        this.countryId = countryId;
    }

    public Location(long countryId, Long cityId, Long streetId) {
        this.countryId = countryId;
        this.cityId = cityId;
        this.streetId = streetId;
    }
}
