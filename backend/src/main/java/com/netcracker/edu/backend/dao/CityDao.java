package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.City;

import java.util.List;
import java.util.Optional;

public interface CityDao extends GenericDao<City> {

    Optional<City> getCityByName(String name, long countryId);

    List<City> getCitiesByCountryId(long id);
}
