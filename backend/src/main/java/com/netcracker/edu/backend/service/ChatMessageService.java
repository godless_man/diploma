package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ChatMessageDao;
import com.netcracker.edu.backend.model.ChatMessage;
import com.netcracker.edu.backend.model.Notification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class ChatMessageService {

    @Autowired
    private ChatMessageDao chatMessageDao;
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    public ChatMessage createChatMessage(ChatMessage chatMessage) {
        log.debug("Creating chat message");
        if (chatMessage.getUserId() != chatMessage.getOwnerId()) {
            messagingTemplate.convertAndSend("/notification/messages/" + chatMessage.getOwnerId(), new Notification(chatMessage.getUserId(), chatMessage.getChatId(), "MESSAGE", chatMessage.getUsername()));
        } else {
            messagingTemplate.convertAndSend("/notification/approvers", new Notification(chatMessage.getUserId(), chatMessage.getChatId(), "MESSAGE", chatMessage.getUsername()));
        }
        return chatMessageDao.insert(chatMessage);
    }

    public void updateChatMessage(ChatMessage chatMessage) {
        log.debug("Updating chat message");
        chatMessageDao.update(chatMessage);
    }

    public List<ChatMessage> getChatMessageByChatId(long id) {
        log.debug("Getting chat messages");
        return chatMessageDao.getAllChatMessagesByChatId(id);
    }
}
