package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dto.response.AdminDashboardResponse;
import com.netcracker.edu.backend.dto.response.ApproverDashboardResponse;
import com.netcracker.edu.backend.dto.response.CarrierDashboardResponse;
import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.model.UserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.dto.response.AdminDashboardResponse.*;
import static com.netcracker.edu.backend.dto.response.CarrierDashboardResponse.Sales;
import static com.netcracker.edu.backend.dto.response.CarrierDashboardResponse.Views;
import static com.netcracker.edu.backend.utils.Constants.*;

@Service
@Transactional
@Slf4j
public class DashboardService {

    private UserService userService;
    private UserDetailsService userDetailsService;
    private ServiceService serviceService;
    private TripService tripService;
    private BundleService bundleService;
    private OrderService orderService;
    private TroubleTicketService troubleTicketService;
    private ViewService viewService;

    @Autowired
    public DashboardService(UserService userService, UserDetailsService userDetailsService, ServiceService serviceService,
                            TripService tripService, BundleService bundleService, OrderService orderService, TroubleTicketService troubleTicketService,
                            ViewService viewService) {
        this.userService = userService;
        this.userDetailsService = userDetailsService;
        this.serviceService = serviceService;
        this.tripService = tripService;
        this.bundleService = bundleService;
        this.orderService = orderService;
        this.troubleTicketService = troubleTicketService;
        this.viewService = viewService;
    }

    public AdminDashboardResponse getAdminDashboardResponse() {
        log.debug("Getting admin dashboard response");
        AdminDashboardResponse response = new AdminDashboardResponse();
        response.setUsersPerYear(getUsersPerYear());
        response.setServicesDistribution(getServicesDistribution());
        response.setCountryTrips(getCountryTrips());
        response.setCarriersPerYear(getCarriersPerYear());
        response.setCostsPerInterval(getCostsPerInterval());
        response.setTroubleTicketStatistic(getTroubleTicketStatistic());
        return response;
    }

    public CarrierDashboardResponse getCarrierDashboardResponse(long id) {
        log.debug("Getting carrier dashboard response");
        CarrierDashboardResponse response = new CarrierDashboardResponse();
        response.setSales(getSales(id));
        response.setViews((getViews(id)));
        return response;
    }

    public ApproverDashboardResponse getApproverDashboardResponse(long id) {
        log.debug("Getting approver dashboard response");
        ApproverDashboardResponse response = new ApproverDashboardResponse();
        response.setProductivityDiagram(getProductivityDiagram());
        response.setWaitingDiagram(getWaitingDiagram());
        return response;
    }

    private List<UsersPerYear> getUsersPerYear() {
        log.debug("Getting users per year");
        List<UsersPerYear> result = new ArrayList<>();

        Map<Integer, Long> usersPerYear = userDetailsService.getAllUserDetails()
                .stream()
                .parallel()
                .map(UserDetails::getRegistrationDate)
                .map(Timestamp::toLocalDateTime)
                .map(LocalDateTime::getYear)
                .sorted()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        usersPerYear.forEach((integer, aLong) -> result.add(new UsersPerYear(integer.toString(), aLong)));

        return result;
    }

    private List<UsersPerYear> getCarriersPerYear() {
        log.debug("Getting carriers per year");
        List<UsersPerYear> result = new ArrayList<>();

        Map<Integer, Long> usersPerYear = userService.getDetailedUserByRole(RoleName.ROLE_PROVIDER)
                .stream()
                .parallel()
                .map(User::getDetails)
                .map(UserDetails::getRegistrationDate)
                .map(Timestamp::toLocalDateTime)
                .map(LocalDateTime::getYear)
                .sorted()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        usersPerYear.forEach((integer, aLong) -> result.add(new UsersPerYear(integer.toString(), aLong)));

        return result;
    }

    private List<Distribution> getServicesDistribution() {
        log.debug("Getting services distribution");

        return new ArrayList<>(Arrays.asList(new Distribution(ServiceType.SERVICE, serviceService.countAllServices()),
                new Distribution(ServiceType.TRIP, tripService.countAllTrips()),
                new Distribution(ServiceType.BUNDLE, bundleService.countAllBundles())));
    }

    private List<CountryTrips> getCountryTrips() {
        log.debug("Getting country-trips");
        return tripService.getTripsAmountByCountry();
    }

    private List<CostsPerInterval> getCostsPerInterval() {
        log.debug("Getting costs per interval");
        return new ArrayList<>(Arrays.asList(new CostsPerInterval(DashboardInterval.PER_DAY, orderService.getOrdersWithPriceToday()
                        .stream()
                        .mapToLong(order -> order.getPrice().longValue())
                        .sum()),
                new CostsPerInterval(DashboardInterval.PER_WEEK, orderService.getOrdersWithPriceWeek().stream()
                        .mapToLong(order -> order.getPrice().longValue())
                        .sum()),
                new CostsPerInterval(DashboardInterval.PER_MONTH, orderService.getOrdersWithPriceMonth().stream()
                        .mapToLong(order -> order.getPrice().longValue())
                        .sum())));
    }

    private List<TroubleTicketStatistic> getTroubleTicketStatistic() {
        return new ArrayList<>(Arrays.asList(new TroubleTicketStatistic("unsolved", troubleTicketService.countUnsolvedTroubleTickets()),
                new TroubleTicketStatistic("solved", troubleTicketService.countSolvedTroubleTickets())));
    }

    private List<Sales> getSales(long id) {
        log.debug("Getting sales");
        return new ArrayList<>(Arrays.asList(new Sales("By Trips", orderService.getSalesByTrips(id)),
                new Sales("By Services", orderService.getSalesByServices(id)),
                new Sales(DashboardInterval.PER_WEEK, orderService.getSalesByWeek(id))));
        //Mocked temporarily due to non-filled DB
        /*return new ArrayList<>(Arrays.asList(new Sales("By Trips", 10L),
                new Sales("By Services", 20L),
                new Sales(DashboardInterval.PER_WEEK, 30L)));*/
    }

    private List<Views> getViews(long id) {
        log.debug("Getting views");
        return new ArrayList<>(Arrays.asList(new Views("By Trips", viewService.countViewsByAllTrips(id)),
                new Views("By Services", viewService.countAllViews(id)),
                new Views(DashboardInterval.PER_WEEK, viewService.countAllViewsByWeek(id))));
    }

    private ApproverDashboardResponse.WaitingDiagram getWaitingDiagram() {
        log.debug("Getting waiting diagram");
        return new ApproverDashboardResponse.WaitingDiagram(serviceService.countUnPublishedServices(),troubleTicketService.countUnsolvedTroubleTickets());
    }

    private ApproverDashboardResponse.ProductivityDiagram getProductivityDiagram() {
        log.debug("Getting productivity diagram");
        return new ApproverDashboardResponse.ProductivityDiagram(serviceService.countPublishedServices(), troubleTicketService.countSolvedTroubleTickets());
    }
}
