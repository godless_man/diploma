package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.enums.TroubleTicketStatus;
import com.netcracker.edu.backend.event.CustomLifecycleEvent;
import com.netcracker.edu.backend.model.TroubleTicket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Slf4j
public class ListenerTroubleTicketLifecycleConditionService {

    /* *
     * CREATION STEP
     * */

    public boolean wrongCreationCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: wrongCreationCheck");

        return event.getOldStatusId() == 0
                && event.getNewStatusId() != TroubleTicketStatus.OPEN.getId();
    }

    public boolean openStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: openStepCheck");

        return event.getOldStatusId() == 0
                && event.getNewStatusId() == TroubleTicketStatus.OPEN.getId();
    }

    //----------------------------------------

    /* *
     * ASSIGNMENT STEP
     * */

    public boolean wrongAfterOpenStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: wrongAfterOpenStepCheck");

        return event.getOldStatusId() == TroubleTicketStatus.OPEN.getId()
                && event.getNewStatusId() != TroubleTicketStatus.IN_PROGRESS.getId();
    }

    public boolean inProgressStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: inProgressStepCheck");

        return event.getOldStatusId() == TroubleTicketStatus.OPEN.getId()
                && event.getNewStatusId() == TroubleTicketStatus.IN_PROGRESS.getId();
    }

    //----------------------------------------

    /* *
     * ANSWERED STEP
     * */

    public boolean wrongAfterInProgressStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: wrongAfterInProgressStepCheck");

        return event.getOldStatusId() == TroubleTicketStatus.IN_PROGRESS.getId()
                && event.getNewStatusId() != TroubleTicketStatus.ANSWERED.getId();
    }

    public boolean answeredStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: answeredStepCheck");

        return event.getOldStatusId() == TroubleTicketStatus.IN_PROGRESS.getId()
                && event.getNewStatusId() == TroubleTicketStatus.ANSWERED.getId();
    }

    //----------------------------------------

    /* *
     * FEEDBACK/REOPEN STEP
     * */

    public boolean wrongAfterAnsweredStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: wrongAfterAnsweredStepCheck");

        return event.getOldStatusId() == TroubleTicketStatus.ANSWERED.getId()
                && event.getNewStatusId() != TroubleTicketStatus.REOPENED.getId()
                && event.getNewStatusId() != TroubleTicketStatus.RATED.getId()
                && event.getNewStatusId() != TroubleTicketStatus.FEEDBACK.getId();
    }

    public boolean reopenedStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: reopenedStepCheck");

        return event.getOldStatusId() == TroubleTicketStatus.ANSWERED.getId()
                && event.getNewStatusId() == TroubleTicketStatus.REOPENED.getId();
    }

    public boolean feedbackStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: feedbackStepCheck");

        return event.getOldStatusId() == TroubleTicketStatus.ANSWERED.getId()
                && event.getNewStatusId() == TroubleTicketStatus.RATED.getId()
                && event.getNewStatusId() == TroubleTicketStatus.FEEDBACK.getId();
    }

    //----------------------------------------

    /* *
     * REASSIGNMENT STEP
     * */

    public boolean wrongAfterReopenedStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: wrongAfterReopenedStepCheck");

        return event.getOldStatusId() == TroubleTicketStatus.REOPENED.getId()
                && event.getNewStatusId() != TroubleTicketStatus.IN_PROGRESS.getId();
    }

    public boolean reassignedStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: reassignedStepCheck");

        return event.getOldStatusId() == TroubleTicketStatus.REOPENED.getId()
                && event.getNewStatusId() == TroubleTicketStatus.IN_PROGRESS.getId();
    }

    //----------------------------------------

    /* *
     * ADDITIONAL STEP
     * */

    public boolean wrongAfterFeedbackStepCheck(CustomLifecycleEvent<TroubleTicket> event) {
        log.debug("condition: wrongAfterFeedbackStepCheck");

        return event.getOldStatusId() == TroubleTicketStatus.RATED.getId()
                || event.getOldStatusId() == TroubleTicketStatus.FEEDBACK.getId();
    }
}
