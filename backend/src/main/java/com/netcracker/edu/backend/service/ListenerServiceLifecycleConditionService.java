package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.enums.ServiceStatus;
import com.netcracker.edu.backend.event.CustomLifecycleEvent;
import com.netcracker.edu.backend.model.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

@org.springframework.stereotype.Service
@Transactional
@Slf4j
public class ListenerServiceLifecycleConditionService {

    /* *
     * CREATION STEP
     * */

    public boolean wrongCreationCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: wrongCreationCheck");

        return event.getOldStatusId() == 0
                && event.getNewStatusId() != ServiceStatus.DRAFT.getId()
                && event.getNewStatusId() != ServiceStatus.OPEN.getId();
    }

    public boolean wrongAfterDraftStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: wrongAfterDraftStepCheck");

        return event.getOldStatusId() == ServiceStatus.DRAFT.getId()
                && event.getNewStatusId() != ServiceStatus.DRAFT.getId()
                && event.getNewStatusId() != ServiceStatus.OPEN.getId()
                && event.getNewStatusId() != ServiceStatus.REMOVED.getId();
    }

    public boolean openStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: openStepCheck");

        return (event.getOldStatusId() == 0 || event.getOldStatusId() == ServiceStatus.DRAFT.getId())
                && event.getNewStatusId() == ServiceStatus.OPEN.getId();
    }

    //----------------------------------------

    /* *
     * ASSIGNMENT STEP
     * */

    public boolean wrongAfterOpenStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: openStepCheck");

        return event.getOldStatusId() == ServiceStatus.OPEN.getId()
                && event.getNewStatusId() != ServiceStatus.OPEN.getId()
                && event.getNewStatusId() != ServiceStatus.ASSIGNED.getId();
    }

    public boolean assignStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: assignStepCheck");

        return event.getOldStatusId() == ServiceStatus.OPEN.getId()
                && event.getNewStatusId() == ServiceStatus.ASSIGNED.getId();
    }

    //----------------------------------------

    /* *
     * PUBLISHING/CLARIFICATION REQUESTING STEP
     * */

    public boolean wrongAfterAssignStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: wrongAfterAssignStepCheck");

        return event.getOldStatusId() == ServiceStatus.ASSIGNED.getId()
                && event.getNewStatusId() != ServiceStatus.PUBLISHED.getId()
                && event.getNewStatusId() != ServiceStatus.UNDER_CLARIFICATION.getId();
    }

    public boolean publishOrClarificationStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: publishOrClarificationStepCheck");

        return event.getOldStatusId() == ServiceStatus.ASSIGNED.getId()
                && (event.getNewStatusId() == ServiceStatus.PUBLISHED.getId()
                || event.getNewStatusId() == ServiceStatus.UNDER_CLARIFICATION.getId());
    }

    //----------------------------------------

    /* *
     * AFTER PUBLISHING STEPS
     * */

    public boolean wrongAfterPublishStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: wrongAfterPublishStepCheck");

        return event.getOldStatusId() == ServiceStatus.PUBLISHED.getId()
                && event.getNewStatusId() != ServiceStatus.UNDER_CLARIFICATION.getId()
                && event.getNewStatusId() != ServiceStatus.ARCHIVED.getId();
    }

    public boolean clarificationStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: clarificationStepCheck");

        return event.getOldStatusId() == ServiceStatus.PUBLISHED.getId()
                && event.getNewStatusId() == ServiceStatus.UNDER_CLARIFICATION.getId();
    }

    //----------------------------------------

    /* *
     * CLARIFICATION RESULT STEP
     * */

    public boolean wrongAfterClarificationStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: wrongAfterClarificationStepCheck");

        return event.getOldStatusId() == ServiceStatus.UNDER_CLARIFICATION.getId()
                && event.getNewStatusId() != ServiceStatus.OPEN.getId()
                && event.getNewStatusId() != ServiceStatus.REMOVED.getId();
    }

    public boolean reopenStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: reopenStepCheck");

        return event.getOldStatusId() == ServiceStatus.UNDER_CLARIFICATION.getId()
                && event.getNewStatusId() == ServiceStatus.OPEN.getId();
    }

    //----------------------------------------

    /* *
     * ADDITIONAL STEPS
     * */

    public boolean unarchivedStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: unarchivedStepCheck");

        return event.getOldStatusId() == ServiceStatus.ARCHIVED.getId()
                && event.getNewStatusId() == ServiceStatus.OPEN.getId();
    }

    public boolean wrongAfterArchivedStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: wrongAfterArchivedStepCheck");

        return event.getOldStatusId() == ServiceStatus.ARCHIVED.getId()
                && event.getNewStatusId() != ServiceStatus.OPEN.getId()
                && event.getNewStatusId() != ServiceStatus.REMOVED.getId();
    }

    public boolean wrongAfterRemovedStepCheck(CustomLifecycleEvent<Service> event) {
        log.debug("condition: wrongAfterRemovedStepCheck");

        return event.getOldStatusId() == ServiceStatus.REMOVED.getId();
    }
}
