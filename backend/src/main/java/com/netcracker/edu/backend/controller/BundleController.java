package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.Service;
import com.netcracker.edu.backend.service.BundleService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import static com.netcracker.edu.backend.utils.Constants.RoleName;

@RestController
@RequestMapping("/api/account/bundles")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class BundleController {

    @Autowired
    private BundleService bundleService;

    @ApiOperation(value = "loadAllBundles", notes = "Loads all bundles list")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping
    public ResponseEntity<?> loadAllBundles() {
        log.debug("Requesting all bundles");

        return ResponseEntity.ok(bundleService.getAllBundlesDetailed());
    }

    @ApiOperation(value = "getBundle", notes = "Takes bundle id and loads it")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Bundle with exact id not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> getBundle(@PathVariable long id) {
        log.debug("Requesting bundle");

        return ResponseEntity.ok(bundleService.getBundleByIdDetailed(id));
    }

    @ApiOperation(value = "createBundle", notes = "Takes bundle object and inserts it to DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_ADMIN)
    @PostMapping
    public ResponseEntity<?> createBundle(@RequestBody Service bundle) {
        log.debug("Requesting bundle creating");

        return ResponseEntity.ok(bundleService.createBundle(bundle));
    }

    @ApiOperation(value = "updateBundle", notes = "Takes bundle object and updates exact object in DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_ADMIN)
    @PutMapping
    public ResponseEntity<?> updateBundle(@RequestBody Service bundle) {
        log.debug("Requesting bundle updating");

        bundleService.updateBundle(bundle);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "deleteBundle", notes = "Takes bundle id and deletes exact object from DB")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Secured(RoleName.ROLE_ADMIN)
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteBundle(@PathVariable long id) {
        log.debug("Requesting bundle deleting");

        bundleService.deleteBundleById(id);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "loadAllBundlesWithImg", notes = "Loads all bundles with image")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/withImg")
    public ResponseEntity<?> loadAllBundlesWithImg() {
        log.debug("Requesting all bundles with image");

        return ResponseEntity.ok(bundleService.getAllPublishedBundlesWithImg());
    }
}
