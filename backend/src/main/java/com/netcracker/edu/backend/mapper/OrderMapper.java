package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Order;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.OrderColumns;

public class OrderMapper implements RowMapper<Order> {

    @Override
    public Order mapRow(ResultSet resultSet, int i) throws SQLException {
        Order order = new Order();

        order.setId(resultSet.getLong(OrderColumns.ORDER_ID));
        order.setOrderDate(resultSet.getTimestamp(OrderColumns.ORDER_DATE));
        order.setTypeId(resultSet.getLong(OrderColumns.TYPE_ID));
        order.setUserId(resultSet.getLong(OrderColumns.USER_ID));
        order.setPrice(resultSet.getBigDecimal(OrderColumns.PRICE));

        return order;
    }
}
