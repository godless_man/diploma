package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.ChatMessageDao;
import com.netcracker.edu.backend.mapper.ChatMessageMapper;
import com.netcracker.edu.backend.model.ChatMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class ChatMessageDaoImpl extends GenericDaoImpl<ChatMessage> implements ChatMessageDao {

    @Value("${db.query.chatMessage.insert}")
    private String sqlChatMessageInsert;

    @Value("${db.query.chatMessage.update}")
    private String sqlChatMessageUpdate;

    @Value("${db.query.chatMessage.getAllByChatId}")
    private String sqlGetChatMessagesByChatId;

    public ChatMessageDaoImpl() {
        super(new ChatMessageMapper(), TableName.CHAT_MESSAGE);
    }

    @Override
    public List<ChatMessage> getAllChatMessagesByChatId(long id) {
        log.debug("Getting chatMessages by chat id: {}", id);

        List<ChatMessage> chatMessages = jdbcTemplate.query(sqlGetChatMessagesByChatId, new Object[]{id}, rowMapper);

        log.debug("Found chatMessages with ids: {}", chatMessages.stream().map(ChatMessage::getId).collect(Collectors.toList()));

        return chatMessages;
    }

    @Override
    protected String getInsertSql() {
        return sqlChatMessageInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, ChatMessage entity) throws SQLException {
        int argNum = 1;

        statement.setLong(argNum++, entity.getChatId());
        statement.setLong(argNum++, entity.getUserId());
        statement.setString(argNum++, entity.getMessage());
        statement.setTimestamp(argNum++, entity.getMessageTime());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlChatMessageUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(ChatMessage entity) {
        return new Object[]{entity.getChatId(), entity.getUserId(), entity.getMessage(), entity.getMessageTime()};
    }
}
