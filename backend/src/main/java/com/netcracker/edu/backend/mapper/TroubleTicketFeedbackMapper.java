package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.TroubleTicketFeedback;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.TroubleTicketFeedbackColumns;

public class TroubleTicketFeedbackMapper implements RowMapper<TroubleTicketFeedback> {

    @Override
    public TroubleTicketFeedback mapRow(ResultSet resultSet, int i) throws SQLException {
        TroubleTicketFeedback troubleTicketMessage = new TroubleTicketFeedback();

        troubleTicketMessage.setId(resultSet.getLong(TroubleTicketFeedbackColumns.TROUBLE_TICKET_FEEDBACK_ID));
        troubleTicketMessage.setRating(resultSet.getInt(TroubleTicketFeedbackColumns.RATING));
        troubleTicketMessage.setText(resultSet.getString(TroubleTicketFeedbackColumns.TEXT));

        return troubleTicketMessage;
    }
}
