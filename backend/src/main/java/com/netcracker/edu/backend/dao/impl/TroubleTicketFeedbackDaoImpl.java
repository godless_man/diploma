package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.TroubleTicketFeedbackDao;
import com.netcracker.edu.backend.mapper.TroubleTicketFeedbackMapper;
import com.netcracker.edu.backend.model.TroubleTicketFeedback;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
public class TroubleTicketFeedbackDaoImpl extends GenericDaoImpl<TroubleTicketFeedback> implements TroubleTicketFeedbackDao {

    @Value("${db.query.troubleTicketFeedback.insert}")
    private String sqlTroubleTicketFeedbackInsert;

    public TroubleTicketFeedbackDaoImpl() {
        super(new TroubleTicketFeedbackMapper(), TableName.TROUBLE_TICKET_FEEDBACK);
    }

    @Override
    protected String getInsertSql() {
        return sqlTroubleTicketFeedbackInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, TroubleTicketFeedback entity) throws SQLException {
        int argNum = 1;

        statement.setLong(argNum++, entity.getId());

        Integer rating = entity.getRating();
        if (rating == null) {
            statement.setNull(argNum++, Types.NULL);
        } else {
            statement.setInt(argNum++, rating);
        }

        statement.setString(argNum++, entity.getText());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return null;
    }

    @Override
    protected Object[] getArgsForUpdate(TroubleTicketFeedback entity) {
        return new Object[0];
    }
}
