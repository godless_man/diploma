package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.NotificationDao;
import com.netcracker.edu.backend.model.Notification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@Transactional
public class NotificationService {

    private static final String NOTIFICATION_APPROVERS = "/notification/approvers";
    private static final String NOTIFICATION_APPROVER = "/notification/approver/";
    private static final String NOTIFICATION_PROVIDER = "/notification/provider/";
    private static final String NOTIFICATION_USER = "/notification/user/";

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private NotificationDao notificationDao;

    public Notification createNotification(Notification notification) {
        log.debug("Creating notification");

        return notificationDao.insert(notification);
    }

    public void updateNotification(Notification notification) {
        log.debug("Updating notification");

        notificationDao.update(notification);
    }

    public Notification getDetailedNotificationOfServiceById(long id) {
        log.debug("Getting notification");

        return notificationDao.getDetailedOfServiceById(id).orElse(new Notification());
    }

    public Notification getDetailedNotificationOfTroubleTicketById(long id) {
        log.debug("Getting notification");

        return notificationDao.getDetailedOfTroubleTicketById(id).orElse(new Notification());
    }

    public Notification getDetailedNotificationOfServiceByServiceId(long serviceId) {
        log.debug("Getting notification");

        return notificationDao.getDetailedOfServiceByServiceId(serviceId).orElse(new Notification());
    }

    public Notification getDetailedNotificationOfTroubleTicketByTroubleTicketId(long troubleTicketId) {
        log.debug("Getting notification");

        return notificationDao.getDetailedOfTroubleTicketByTroubleTicketId(troubleTicketId).orElse(new Notification());
    }

    public List<Notification> getAllNotificationsDetailedOfServiceByUserId(long userId) {
        log.debug("Getting notification");

        return notificationDao.getAllDetailedOfServiceByUserId(userId);
    }

    public List<Notification> getAllNotificationsDetailedOfTroubleTicketByUserId(long userId) {
        log.debug("Getting notification");

        return notificationDao.getAllDetailedOfTroubleTicketByUserId(userId);
    }

    public List<Notification> getAllNotificationsDetailedOfServiceByUserIdNotRead(long userId) {
        log.debug("Getting notification");

        return notificationDao.getAllDetailedOfServiceByUserIdNotRead(userId);
    }

    public List<Notification> getAllNotificationsDetailedOfTroubleTicketByUserIdNotRead(long userId) {
        log.debug("Getting notification");

        return notificationDao.getAllDetailedOfTroubleTicketByUserIdNotRead(userId);
    }

    public List<Notification> getAllNotificationsDetailedOfServiceForProviderNotRead(long userId) {
        log.debug("Getting notification");

        return notificationDao.getAllDetailedOfServiceForProviderNotRead(userId);
    }

    public List<Notification> getAllNotificationsDetailedOfServiceForApproverNotRead(long userId) {
        log.debug("Getting notification");

        return notificationDao.getAllDetailedOfServiceForApproverNotRead(userId);
    }

    public List<Notification> getAllNotificationsDetailedOfTroubleTicketForUserNotRead(long userId) {
        log.debug("Getting notification");

        return notificationDao.getAllDetailedOfTroubleTicketForUserNotRead(userId);
    }

    public List<Notification> getAllNotificationsDetailedOfTroubleTicketForApproverNotRead(long userId) {
        log.debug("Getting notification");

        return notificationDao.getAllDetailedOfTroubleTicketForApproverNotRead(userId);
    }

    public void setReadTrueForAllNotifications() {
        log.debug("Updating notifications");

        notificationDao.setReadTrueForAll();
    }

    public Notification createAndGetNotificationDetailed(Notification notification) {
        log.debug("Creating/updating notification and getting detailed");

        if (notification.getServiceId() != null) {

            int updateResult = notificationDao.updateIfExistsByService(notification);
            if (updateResult > 0) {
                return getDetailedNotificationOfServiceByServiceId(notification.getServiceId());
            }

            return getDetailedNotificationOfServiceById(createNotification(notification).getId());
        } else {

            int updateResult = notificationDao.updateIfExistsByTroubleTicket(notification);
            if (updateResult > 0) {
                return getDetailedNotificationOfTroubleTicketByTroubleTicketId(notification.getTroubleTicketId());
            }

            return getDetailedNotificationOfTroubleTicketById(createNotification(notification).getId());
        }
    }

    public void sendNotificationToApprovers(Notification notification) {
        log.debug("Sending notification to approvers");

        notification = createAndGetNotificationDetailed(notification);

        messagingTemplate.convertAndSend(NOTIFICATION_APPROVERS, notification);
    }

    public void sendNotificationToApprover(Notification notification, long id) {
        log.debug("Sending notification to approver with id: {}", id);

        notification = createAndGetNotificationDetailed(notification);

        messagingTemplate.convertAndSend(NOTIFICATION_APPROVER + id, notification);
    }

    public void sendNotificationToProvider(Notification notification, long id) {
        log.debug("Sending notification to provider with id: {}", id);

        notification = createAndGetNotificationDetailed(notification);

        messagingTemplate.convertAndSend(NOTIFICATION_PROVIDER + id, notification);
    }

    public void sendNotificationToUser(Notification notification, long id) {
        log.debug("Sending notification to user with id: {}", id);

        notification = createAndGetNotificationDetailed(notification);

        messagingTemplate.convertAndSend(NOTIFICATION_USER + id, notification);
    }
}
