package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.CityDao;
import com.netcracker.edu.backend.mapper.CityMapper;
import com.netcracker.edu.backend.model.City;
import com.netcracker.edu.backend.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class CityDaoImpl extends GenericDaoImpl<City> implements CityDao {

    @Value("${db.query.city.insert}")
    private String sqlCityInsert;

    @Value("${db.query.city.update}")
    private String sqlCityUpdate;

    @Value("${db.query.city.getByName}")
    private String sqlGetCityByName;

    @Value("${db.query.city.getCitiesByCountryId}")
    private String sqlGetCitiesByCountryId;

    public CityDaoImpl() {
        super(new CityMapper(), Constants.TableName.CITY);
    }

    @Override
    protected String getInsertSql() {
        return sqlCityInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, City entity) throws SQLException {

        int argNum = 1;
        statement.setString(argNum++, entity.getName());
        statement.setLong(argNum++, entity.getCountryId());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlCityUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(City entity) {
        return new Object[]{entity.getName(), entity.getCountryId()};
    }

    @Override
    public Optional<City> getCityByName(String name, long countryId) {
        log.debug("Getting city by name: {}", name);

        List<City> city = jdbcTemplate.query(sqlGetCityByName, new Object[]{name, countryId}, rowMapper);

        log.debug("Found cities (by name) with ids {}", city.stream().map(City::getName).collect(Collectors.toList()));

        return getSingleElement(city);
    }

    @Override
    public List<City> getCitiesByCountryId(long id) {
        log.debug("Getting city by id: {}", id);

        List<City> cities = jdbcTemplate.query(sqlGetCitiesByCountryId, new Object[]{id}, rowMapper);

        log.debug("Found cities (for country with id = {}) with ids {}", id, cities.stream().map(City::getName).collect(Collectors.toList()));

        return cities;
    }

}

