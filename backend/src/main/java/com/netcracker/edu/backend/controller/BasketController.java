package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.Service;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.security.UserPrincipal;
import com.netcracker.edu.backend.service.OrderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.netcracker.edu.backend.utils.Constants.RoleName;

@RestController
@RequestMapping("/api/basket")
@CrossOrigin("http://localhost:4200")
@Slf4j
public class BasketController {

    private OrderService orderService;

    @Autowired
    public BasketController(OrderService orderService) {
        this.orderService = orderService;
    }

    @ApiOperation(value = "getDraftBasket", notes = "Loads draft basket")
    @ApiResponses(
            @ApiResponse(code = 200, message = "OK")
    )
    @Secured(RoleName.ROLE_USER)
    @GetMapping
    public ResponseEntity<?> getDraftBasket(@CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting user's basket");

        List<Service> basket = orderService.getBasketByUserId(userPrincipal.getId());
        return ResponseEntity.ok(basket);
    }

    @ApiOperation(value = "addToBasket", notes = "Takes service id and adding it to basket")
    @ApiResponses(
            @ApiResponse(code = 200, message = "OK")
    )
    @Secured(RoleName.ROLE_USER)
    @PutMapping("/{id}")
    public ResponseEntity<?> addToBasket(@PathVariable long id, @CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting adding service to basket");

        orderService.addToBasket(id, userPrincipal.getId());
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "deleteFromBasket", notes = "Takes service id and deleting it from basket")
    @ApiResponses(
            @ApiResponse(code = 200, message = "OK")
    )
    @Secured(RoleName.ROLE_USER)
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteFromBasket(@PathVariable long id, @CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting deleting service from basket");

        orderService.deleteFromBasket(id, userPrincipal.getId());
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "completeOrder", notes = "Completing order")
    @ApiResponses(
            @ApiResponse(code = 200, message = "OK")
    )
    @Secured(RoleName.ROLE_USER)
    @GetMapping("/pay")
    public ResponseEntity<?> completeOrder(@CurrentUser UserPrincipal userPrincipal) {
        log.debug("Requesting order completing");

        orderService.completeOrder(userPrincipal.getId());
        return ResponseEntity.ok().build();
    }
}
