package com.netcracker.edu.backend.dto.request;

import com.netcracker.edu.backend.model.Country;
import lombok.*;

import javax.validation.constraints.NotNull;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchTripMultipleRequest {

    private Country locationCountry;

    private Country[] destinationCountry;

    @NotNull
    private int numberOfPeople;

    @NotNull
    private int priceFrom;

    @NotNull
    private int priceTo;
}
