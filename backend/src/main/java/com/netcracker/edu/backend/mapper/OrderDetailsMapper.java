package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.OrderDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.OrderColumns;
import static com.netcracker.edu.backend.utils.Constants.ServiceColumns;

public class OrderDetailsMapper implements RowMapper<OrderDetails> {

    @Override
    public OrderDetails mapRow(ResultSet resultSet, int i) throws SQLException {
        OrderDetails order = new OrderDetails();

        order.setServiceId(resultSet.getLong(ServiceColumns.DETAILED_SERVICE_ID));
        order.setPrice(resultSet.getBigDecimal(OrderColumns.PRICE));
        order.setName(resultSet.getString(ServiceColumns.NAME));

        return order;
    }
}