package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.ServiceMessage;

import java.util.Optional;

public interface ServiceMessageDao extends GenericDao<ServiceMessage> {

    Optional<ServiceMessage> getServiceMessageByServiceId(long id);
}
