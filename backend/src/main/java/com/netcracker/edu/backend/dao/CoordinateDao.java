package com.netcracker.edu.backend.dao;


import com.netcracker.edu.backend.model.Coordinate;

import java.util.Optional;

public interface CoordinateDao extends GenericDao<Coordinate> {

    Optional<Coordinate> getLocationByServiceId(long id);

    Optional<Coordinate> getDestinationByServiceId(long id);

    Optional<Coordinate> getCoordinateByCityId(long id);
}
