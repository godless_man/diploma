package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Subscription;
import com.netcracker.edu.backend.utils.Constants;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SubscriptionMapper implements RowMapper<Subscription> {

    @Override
    public Subscription mapRow(ResultSet resultSet, int i) throws SQLException {
        Subscription subscription = new Subscription();

        subscription.setUserId(resultSet.getLong(Constants.SubscriptionColumns.USER_ID));
        subscription.setProviderId(resultSet.getLong(Constants.SubscriptionColumns.PROVIDER_ID));
        return subscription;
    }
}
