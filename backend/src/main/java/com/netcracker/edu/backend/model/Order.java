package com.netcracker.edu.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order extends Identified {

    private long id;
    private Timestamp orderDate;
    private long userId;
    private long typeId;
    private String name;
    private List<OrderDetails> orderDetails;
    private BigDecimal price;
}
