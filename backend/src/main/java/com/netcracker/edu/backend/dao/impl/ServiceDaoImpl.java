package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.ServiceDao;
import com.netcracker.edu.backend.dto.request.SearchTripRequest;
import com.netcracker.edu.backend.mapper.*;
import com.netcracker.edu.backend.model.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.netcracker.edu.backend.dto.response.AdminDashboardResponse.CountryTrips;
import static com.netcracker.edu.backend.utils.Constants.ServiceType;
import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class ServiceDaoImpl extends GenericDaoImpl<Service> implements ServiceDao {

    private static final RowMapper<Service> serviceStatusLocationMapper = new ServiceStatusLocationMapper();
    private static final RowMapper<Service> serviceStatusLocationDiscountMapper = new ServiceStatusLocationDiscountMapper();
    private static final RowMapper<Service> serviceDetailedMapper = new ServiceDetailedMapper();
    private static final RowMapper<Service> servicePurchasedMapper = new ServicePurchasedMapper();
    private static final RowMapper<Service> serviceRatingMapper = new ServiceRatingMapper();
    private static final RowMapper<Long> longMapper = new LongMapper();

    @Value("${db.query.service.insert}")
    private String sqlServiceInsert;

    @Value("${db.query.service.update}")
    private String sqlServiceUpdate;

    @Value("${db.query.service.updateSetRemovedForAllUnderClarification}")
    private String sqlServiceUpdateSetRemovedForAllUnderClarification;

    @Value("${db.query.service.getTypedById}")
    private String sqlGetTypedById;

    @Value("${db.query.service.getAllTyped}")
    private String sqlGetTyped;

    @Value("${db.query.service.getTypedOfStatus}")
    private String sqlGetTypedOfStatus;

    @Value("${db.query.service.getTypedByProviderId}")
    private String sqlGetTypedByProviderId;

    @Value("${db.query.service.getTypedOfStatusByProviderId}")
    private String sqlGetTypedOfStatusByProviderId;

    @Value("${db.query.service.getTypedByApproverId}")
    private String sqlGetTypedByApproverId;

    @Value("${db.query.service.getTypedOfStatusByApproverId}")
    private String sqlGetTypedOfStatusByApproverId;

    @Value("${db.query.service.countAllTyped}")
    private String sqlCountAllTyped;

    @Value("${db.query.service.insertSuggestionLink}")
    private String sqlInsertSuggestionLink;

    @Value("${db.query.service.getTypedByProviderIdWithLocationsAndStatus}")
    private String sqlGetTypedByProviderIdWithLocationsAndStatus;

    @Value("${db.query.service.getTypedByApproverIdOrNotAssignedWithLocationsAndStatus}")
    private String sqlGetTypedByApproverIdOrNotAssignedWithLocationsAndStatus;

    @Value("${db.query.service.getTripsAmountByCountry}")
    private String sqlGetTripsAmountByCountry;

    @Value("${db.query.service.getSaledWithLocationsAndStatusByDate}")
    private String sqlGetSalesBySomePeriod;

    @Value("${db.query.service.getTypeStatusAndImg}")
    private String sqlGetTypeStatusAndImg;

    @Value("${db.query.service.insertBundleLink}")
    private String sqlInsertBundleLink;

    @Value("${db.query.service.getTypedByIdDetailed}")
    private String sqlGetTypedByIdDetailed;

    @Value("${db.query.service.getTripOrSuggestionByIdDetailed}")
    private String sqlGetTripOrSuggestionByIdDetailed;

    @Value("${db.query.service.getSuggestionsByTripId}")
    private String sqlGetSuggestionsByTripId;

    @Value("${db.query.service.getServicesBySuggestionId}")
    private String sqlGetServicesBySuggestionId;

    @Value("${db.query.service.getTripByCountriesPeopleAndPrice}")
    private String sqlGetTripByCountriesPeopleAndPrice;

    @Value("${db.query.service.getPublishedTypedIdWithLocationsAndStatus}")
    private String sqlGetPublishedTypedIdWithLocationsAndStatus;

    @Value("${db.query.service.getPublishedByProviderId}")
    private String sqlGetPublishedByProviderId;

    @Value("${db.query.service.getAllPurchasedByUserWithUserId}")
    private String sqlGetAllServicesPurchasedByUserWithUserId;

    @Value("${db.query.service.getTripsByBundleId}")
    private String sqlGetTripsByBundleId;

    @Value("${db.service.searchByDiscount}")
    private String sqlSearchByDiscount;

    @Value("${db.service.searchByRating}")
    private String sqlSearchByRating;

    @Value("${db.service.searchByYourRaitingProviderId}")
    private String sqlSearchByYourRaitingProviderId;

    @Value("${db.service.searchByNumOfOrderProviderId}")
    private String sqlSearchByNumOfOrderProviderId;

    @Value("${db.query.service.getAllDetailedTypedByServiceName}")
    private String sqlGetAllDetailedTypedByServiceName;

    @Value("${db.query.service.deleteBundleLinks}")
    private String sqlDeleteBundleLinks;

    @Value("${db.query.service.countPublishedServices}")
    private String sqlCountPublishedServices;

    @Value("${db.query.service.countUnPublishedServices}")
    private String sqlCountUnPublishedServices;

    public ServiceDaoImpl() {
        super(new ServiceMapper(), TableName.SERVICE);
    }

    @Override
    protected String getInsertSql() {
        return sqlServiceInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Service entity) throws SQLException {
        int argNum = 1;
        statement.setLong(argNum++, entity.getTypeId());
        statement.setString(argNum++, entity.getName());

        Long approverId = entity.getApproverId();
        if (approverId == null) {
            statement.setNull(argNum++, Types.NULL);
        } else {
            statement.setLong(argNum++, approverId);
        }

        statement.setLong(argNum++, entity.getProviderId());
        statement.setLong(argNum++, entity.getStatusId());
        statement.setLong(argNum++, entity.getLocationId());

        Long destinationId = entity.getDestinationId();
        if (destinationId == null) {
            statement.setNull(argNum++, Types.NULL);
        } else {
            statement.setLong(argNum++, destinationId);
        }

        statement.setLong(argNum++, entity.getNumberOfPeople());
        statement.setBigDecimal(argNum++, entity.getPrice());
        statement.setString(argNum++, entity.getDescription());
        statement.setString(argNum++, entity.getImgSrc());
        statement.setBoolean(argNum++, entity.isOneWay());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlServiceUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(Service entity) {
        return new Object[]{entity.getTypeId(), entity.getName(), entity.getApproverId(), entity.getProviderId(),
                entity.getStatusId(), entity.getLocationId(), entity.getDestinationId(), entity.getNumberOfPeople(),
                entity.getPrice(), entity.getDescription(), entity.getImgSrc(), entity.isOneWay()};
    }

    @Override
    public List<Service> getAllTrips() {
        log.debug("Getting trips");

        List<Service> services = jdbcTemplate.query(sqlGetTyped, new Object[]{ServiceType.TRIP}, rowMapper);

        log.debug("Found users with ids {}", services.stream().map(Service::getId).collect(Collectors.toList()));

        return services;
    }

    @Override
    public Optional<Service> getTripById(long id) {
        log.debug("Getting trip by id: {}", id);

        List<Service> trip = jdbcTemplate.query(sqlGetTypedById, new Object[]{ServiceType.TRIP, id}, rowMapper);

        log.debug("Found trips with ids {}", trip.stream().map(Service::getId).collect(Collectors.toList()));

        return getSingleElement(trip);
    }

    @Override
    public Optional<Service> getTripByIdDetailed(long id) {
        log.debug("Getting detailed trip by id: {}", id);

        List<Service> trip = jdbcTemplate.query(sqlGetTypedByIdDetailed, new Object[]{ServiceType.TRIP, id}, serviceDetailedMapper);

        log.debug("Found trips with ids {}", trip.stream().map(Service::getId).collect(Collectors.toList()));

        return getSingleElement(trip);
    }

    @Override
    public Optional<Service> getTripsByIdDetailed(long id) {
        log.debug("Getting detailed trip by id: {}", id);

        List<Service> trip = jdbcTemplate.query(sqlGetTripOrSuggestionByIdDetailed, new Object[]{id}, serviceDetailedMapper);

        log.debug("Found trips with ids {}", trip.stream().map(Service::getId).collect(Collectors.toList()));

        return getSingleElement(trip);
    }

    @Override
    public List<Service> getSuggestionsByTripId(long id) {
        log.debug("Getting suggestions by tripId: {}", id);

        List<Service> suggestions = jdbcTemplate.query(sqlGetSuggestionsByTripId, new Object[]{id}, serviceStatusLocationDiscountMapper);

        log.debug("Found sugestions with ids {}", suggestions.stream().map(Service::getId).collect(Collectors.toList()));

        return suggestions;
    }

    @Override
    public List<Service> getServicesBySuggestionId(long id) {
        log.debug("Getting services by suggestionId: {}", id);

        List<Service> services = jdbcTemplate.query(sqlGetServicesBySuggestionId, new Object[]{id}, serviceStatusLocationMapper);

        log.debug("Found services with ids {}", services.stream().map(Service::getId).collect(Collectors.toList()));

        return services;
    }

    @Override
    public List<Service> getAllServices() {
        log.debug("Getting services");

        List<Service> services = jdbcTemplate.query(sqlGetTyped, new Object[]{ServiceType.SERVICE}, rowMapper);

        log.debug("Found services with ids {}", services.stream().map(Service::getId).collect(Collectors.toList()));

        return services;
    }

    @Override
    public Optional<Service> getServiceById(long id) {
        log.debug("Getting service by id: {}", id);

        List<Service> service = jdbcTemplate.query(sqlGetTypedById, new Object[]{ServiceType.SERVICE, id}, rowMapper);

        log.debug("Found services with ids {}", service.stream().map(Service::getId).collect(Collectors.toList()));

        return getSingleElement(service);
    }

    @Override
    public List<Service> getAllBundles() {
        log.debug("Getting bundles");

        List<Service> bundles = jdbcTemplate.query(sqlGetTyped, new Object[]{ServiceType.BUNDLE}, rowMapper);

        log.debug("Found bundles with ids {}", bundles.stream().map(Service::getId).collect(Collectors.toList()));

        return bundles;
    }

    @Override
    public List<Service> getAllBundlesDetailed() {
        log.debug("Getting bundles");

        List<Service> bundles = jdbcTemplate.query(sqlGetAllDetailedTypedByServiceName, new Object[]{ServiceType.BUNDLE}, serviceStatusLocationMapper);

        log.debug("Found bundles with ids {}", bundles.stream().map(Service::getId).collect(Collectors.toList()));

        return bundles;
    }

    @Override
    public Optional<Service> getBundleById(long id) {
        log.debug("Getting bundle by id: {}", id);

        List<Service> bundle = jdbcTemplate.query(sqlGetTypedById, new Object[]{ServiceType.BUNDLE, id}, rowMapper);

        log.debug("Found bundles with ids {}", bundle.stream().map(Service::getId).collect(Collectors.toList()));

        return getSingleElement(bundle);

    }

    @Override
    public Optional<Service> getBundleByIdDetailed(long id) {
        log.debug("Getting detailed bundle by id: {}", id);

        List<Service> bundle = jdbcTemplate.query(sqlGetTypedByIdDetailed, new Object[]{ServiceType.BUNDLE, id}, serviceDetailedMapper);

        log.debug("Found bundles with ids {}", bundle.stream().map(Service::getId).collect(Collectors.toList()));

        return getSingleElement(bundle);
    }

    @Override
    public List<Service> getTripsByBundleId(long id) {
        log.debug("Getting trips by bundleId: {}", id);

        List<Service> trips = jdbcTemplate.query(sqlGetTripsByBundleId, new Object[]{id}, serviceStatusLocationMapper);

        log.debug("Found trips with ids {}", trips.stream().map(Service::getId).collect(Collectors.toList()));

        return trips;
    }

    @Override
    public List<Service> getAllTripsOfStatus(String status) {
        log.debug("Getting trips by status: {}", status);

        List<Service> trips = jdbcTemplate.query(sqlGetTypedOfStatus, new Object[]{ServiceType.TRIP, status}, rowMapper);

        log.debug("Found trips with ids {}", trips.stream().map(Service::getId).collect(Collectors.toList()));

        return trips;
    }

    @Override
    public List<Service> getTripsByProviderId(String serviceType, long id) {
        log.debug("Getting trips by provideId: {}", id);

        List<Service> trips = jdbcTemplate.query(sqlGetPublishedByProviderId, new Object[]{serviceType, id}, serviceStatusLocationMapper);

        log.debug("Found trips with ids {}", trips.stream().map(Service::getId).collect(Collectors.toList()));

        return trips;
    }

    @Override
    public List<Service> getTripsOfStatusByProviderId(String status, long id) {
        log.debug("Getting trips of status: {}, by providerId - {}", status, id);

        List<Service> trips = jdbcTemplate.query(sqlGetTypedOfStatusByProviderId, new Object[]{ServiceType.TRIP, status, id}, rowMapper);

        log.debug("Found trips with ids {}", trips.stream().map(Service::getId).collect(Collectors.toList()));

        return trips;
    }

    @Override
    public List<Service> getTripsByApproverId(long id) {
        log.debug("Getting trips by approverId: {}", id);

        List<Service> trips = jdbcTemplate.query(sqlGetTypedByApproverId, new Object[]{ServiceType.TRIP, id}, rowMapper);

        log.debug("Found trips with ids {}", trips.stream().map(Service::getId).collect(Collectors.toList()));

        return trips;
    }

    @Override
    public List<Service> getTripsOfStatusByApproverId(String status, long id) {
        log.debug("Getting trips of status: {}, by approverId - {}", status, id);

        List<Service> trips = jdbcTemplate.query(sqlGetTypedOfStatusByApproverId, new Object[]{ServiceType.TRIP, status, id}, rowMapper);

        log.debug("Found trips with ids {}", trips.stream().map(Service::getId).collect(Collectors.toList()));

        return trips;
    }

    @Override
    public List<CountryTrips> getTripsAmountByCountry() {
        log.debug("Getting trips by country");

        return jdbcTemplate.query(sqlGetTripsAmountByCountry, new CountryTripsMapper());
    }

    @Override
    public List<Service> getAllServicesOfStatus(String status) {
        log.debug("Getting services by status: {}", status);

        List<Service> services = jdbcTemplate.query(sqlGetTypedOfStatus, new Object[]{ServiceType.SERVICE, status}, rowMapper);

        log.debug("Found services with ids {}", services.stream().map(Service::getId).collect(Collectors.toList()));

        return services;
    }

    @Override
    public List<Service> getServicesByProviderId(long id) {
        log.debug("Getting services by providerId: {}", id);

        List<Service> services = jdbcTemplate.query(sqlGetTypedByProviderId, new Object[]{ServiceType.SERVICE, id}, rowMapper);

        log.debug("Found services with ids {}", services.stream().map(Service::getId).collect(Collectors.toList()));

        return services;
    }

    @Override
    public List<Service> getServicesOfStatusByProviderId(String status, long id) {
        log.debug("Getting services of status: {}, bu providerId - {}", status, id);

        List<Service> services = jdbcTemplate.query(sqlGetTypedOfStatusByProviderId, new Object[]{ServiceType.SERVICE, status, id}, rowMapper);

        log.debug("Found services with ids {}", services.stream().map(Service::getId).collect(Collectors.toList()));

        return services;
    }

    @Override
    public List<Service> getAllBundlesOfStatus(String status) {
        log.debug("Getting bundles by status: {}", status);

        List<Service> bundles = jdbcTemplate.query(sqlGetTypedOfStatus, new Object[]{ServiceType.BUNDLE, status}, rowMapper);

        log.debug("Found bundles with ids {}", bundles.stream().map(Service::getId).collect(Collectors.toList()));

        return bundles;
    }

    @Override
    public List<Service> getBundlesByProviderId(long id) {
        log.debug("Getting bundles by providerId: {}", id);

        List<Service> bundles = jdbcTemplate.query(sqlGetTypedByProviderId, new Object[]{ServiceType.BUNDLE, id}, rowMapper);

        log.debug("Found bundles with ids {}", bundles.stream().map(Service::getId).collect(Collectors.toList()));

        return bundles;
    }

    @Override
    public List<Service> getBundlesOfStatusByProviderId(String status, long id) {
        log.debug("Getting bundles of status: {}, by providerId {}", status, id);

        List<Service> bundles = jdbcTemplate.query(sqlGetTypedOfStatusByProviderId, new Object[]{ServiceType.BUNDLE, status, id}, rowMapper);

        log.debug("Found bundles with ids {}", bundles.stream().map(Service::getId).collect(Collectors.toList()));

        return bundles;
    }

    @Override
    public void deleteBundleLinks(long id) {
        log.debug("Deleting bundle links with bundle id: {}", id);

        jdbcTemplate.update(sqlDeleteBundleLinks, id);
    }

    @Override
    public Optional<Long> countAllTrips() {
        log.debug("Getting count of all trips");

        List<Long> count = jdbcTemplate.query(sqlCountAllTyped, new Object[]{ServiceType.TRIP}, longMapper);

        return getCountElement(count);
    }

    @Override
    public Optional<Long> countAllServices() {
        log.debug("Getting count of all services");

        List<Long> count = jdbcTemplate.query(sqlCountAllTyped, new Object[]{ServiceType.SERVICE}, longMapper);

        return getCountElement(count);
    }

    @Override
    public Optional<Long> countAllBundles() {
        log.debug("Getting count of all bundles");

        List<Long> count = jdbcTemplate.query(sqlCountAllTyped, new Object[]{ServiceType.BUNDLE}, longMapper);

        return getCountElement(count);
    }

    @Override
    public void insertSuggestionLink(long suggestionId, long serviceId) {
        log.debug("Inserting suggestion link between suggestion {} and service {}", suggestionId, serviceId);

        jdbcTemplate.update(sqlInsertSuggestionLink, suggestionId, serviceId);
    }

    @Override
    public List<Service> getTripsByProviderIdWithLocationAndStatus(long id) {
        log.debug("Getting trips with location and status by providerId: {}", id);

        List<Service> trips = jdbcTemplate.query(sqlGetTypedByProviderIdWithLocationsAndStatus, new Object[]{ServiceType.TRIP, id}, serviceStatusLocationMapper);

        log.debug("Found trips with ids {}", trips.stream().map(Service::getId).collect(Collectors.toList()));

        return trips;
    }

    @Override
    public List<Service> getTripsByApproverIdOrNotAssignedWithLocationAndStatus(long id) {
        log.debug("Getting trips with location and status by approverId: {}", id);

        List<Service> trips = jdbcTemplate.query(sqlGetTypedByApproverIdOrNotAssignedWithLocationsAndStatus, new Object[]{ServiceType.TRIP, id}, serviceStatusLocationMapper);

        log.debug("Found trips with ids {}", trips.stream().map(Service::getId).collect(Collectors.toList()));

        return trips;
    }

    @Override
    public void insertBundleLink(long bundleId, long tripId) {
        log.debug("Inserting bundle link between bundle {} and service {}", bundleId, tripId);

        jdbcTemplate.update(sqlInsertBundleLink, bundleId, tripId);
    }


    @Override
    public List<Service> getSalesBySomePeriod(Timestamp start, Timestamp end, String serviceType) {
        log.debug("Getting sales by period: {} - {}, with serviceType {}", start, end, serviceType);

        return jdbcTemplate.query(sqlGetSalesBySomePeriod, new Object[]{start, end, serviceType}, serviceStatusLocationMapper);
    }

    @Override
    public List<Service> getTypeStatusAndImg(String serviceTypeName, String serviceStatusName) {
        log.debug("Getting typeStatus and image: {} {}", serviceTypeName, serviceStatusName);

        return jdbcTemplate.query(sqlGetTypeStatusAndImg, new Object[]{serviceTypeName, serviceStatusName}, rowMapper);
    }

    @Override
    public List<Service> getTripByCountriesPeopleAndPrice(String locationListCountries, String destinationListCountries, int numberOfPeople, int priceMin, int priceMax) {
        log.debug("Getting trips by locationCountries {}, destinationCountries {}, number of people {} and price {} - {}",
                locationListCountries, destinationListCountries, numberOfPeople, priceMin, priceMax);

        return jdbcTemplate.query(sqlGetTripByCountriesPeopleAndPrice, new Object[]{locationListCountries, destinationListCountries, numberOfPeople, priceMin, priceMax}, serviceStatusLocationMapper);
    }

    @Override
    public List<Service> getPublishedWithLocationsAndStatus(String serviceType) {
        log.debug("Getting trips published and status: {}", serviceType);

        return jdbcTemplate.query(sqlGetPublishedTypedIdWithLocationsAndStatus, new Object[]{serviceType}, serviceStatusLocationMapper);
    }

    @Override
    public void setRemovedForAllUnderClarification() {
        log.debug("Setting REMOVED status for UNDER_CLARIFICATION");

        jdbcTemplate.update(sqlServiceUpdateSetRemovedForAllUnderClarification);
    }

    @Override
    public List<Service> getAllServicesPurchasedByUserWithUserId(long id) {
        log.debug("Getting services purchased by user with id: {}", id);

        List<Service> services = jdbcTemplate.query(sqlGetAllServicesPurchasedByUserWithUserId, new Object[]{id}, servicePurchasedMapper);

        log.debug("Found services with ids {}", services.stream().map(Service::getId).collect(Collectors.toList()));

        return services;
    }

    @Override
    public Optional<Long> getSearchByYourRaitingProviderId(long userId) {
        log.debug("Getting best provider from rating feedback from user with id: {}", userId);
        List<Long> providerId = jdbcTemplate.query(sqlSearchByYourRaitingProviderId, new Object[]{userId, userId}, longMapper);

        return getCountElement(providerId);
    }

    @Override
    public List<Service> getSearchByRating(SearchTripRequest searchTripRequest, int limit) {
        log.debug("Search Trip by rating , location {} and destination {}", searchTripRequest.getLocationCountry(), searchTripRequest.getDestinationCountry());
        return jdbcTemplate.query(sqlSearchByRating, new Object[]{searchTripRequest.getLocationCountry(), searchTripRequest.getDestinationCountry(), searchTripRequest.getPriceFrom(), searchTripRequest.getPriceTo(), searchTripRequest.getNumberOfPeople(), limit}, serviceRatingMapper);
    }

    @Override
    public List<Service> getSearchByDiscount(SearchTripRequest searchTripRequest, int limit) {
        log.debug("Search Trip by discount , location {} and destination {}", searchTripRequest.getLocationCountry(), searchTripRequest.getDestinationCountry());
        return jdbcTemplate.query(sqlSearchByDiscount, new Object[]{searchTripRequest.getLocationCountry(), searchTripRequest.getDestinationCountry(), searchTripRequest.getPriceFrom(), searchTripRequest.getPriceTo(), searchTripRequest.getNumberOfPeople(), limit}, serviceRatingMapper);
    }

    @Override
    public Optional<Long> getSearchByNumOfOrderProviderId(long userId) {
        log.debug("Getting best provider from rating feedback from user with id: {}", userId);
        List<Long> providerId = jdbcTemplate.query(sqlSearchByNumOfOrderProviderId, new Object[]{userId}, longMapper);

        return getCountElement(providerId);
    }

    @Override
    public List<Service> getSpecialSearch(String request, Object[] objects) {
        log.debug("Getting special search by request {}", request);

        return jdbcTemplate.query(request, objects, serviceStatusLocationMapper);
    }

    @Override
    public Optional<Long> countPublishedServices() {
        log.debug("count published services");

        List<Long> count = jdbcTemplate.query(sqlCountPublishedServices, new Object[]{}, longMapper);

        return getCountElement(count);
    }

    @Override
    public Optional<Long> countUnPublishedServices() {
        log.debug("count unpublished services");

        List<Long> count = jdbcTemplate.query(sqlCountUnPublishedServices, new Object[]{}, longMapper);

        return getCountElement(count);
    }
}
