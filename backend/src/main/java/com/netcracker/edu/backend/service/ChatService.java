package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ChatDao;
import com.netcracker.edu.backend.model.Chat;
import com.netcracker.edu.backend.model.Notification;
import com.netcracker.edu.backend.security.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class ChatService {

    @Autowired
    private ChatDao chatDao;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    public Chat createChat(Chat chat) {
        log.debug("Creating chat");

        return chatDao.insert(chat);
    }

    public void updateChat(Chat chat) {
        log.debug("Updating chat");

        if (chat.getApproverId() == 0) {
            chat.setApproverId(null);
        }

        chatDao.update(chat);
    }

    public Chat getChatByUserIdOrCreate(UserPrincipal userPrincipal) {
        log.debug("Getting chat");

        Chat chat = chatDao.getChatByUserId(userPrincipal.getId()).orElse(new Chat());

        if (chat.getId() == 0) {
            chat = createChat(new Chat(userPrincipal.getId()));
            messagingTemplate.convertAndSend("/notification/approvers", new Notification(userPrincipal.getId(), chat.getId(), "MESSAGE", userPrincipal.getUsername()));
        }

        chat.setUsername(userPrincipal.getUsername());

        return chat;
    }

    public List<Chat> getAllChatsForApprover(long id) {
        log.debug("Getting chats");

        return chatDao.getAllChatsForApprover(id);
    }

    public Chat changeChatAssignment(Chat chat, UserPrincipal userPrincipal) {

        Long newApproverId = null;
        if (chat.getApproverId() == 0 || chat.getApproverId() == null) {
            newApproverId = userPrincipal.getId();
        }

        log.debug("Chat assignment changing, setting approver id {}", newApproverId);

        chat.setApproverId(newApproverId);
        updateChat(chat);

        return chat;
    }
}
