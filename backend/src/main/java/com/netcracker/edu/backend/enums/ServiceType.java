package com.netcracker.edu.backend.enums;

import lombok.Getter;

@Getter
public enum ServiceType {

    TRIP(1, "Trip"),
    SERVICE(2, "Service"),
    BUNDLE(3, "Bundle"),
    SUGGESTION(4, "Suggestion");

    private final long id;
    private final String serviceTypeName;

    ServiceType(long id, String serviceTypeName) {
        this.id = id;
        this.serviceTypeName = serviceTypeName;
    }

    public static String getNameFromId(long id) {

        for (ServiceType serviceType : values()) {
            if (serviceType.getId() == id) {
                return serviceType.getServiceTypeName();
            }
        }

        return "UNKNOWN";
    }

    public static long getIdFromName(String name) {

        for (ServiceType serviceType : values()) {
            if (serviceType.getServiceTypeName().equals(name)) {
                return serviceType.getId();
            }
        }

        return 0;
    }
}
