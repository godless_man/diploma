package com.netcracker.edu.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TroubleTicket extends Identified {

    private long serviceId;
    private long userId;
    private Long approverId;
    private long statusId;
    private String headline;
    private Timestamp applyTime;

    private String status;

    private Integer rating;
    private String text;

    public TroubleTicket(long serviceId, long userId, long statusId, String headline, Timestamp applyTime) {
        this.serviceId = serviceId;
        this.userId = userId;
        this.statusId = statusId;
        this.headline = headline;
        this.applyTime = applyTime;
    }
}
