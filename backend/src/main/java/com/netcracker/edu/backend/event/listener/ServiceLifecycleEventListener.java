package com.netcracker.edu.backend.event.listener;

import com.netcracker.edu.backend.event.CustomServiceLifecycleEvent;
import com.netcracker.edu.backend.exception.BadRequestException;
import com.netcracker.edu.backend.model.Notification;
import com.netcracker.edu.backend.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
@Slf4j
public class ServiceLifecycleEventListener {

    @Autowired
    private NotificationService notificationService;

    private static final String WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE = "Wrong state changing due to lifecycle";

    /* *
     * CREATION STEP
     * */

    @EventListener(condition = "@listenerServiceLifecycleConditionService.wrongCreationCheck(#event)")
    public void onWrongCreationEvent(CustomServiceLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerServiceLifecycleConditionService.wrongAfterDraftStepCheck(#event)")
    public void onWrongAfterDraftEvent(CustomServiceLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerServiceLifecycleConditionService.openStepCheck(#event)")
    public void onOpenEvent(CustomServiceLifecycleEvent event) {
        log.debug("Listening onOpenEvent");
        notificationService.sendNotificationToApprovers(buildNotification(event));
    }

    //----------------------------------------

    /* *
     * ASSIGNMENT STEP
     * */

    @EventListener(condition = "@listenerServiceLifecycleConditionService.wrongAfterOpenStepCheck(#event)")
    public void onWrongAfterOpenEvent(CustomServiceLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerServiceLifecycleConditionService.assignStepCheck(#event)")
    public void onAssignedEvent(CustomServiceLifecycleEvent event) {
        log.debug("Listening onAssignedEvent");
        notificationService.sendNotificationToProvider(buildNotification(event), event.getRecipientId());
    }

    //----------------------------------------

    /* *
     * PUBLISHING/CLARIFICATION REQUESTING STEP
     * */

    @EventListener(condition = "@listenerServiceLifecycleConditionService.wrongAfterAssignStepCheck(#event)")
    public void onWrongAfterAssignedEvent(CustomServiceLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerServiceLifecycleConditionService.publishOrClarificationStepCheck(#event)")
    public void onPublishedOrClarificationEvent(CustomServiceLifecycleEvent event) {
        log.debug("Listening onPublishedOrClarificationEvent");
        notificationService.sendNotificationToProvider(buildNotification(event), event.getRecipientId());
    }

    //----------------------------------------

    /* *
     * AFTER PUBLISHING STEPS
     * */

    @EventListener(condition = "@listenerServiceLifecycleConditionService.wrongAfterPublishStepCheck(#event)")
    public void onWrongAfterPublishedEvent(CustomServiceLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerServiceLifecycleConditionService.clarificationStepCheck(#event)")
    public void onAfterPublishedEvent(CustomServiceLifecycleEvent event) {
        log.debug("Listening onAfterPublishedEvent");
        notificationService.sendNotificationToProvider(buildNotification(event), event.getRecipientId());
    }

    //----------------------------------------

    /* *
     * CLARIFICATION RESULT STEP
     * */

    @EventListener(condition = "@listenerServiceLifecycleConditionService.wrongAfterClarificationStepCheck(#event)")
    public void onWrongAfterClarificationEvent(CustomServiceLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerServiceLifecycleConditionService.reopenStepCheck(#event)")
    public void onReopenedEvent(CustomServiceLifecycleEvent event) {
        log.debug("Listening onReopenedEvent");
        notificationService.sendNotificationToApprovers(buildNotification(event));
    }

    //----------------------------------------

    /* *
     * ADDITIONAL STEPS
     * */

    @EventListener(condition = "@listenerServiceLifecycleConditionService.unarchivedStepCheck(#event)")
    public void onUnarchivedEvent(CustomServiceLifecycleEvent event) {
        log.debug("Listening onReopenedEvent");
        notificationService.sendNotificationToApprovers(buildNotification(event));
    }

    @EventListener(condition = "@listenerServiceLifecycleConditionService.wrongAfterArchivedStepCheck(#event)")
    public void onWrongAfterArchivedEvent(CustomServiceLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    @EventListener(condition = "@listenerServiceLifecycleConditionService.wrongAfterRemovedStepCheck(#event)")
    public void onWrongAfterRemovedEvent(CustomServiceLifecycleEvent event) {
        log.error(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
        throw new BadRequestException(WRONG_STATE_CHANGING_DUE_TO_LIFECYCLE);
    }

    //--------------------------------------------

    private Notification buildNotification(CustomServiceLifecycleEvent event) {
        return new Notification(event.getSubjectId(), event.getObject().getId(), event.getObject().getClass());
    }
}
