package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.TroubleTicket;

import java.util.List;
import java.util.Optional;

public interface TroubleTicketDao extends GenericDao<TroubleTicket> {

    Optional<TroubleTicket> getTroubleTicketDetailedById(long id);

    List<TroubleTicket> getTroubleTicketsByUserId(long id);

    List<TroubleTicket> getTroubleTicketsByServiceIdAndUserId(long serviceId, long userId);

    List<TroubleTicket> getTroubleTicketsByApproverIdOrNotAssigned(long id);

    Optional<Long> countUnsolvedTroubleTickets();

    Optional<Long> countSolvedTroubleTickets();
}
