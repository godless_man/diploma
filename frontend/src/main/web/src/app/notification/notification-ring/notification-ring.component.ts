import {Component, OnDestroy, OnInit} from '@angular/core';
import {Notification} from '@models/notification';
import {StompClientService} from '@services/stomp-client.service';
import {AuthService} from '@services/auth.service';
import {NotificationsService} from '@services/notifications.service';
import {Role} from '@models/role';
import {ShareDataService} from '@services/share-data.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {TitleCasePipe} from '@angular/common';

@Component({
  selector: 'app-notification-ring',
  templateUrl: './notification-ring.component.html',
  styleUrls: ['./notification-ring.component.scss']
})
export class NotificationRingComponent implements OnInit, OnDestroy {
  counter: number;
  notifications?: Notification[] = [];
  showedNotifications?: Notification[] = [];
  role;
  userId;

  subscription: Subscription = new Subscription();
  titleCasePipe = new TitleCasePipe();

  constructor(private stompClient: StompClientService,
              private authService: AuthService,
              private notificationService: NotificationsService,
              private shareDataService: ShareDataService,
              private router: Router) {
  }

  ngOnInit() {
    this.role = this.authService.getDecodedToken().authority;
    this.userId = this.authService.getDecodedToken().sub;
    this.subscription.add(this.stompClient.lastNotification.subscribe(data => this.newNotifications(data)));
    this.counter = 0;

    if (this.role === Role.Approver) {
      this.subscription.add(this.notificationService.getAllNotReadNotificationsForApprover(this.userId).subscribe(
          data => {
            this.notifications = data;
            this.counter = this.notifications.length;
            this.formArray();
          }
      ))
    }
    if (this.role === Role.Provider) {
      this.subscription.add(this.notificationService.getAllNotReadNotificationsForProvider(this.userId).subscribe(
          data => {
            this.notifications = data;
            this.counter = this.notifications.length;
            this.formArray();
          }
      ))
    }
    if (this.role === Role.User) {
      this.subscription.add(this.notificationService.getAllNotReadNotificationsForUser(this.userId).subscribe(
          data => {
            this.notifications = data;
            this.counter = this.notifications.length;
            this.formArray();
          }
      ))
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  newNotifications(notification: Notification) {
    if (notification) {
      this.notifications.unshift(notification);
      this.formArray();
    }
    this.counter++;
  }

  clearAll() {
    this.counter = 0;
    this.notifications = null;
    this.showedNotifications = null;
  }

  readOne(notification: Notification) {
    this.notifications.splice(this.notifications.indexOf(notification), 1);
    this.formArray();
    this.counter--;
    if (this.counter < 0) {
      this.counter = null
    }
  }

  formArray() {
    this.showedNotifications = [];
    for (const i in this.notifications) {
      if (this.showedNotifications.length < 5) {
        this.showedNotifications.push(this.notifications[i])
      }
    }
  }

  setTicketDataAndRedirect(notification: Notification) {
    if (this.role === Role.Approver) {
      if (notification.serviceId) {
        this.shareDataService.dialog = notification.username;
        this.router.navigate(['chat'])
      } else {
        this.shareDataService.tabIndex = 3;
        this.shareDataService.ticketId = notification.troubleTicketId;
        this.router.navigate(['account']);
      }


    } else if (this.role === Role.User) {
      if (notification.serviceId) {
        this.router.navigate(['chat'])
      } else {
        this.shareDataService.tabIndex = 2;

        this.shareDataService.ticketId = notification.troubleTicketId;
        this.router.navigate(['account']);

      }
    }
  }

  statusCssClass(status: string) {
    if (status) {
      const str = status.split(' ');
      if (str.length > 1) {
        const newStr = [];
        str.forEach(value => newStr.push(this.titleCasePipe.transform(value)));
        return newStr[0] + newStr[1]
      } else {
        return str;
      }
    }
  }
}
