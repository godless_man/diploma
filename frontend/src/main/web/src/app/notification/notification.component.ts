import {Component, OnInit} from '@angular/core';
import {Notification} from '@models/notification';
import {NotificationsService} from '@services/notifications.service';
import {AuthService} from '@services/auth.service';
import {Role} from '@models/role';
import {ShareDataService} from '@services/share-data.service';
import {Router} from '@angular/router';
import {TitleCasePipe} from '@angular/common';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  serviceNotifications?: Notification[] = [];
  troubleTicketsNotifications?: Notification[] = [];
  messageNotifications?: Notification[] = [];

  notifications?: Notification[] = [];
  role = null;
  userId = null;
  titleCasePipe = new TitleCasePipe();

  constructor(private authService: AuthService,
              private notificationService: NotificationsService,
              private shareService: ShareDataService,
              private router: Router) {
  }

  ngOnInit() {
    this.role = this.authService.getDecodedToken().authority;
    this.userId = this.authService.getDecodedToken().sub;
    if (this.role === Role.Approver) {
      this.notificationService.getAllNotReadNotificationsForApprover(this.userId).subscribe(
          data => {
            this.notificationFilter(data);
          }
      )
    }
    if (this.role === Role.Provider) {
      this.notificationService.getAllNotReadNotificationsForProvider(this.userId).subscribe(
          data => {
            this.notificationFilter(data);
          }
      )
    }
    if (this.role === Role.User) {
      this.notificationService.getAllNotReadNotificationsForUser(this.userId).subscribe(
          data => {
            this.notificationFilter(data);
          }
      )
    }
    window.scrollTo(0, 0);
  }

  deleteServiceNot(notification: Notification) {
    this.serviceNotifications.splice(this.serviceNotifications.indexOf(notification), 1);
  }

  deleteTicketNot(notification: Notification) {
    this.troubleTicketsNotifications.splice(this.troubleTicketsNotifications.indexOf(notification), 1);
  }

  notificationFilter(notifications: Notification[]) {
    const that = this;
    notifications.forEach(value => {
      if (value.message === 'MESSAGE') {
        that.messageNotifications.push(value);
      } else if (value.serviceStatus) {
        that.serviceNotifications.push(value);
      } else if (value.troubleTicketStatus) {
        that.troubleTicketsNotifications.push(value);
      }
    })
  }

  setTicketDataAndRedirect(troubleTicketId: number) {
    if (this.role === Role.Approver) {
      this.shareService.tabIndex = 3;
    } else if (this.role === Role.User) {
      this.shareService.tabIndex = 2;
    }
    // this.shareService.serviceId = serviceId;
    this.shareService.ticketId = troubleTicketId;

    this.router.navigate(['account']);
  }

  statusCssClass(status: string) {
    if (status) {
      const str = status.split(' ');
      if (str.length > 1) {
        const newStr = [];
        str.forEach(value => newStr.push(this.titleCasePipe.transform(value)));
        return newStr[0] + newStr[1]
      } else {
        return str;
      }
    }
  }
}
