import {Component, OnInit} from '@angular/core';
import {Bundle} from '@models/bundle';
import {BundleService} from '@services/bundle.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bundle-list',
  templateUrl: './bundle-list.component.html',
  styleUrls: ['./bundle-list.component.scss']
})
export class BundleListComponent implements OnInit {

  trips: Bundle[];

  constructor(private bundleService: BundleService, private router: Router) {
  }

  ngOnInit() {
    this.getAllBundle();
  }

  getAllBundle() {
    this.bundleService.getAllBundles().subscribe(res => {
      this.trips = res;
    });
  }

  intoBundlePage(id) {
    this.router.navigate(['bundle/' + id]);
  }
}
