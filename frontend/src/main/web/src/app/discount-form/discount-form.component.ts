import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Trip} from '@models/trip';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {RightDateOrder} from '@helpers/right-date-order.validator';
import {PercentageDiscount} from '@helpers/percentage-discount.validator';
import {FixedDiscountEased} from '@helpers/fixed-discount-eased.validator';
import {DiscountService} from '@services/discount.service';
import {CustomValidators} from '@helpers/CustomValidators';
import {Discount} from '@models/discount';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-discount-form',
  templateUrl: './discount-form.component.html',
  styleUrls: ['./discount-form.component.scss']
})
export class DiscountFormComponent implements OnInit {

  @Input() service: Trip;
  @Input() editableDiscount: Discount;
  @Output() submit = new EventEmitter<Discount>();
  @Output() delete = new EventEmitter<Discount>();

  readonly FixedDiscountType = 'Fixed discount';
  readonly PercentageDiscountType = 'Percentage';
  readonly FixedDiscountSymbol = '$';
  readonly PercentageDiscountSymbol = '%';

  editMode: boolean;
  submitted = false;
  loading = false;
  error = null;
  deactivationLoading = false;

  discountForm: FormGroup;
  serviceControl: FormControl;
  discountSymbol: string;

  startDatePassed = false;
  endDatePassed = false;

  availableServices: Trip[];

  basePrice: number;

  constructor(private formBuilder: FormBuilder,
              private discountService: DiscountService) {
  }

  get discount() {
    if (this.discountSymbol === this.FixedDiscountSymbol) {
      return this.discountForm.get('amount').value;
    } else if (this.discountSymbol === this.PercentageDiscountSymbol) {
      return (this.basePrice * this.discountForm.get('amount').value) / 100;
    } else return 0;
  }

  get finalPrice() {
    return this.basePrice - this.discount;
  }

  get startDate() {
    if (this.editableDiscount) {
      return formatDate(this.editableDiscount.startDate, 'YYYY/mm/dd', 'en-US')
    } else return ''
  }

  get endDate() {
    if (this.editableDiscount) {
      return formatDate(this.editableDiscount.endDate, 'YYYY/mm/dd', 'en-US')
    } else return ''
  }

  ngOnInit() {
    if (this.editableDiscount == null) {
      this.initAvailableServices();

      this.discountForm = this.initDiscount();

      this.serviceControl = new FormControl(0);

      this.serviceControl.valueChanges.subscribe((data) => {
        this.discountForm.controls.serviceId.patchValue(data);

        if (this.service.id === data) this.basePrice = this.service.price;
        else {
          this.basePrice = this.service.suggestions
              .find((suggestion) => {
                return suggestion.id === data;
              }).price;
        }
      });
    } else {
      this.editMode = true;
      this.discountForm = this.initExistingDiscount(this.editableDiscount);
      this.serviceControl = new FormControl(this.editableDiscount.serviceId);

      if (this.service.id === this.editableDiscount.serviceId) this.basePrice = this.service.price;
      else {
        this.basePrice = this.service.suggestions
            .find((suggestion) => {
              return suggestion.id === this.editableDiscount.serviceId;
            }).price;
      }

      if (this.editableDiscount.type === this.FixedDiscountType) {
        this.discountSymbol = this.FixedDiscountSymbol;
      } else if (this.editableDiscount.type === this.PercentageDiscountType) {
        this.discountSymbol = this.PercentageDiscountSymbol;
      }

      if (this.editableDiscount.startDate < Date.now()) this.startDatePassed = true;
      if (this.editableDiscount.endDate < Date.now()) this.endDatePassed = true;
    }
  }

  initAvailableServices() {

    this.availableServices = new Array<Trip>();

    this.availableServices.push(this.service);

    for (const suggestion of this.service.suggestions) {
      this.availableServices.push(new Trip(suggestion));
    }
  }

  initDiscount() {
    return this.formBuilder.group({
      startDate: ['', [CustomValidators.startDate, Validators.required]],
      endDate: ['', [Validators.required]],
      type: ['', [Validators.required]],
      amount: [0, [Validators.required, Validators.min(0.1)]],
      serviceId: [],
      id: []
    }, {
      validator: [RightDateOrder('startDate', 'endDate'),
        PercentageDiscount(),
        FixedDiscountEased(this.service.price)]
    });
  }

  initExistingDiscount(discountPayload: Discount) {

    return this.formBuilder.group({
      startDate: [discountPayload.startDate, [Validators.required]],
      endDate: [discountPayload.endDate, [Validators.required]],
      type: [discountPayload.type, [Validators.required]],
      amount: [discountPayload.amount, [Validators.required, Validators.min(0.1)]],
      serviceId: [discountPayload.serviceId],
      id: [discountPayload.id]
    }, {
      validator: [RightDateOrder('startDate', 'endDate'),
        PercentageDiscount(),
        FixedDiscountEased(this.service.price)]
    });
  }

  selectChangeHandler(event: any) {

    if (event.target.value === this.FixedDiscountType) {
      this.discountSymbol = this.FixedDiscountSymbol;
    } else if (event.target.value === this.PercentageDiscountType) {
      this.discountSymbol = this.PercentageDiscountSymbol;
    }
  }

  saveDiscount(discountPayload: Discount) {
    this.submitted = true;
    this.loading = true;

    if (this.discountForm.invalid) {
      this.loading = false;
      return;
    }

    if (!this.editMode) {
      this.discountService.createDiscount(discountPayload).subscribe(
        (data) => {
          discountPayload.id = data.id;
          this.submit.emit(discountPayload);
        }, err => {
          this.handleError(err);
        }
      );
    } else {
      this.discountService.updateDiscount(discountPayload).subscribe(
        () => {
          this.submit.emit(discountPayload);
        }, err => {
          this.handleError(err);
        }
      );
    }
  }

  deactivateDiscount() {
    this.deactivationLoading = true;

    if (this.startDatePassed) {
      this.discountForm.controls.endDate.patchValue(Date.now() - 10);
      this.saveDiscount(this.discountForm.value);
    } else {
      this.discountService.deleteDiscount(this.discountForm.value).subscribe(
          () => this.delete.emit(this.discountForm.value),
          err => this.handleError(err)
      );
    }
  }

  handleError(err) {
    this.error = err;
    this.loading = false;
    this.deactivationLoading = false;
    window.setTimeout(() => {
      this.error = '';
    }, 5000);
  }
}
