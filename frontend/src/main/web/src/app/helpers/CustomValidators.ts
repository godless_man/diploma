import {FormControl} from '@angular/forms';

export class CustomValidators {
  static required(control: FormControl) {
    return (typeof control.value === 'string' && control.value.trim() === '') ?
        {required: true} :
        null;
  }

  static minLength(control: FormControl, minLength: number) {
    return (typeof control.value === 'string' && control.value.trim().length < minLength) ?
        {minLength: true} :
        null;
  }

  static startDate(control: FormControl) {
    return (new Date(control.value) < new Date(Date.now())) ?
        {startDate: true} :
        null;
  }
}
