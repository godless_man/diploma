import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {DiscountService} from '@services/discount.service';
import {Discount} from '@models/discount';

@Injectable()
export class TripDiscountsResolver implements Resolve<any> {

  constructor(private  discountService: DiscountService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<Discount[]> {
    return this.discountService.getTripRelatedDiscounts(+route.paramMap.get('id'));
  }
}
