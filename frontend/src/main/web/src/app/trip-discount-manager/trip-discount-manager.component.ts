import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Trip} from '@models/trip';
import {DiscountService} from '@services/discount.service';
import {TableSettings} from '@models/table-settings.model';
import {ActivatedRoute} from '@angular/router';
import {Discount} from '@models/discount';

@Component({
  selector: 'app-trip-discount-manager',
  templateUrl: './trip-discount-manager.component.html',
  styleUrls: ['./trip-discount-manager.component.scss']
})
export class TripDiscountManagerComponent implements OnInit {

  @Input() service: Trip;

  @Output() backEvent = new EventEmitter();

  @Output() updateEvent = new EventEmitter<Discount>();

  discountsTableSettings: TableSettings[] =
    [
      {
        propertyName: 'serviceName'
      },
      {
        propertyName: 'type'
      },
      {
        propertyName: 'amount'
      },
      {
        propertyName: 'startDate',
        type: 'date'
      },
      {
        propertyName: 'endDate',
        type: 'date'
      }
    ];
  discountEditMode = false;
  discountCreationMode = false;

  private tableName = 'Discounts for current trip';
  private discounts: Discount[];
  private editable = true;
  private currDiscount: Discount;

  constructor(private discountService: DiscountService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.discounts = this.route.snapshot.data.tripDiscountsData;

    this.discounts.forEach((discount) => {
      discount.serviceName = discount.service.name
    });
  }

  onSubmit(addedDiscount: Discount) {
    let isNewDiscount = true;
    let i = 0;

    this.discounts.forEach((discountIterator) => {

      if (discountIterator.id === addedDiscount.id) {
        isNewDiscount = false;

        if (new Date(addedDiscount.endDate) < new Date(Date.now())) {
          this.discounts.splice(i, 1);
        } else {
          discountIterator.startDate = addedDiscount.startDate;
          discountIterator.endDate = addedDiscount.endDate;
          discountIterator.amount = addedDiscount.amount;
          discountIterator.type = addedDiscount.type;
        }
      }
      i++;
    });

    if (isNewDiscount) {

      let serviceToAppend: Trip;

      if (this.service.id === addedDiscount.serviceId) serviceToAppend = this.service;
      else {
        for (const suggestion of this.service.suggestions) {
          if (suggestion.id === addedDiscount.serviceId) {
            serviceToAppend = new Trip(suggestion);
            break;
          }
        }
      }
      addedDiscount.service = serviceToAppend;
      addedDiscount.serviceName = serviceToAppend.name;
      this.discounts.push(addedDiscount);
    }

    this.route.snapshot.data.tripDiscountsData = this.discounts;

    this.discountEditMode = false;
    this.discountCreationMode = false;

    this.updateEvent.emit(addedDiscount);
  }

  onDelete(deletedDiscount: Discount) {

    let i = 0;

    this.discounts.forEach((discountIterator) => {

      if (discountIterator.id === deletedDiscount.id) {
        this.discounts.splice(i, 1);
      }

      i++;
    });

    this.route.snapshot.data.tripDiscountsData = this.discounts;

    this.discountEditMode = false;
    this.discountCreationMode = false;

    this.updateEvent.emit(deletedDiscount);
  }

  switchDiscountEditMode() {
    this.discountEditMode = !this.discountEditMode;
  }

  switchDiscountCreationMode() {
    this.discountCreationMode = !this.discountCreationMode;
  }

  switchBackMode() {
    this.backEvent.emit();
  }

  info(event) {
    this.currDiscount = this.discounts
      .find((discount) => {
        return discount.id === event
      });

    this.discountEditMode = true;
  }


}
