import {Injectable} from '@angular/core';
import {COMPLETE_ORDER} from '@environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private http: HttpClient) {
  }

  makePayment() {
    return this.http.get<any>(`${COMPLETE_ORDER}`)
  }
}
