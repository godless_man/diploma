import {Injectable} from '@angular/core';

const CLIENT_ID = '946828012865-5hd0k6f22l95r15943usdhr3ja777adh.apps.googleusercontent.com';
const API_KEY = 'AIzaSyAcaaElwdGQIGIRnr2Ay1Hl9BuqEwDpjeY';
const DISCOVERY_DOCS = ['https://www.googleapis.com/discovery/v1/apis/drive/v3/rest'];
const SCOPES = 'https://www.googleapis.com/auth/drive';

@Injectable()
export class GapiSession {

  initClient() {
    return new Promise(resolve => {
      gapi.load('client:auth2', () => {
        return gapi.client.init({
          apiKey: API_KEY,
          clientId: CLIENT_ID,
          discoveryDocs: DISCOVERY_DOCS,
          scope: SCOPES,
        }).then(() => {
          // this.googleAuth = gapi.auth2.getAuthInstance();
          resolve();
        });
      });
    });
  }
}
