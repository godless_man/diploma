export interface ChatMessage {
  username: string;
  userId: number;
  role: string;
  message: string;
  time: Date;
  email: string;
  destinationId: number;
  chatId: number;
}

export interface Chat {
  id: number;
  userId: number;
  approverId: number;
  username: string;
}
