export interface ApproverDashboard {
  productivityDiagram: ProductivityDiagram;
  waitingDiagram: WaitingDiagram;
}

export interface ProductivityDiagram {
  quantityOfApprovedServices: number;
  quantityOfAnsweredTroubleTickets: number;
}

export interface WaitingDiagram {
  quantityOfUnapprovedServices: number;
  quantityOfUnansweredTroubleTickets: number;
}



