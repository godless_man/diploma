import {OrderDetails} from '@models/orderDetails';

export interface Order {
  id: number;
  orderDate: Date;
  price: number;
  name: string;
  orderDetails : OrderDetails[]
}
