export interface AuthorityToken {
  sub: number, // subject -> userId
  authority: string, // user role
  iat: Date, // issuedAt
  username: string,
  email: string,
  exp: Date, // expiration date
}
