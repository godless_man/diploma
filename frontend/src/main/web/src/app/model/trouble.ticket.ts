export interface TroubleTicket {
  id: number;
  serviceId: number;
  userId: number;
  approverId: number;
  statusId: number;
  headline: string;
  applyTime: Date;

  status: string;

  rating: number;
  text: string;
}

export interface TroubleTicketMessage {
  id: number;
  troubleTicketId: number;
  userId: number;
  message: string;
  messageTime: Date;
  username: string;
  email: string;
}
