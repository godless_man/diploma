export interface Notification {
  id: number;
  userId: number;
  serviceId?: number;
  troubleTicketId?: number;
  message?: string;
  notificationTime: Date;
  isRead: boolean;
  username?: string;
  serviceName?: string;
  serviceTypeName?: string;
  serviceStatus?: string;
  troubleTicketStatus?: string;
}
