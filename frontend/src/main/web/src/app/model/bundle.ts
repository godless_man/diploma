import {Trip} from './trip';
import {City} from '@models/city';
import {User} from '@models/user';

export interface Bundle {
  id?: number;
  name: string;
  description?: string;
  numberOfPeople: number;
  typeId: number,
  statusId: number,
  providerId: number,
  price: number;
  bundleTrips: Trip[];
  location: City;
  destination: City;
  imgSrc?: string;
  oneWay?: boolean;

  provider?: User;
}

