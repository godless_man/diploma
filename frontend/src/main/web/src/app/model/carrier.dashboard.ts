export interface CarrierDashboard {
  sales: Sales[];
  views: Views[];
}

export interface Sales {
  salesBy: string;
  salesAmount: number;
}

export interface Views {
  viewsBy: string;
  viewsAmount: number;
}

