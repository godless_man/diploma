export interface OrderDetails{
  serviceId : number;
  name : string;
  price : number;
}
