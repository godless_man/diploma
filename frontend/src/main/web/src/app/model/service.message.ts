export interface ServiceMessage {
  id: number;
  serviceId: number;
  messageTime: Date;
  message: string;

  approverId: number;
  approverUsername: string;
}
