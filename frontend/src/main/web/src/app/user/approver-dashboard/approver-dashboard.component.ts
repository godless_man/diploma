import {AfterContentInit, Component, OnInit} from '@angular/core';
import {ApproverDashboard, ProductivityDiagram, WaitingDiagram} from '@models/approver.dashboard';
import {HttpClient} from '@angular/common/http';
import {DashboardService} from '@services/dashboard.service';

@Component({
    selector: 'app-approver-dashboard',
    templateUrl: './approver-dashboard.component.html',
    styleUrls: ['./approver-dashboard.component.scss']
})
export class ApproverDashboardComponent implements OnInit, AfterContentInit {

    // DOUGHNUT CHART of ProductivityDiagram
    labelsOfProductivity = ['Approved Services', 'Answered Trouble Tickets'];
    labelsOfWaiting = ['Waited Services', 'Waited Trouble Tickets'];
    public doughnut = 'doughnut';
    public doughnutDataSets: Array<any>;
    public doughnutLabels: Array<any>;
    public doughnutColors: Array<any> = [
        {
            backgroundColor: ['#F7464A', '#46BFBD'],
            hoverBackgroundColor: ['#FF5A5E', '#5AD3D1'],
            borderWidth: 2,
        }
    ];
    public doughnutOptions: any = {
        responsive: true
    };
    // ----------------

    // DOUGHNUT CHART of WaitingDiagram
    public doughnutOfWaitingDiagram = 'doughnut';
    public doughnutDataSetsOfWaitingDiagram: Array<any>;
    public doughnutLabelsOfWaitingDiagram: Array<any>;
    public doughnutColorsOfWaitingDiagram: Array<any> = [
        {
            backgroundColor: ['#F7464A', '#46BFBD'],
            hoverBackgroundColor: ['#FF5A5E', '#5AD3D1'],
            borderWidth: 2,
        }
    ];
    public doughnutOptionsOfWaitingDiagram: any = {
        responsive: true
    };
    // ----------------

    waitingDiagram: WaitingDiagram = {
        quantityOfUnapprovedServices: 0,
        quantityOfUnansweredTroubleTickets: 0

    };

  productivityDiagram : ProductivityDiagram  = {
    quantityOfApprovedServices: 0,
    quantityOfAnsweredTroubleTickets: 0
  };

  private approverDashboard : ApproverDashboard = {
    waitingDiagram : this.waitingDiagram,
    productivityDiagram : this.productivityDiagram

  };

  dataOfProductivityDiagram = [];
  dataOfWaitingDiagram = [];

  constructor(private http: HttpClient, private dashboardService: DashboardService) {
  }

  ngOnInit() {
    this.dashboardService.getApproverDashboard().subscribe(data =>
    {
      this.approverDashboard = data;
      this.dataOfProductivityDiagram.push(this.approverDashboard.productivityDiagram.quantityOfAnsweredTroubleTickets);
      this.dataOfProductivityDiagram.push(this.approverDashboard.productivityDiagram.quantityOfApprovedServices);
      this.dataOfWaitingDiagram.push(this.approverDashboard.waitingDiagram.quantityOfUnansweredTroubleTickets);
      this.dataOfWaitingDiagram.push(this.approverDashboard.waitingDiagram.quantityOfUnapprovedServices);

    })
  }

  ngAfterContentInit() {
    this.doughnutDataSets = [
      {
        data: this.dataOfProductivityDiagram,
        label: 'Service amount'
      }
    ];
    this.doughnutLabels = this.labelsOfProductivity;

    this.doughnutDataSetsOfWaitingDiagram = [
      {
        data: this.dataOfWaitingDiagram,
        label: 'Service amount'
      }
    ];

    this.doughnutLabelsOfWaitingDiagram = this.labelsOfWaiting;

  }

}
