import {Component, OnInit} from '@angular/core';
import {User} from '@models/user';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {GET_USER_SUBSCRIPTIONS} from '@environments/environment';
import {ProviderService} from '@services/provider.service';
import {SnackbarComponent} from '@models/snackbar/snackbar.component';
import {ShareDataService} from '@services/share-data.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-subscriptions',
  templateUrl: './user-subscriptions.component.html',
  styleUrls: ['./user-subscriptions.component.scss']
})
export class UserSubscriptionsComponent implements OnInit {
  providers: User[] = [];

  constructor(private http: HttpClient,
              private router: Router,
              private providerService: ProviderService,
              private shareDataService: ShareDataService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.http.get<User[]>(`${GET_USER_SUBSCRIPTIONS}`).subscribe(
      data => {
        this.providers = data;
      }, err => {
        alert(err);
      });
  }

  deleteOne(provider: User) {
    this.providers.splice(this.providers.indexOf(provider), 1);
    this.providerService.unsubscribeToProvider(provider.id).subscribe(() => {
      this.openSnackBar('Subscription cancelled successfully')
    }, err => {
      alert(err);
    });
  }

  openSnackBar(message: string) {
    this.shareDataService.snackBarMessage = message;
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: 1000
    })
  }
}
