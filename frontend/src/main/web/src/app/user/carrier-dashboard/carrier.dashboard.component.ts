import {AfterContentInit, Component, OnInit} from '@angular/core';
import {CarrierDashboard, Sales, Views} from '@models/carrier.dashboard';
import {HttpClient} from '@angular/common/http';
import {GET_CARRIER_DASHBOARDS} from '@environments/environment';

@Component({
  selector: 'app-carrier-dashboard',
  templateUrl: './carrier.dashboard.component.html',
  styleUrls: ['./carrier.dashboard.component.scss']
})
export class CarrierDashboardComponent implements OnInit, AfterContentInit {

  // POLAR CHART Of Sales
  public pie = 'pie';
  public pieDataSets: Array<any>;
  public pieLabels: Array<any>;
  public pieColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'],
      borderWidth: 2,
    }
  ];
  public pieOptions: any = {
    responsive: true
  };
  // ----------------

  // POLAR CHART Of Views
  public pieOfViews = 'pie';
  public pieDataSetsOfViews: Array<any>;
  public pieLabelsOfViews: Array<any>;
  public pieColorsOfViews: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'],
      borderWidth: 2,
    }
  ];
  public pieOptionsOfViews: any = {
    responsive: true
  };
  // ----------------

  private carrierDashboard: CarrierDashboard = {
    sales: [],
    views: []
  };

  salesBy: string[];
  salesAmount: number[];
  viewsBy: string[];
  viewsAmount: number[];

  constructor(private http: HttpClient) {
    this.salesBy = [];
    this.salesAmount = [];
    this.viewsBy = [];
    this.viewsAmount = [];
  }

  ngOnInit() {
    this.http.get<CarrierDashboard>(`${GET_CARRIER_DASHBOARDS}`).subscribe(
      data => {
        if (data) {

          this.carrierDashboard = data;
          this.carrierDashboard.sales.forEach((element: Sales) => {
            this.salesBy.push(element.salesBy);
            this.salesAmount.push(element.salesAmount);
          });
          this.carrierDashboard.views.forEach((element: Views) => {
            this.viewsBy.push(element.viewsBy);
            this.viewsAmount.push(element.viewsAmount);
          });
        }
      }, err => {
        alert(err);
      })
  }

  ngAfterContentInit() {
    this.pieDataSets = [
      {data: this.salesAmount, label: 'Sales amount'}
    ];
    this.pieLabels = this.salesBy;

    this.pieDataSetsOfViews = [
      {data: this.viewsAmount, label: 'Views amount'}
    ];
    this.pieLabelsOfViews = this.viewsBy;
  }

  // Events
  public pieClicked(e: any): void {
  }

  public pieHovered(e: any): void {
  }
}
