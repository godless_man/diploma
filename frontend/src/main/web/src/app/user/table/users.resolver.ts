import {Resolve} from '@angular/router';
import {Injectable} from '@angular/core';
import {UsersService} from '@services/users.service';
import {Observable} from 'rxjs';
import {User} from '@models/user';

@Injectable()
export class UserResolver implements Resolve<any> {


  constructor(private userService: UsersService) {
  }

  resolve(): Observable<User[]> {
    return this.userService.getAllUsers();
  }
}
