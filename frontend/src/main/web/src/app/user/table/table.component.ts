import {Component, OnInit} from '@angular/core';
import {User} from '@models/user';
import {TableSettings} from '@models/table-settings.model';
import {UsersService} from '@services/users.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
    tableName = 'Users';
    users: User[];
    editable = true;
    urlToForward = '/user/';

    creationMode = false;

    usersTableSettings: TableSettings[] =
        [
            {
                propertyName: 'active',
                type: 'status'
            },
            {
                propertyName: 'email',
            },
            {
                propertyName: 'authorityId',
                type: 'authority'
            },
            {
                propertyName: 'username',
            },
    ];

  error = '';
  success = '';

  constructor(private usersService: UsersService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.users = this.route.snapshot.data.userData;
  }

  info(event) {
      this.router.navigate([]).then(() => {
          window.open(this.urlToForward + event);
      });
  }

  delete(record) {
    this.usersService.deleteUserById(record.id).subscribe(
        () => {
            this.users.splice(this.users.indexOf(record), 1);
            this.success = 'Deleted successful';
            window.setTimeout(() => {
                this.success = '';
            }, 5000);
        }, () => {
            this.error = 'Deleting Failed';
            window.setTimeout(() => {
                this.error = '';
            }, 5000);
        }
    )
  }

  switchMode() {
    this.creationMode = !this.creationMode;
  }
}
