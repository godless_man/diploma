import {Component, OnInit} from '@angular/core';
import {TableSettings} from '@models/table-settings.model';
import {ActivatedRoute, Router} from '@angular/router';
import {BundleService} from '@services/bundle.service';
import {Bundle} from '@models/bundle';
import {AuthService} from '@services/auth.service';
import {SnackbarComponent} from '@models/snackbar/snackbar.component';
import {ShareDataService} from '@services/share-data.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-bundles',
  templateUrl: './user-bundles.component.html',
  styleUrls: ['./user-bundles.component.scss']
})
export class UserBundlesComponent implements OnInit {

  tableName = 'Trips';
  bundles: Bundle[];
  editable = true;
  deleting = true;
  url = '/bundle/';
  showTable = true;

  bundlesTableSettings: TableSettings[] = [
    {
      propertyName: 'name'
    },
    {
      propertyName: 'location',
      type: 'location'
    },
    {
      propertyName: 'destination',
      type: 'location'
    },
    {
      propertyName: 'numberOfPeople'
    },
    {
      propertyName: 'price',
      type: 'currency'
    },


  ];

  constructor(private bundleService: BundleService,
              private route: ActivatedRoute,
              private router: Router,
              private shareDataService: ShareDataService,
              private snackBar: MatSnackBar,
              private authService: AuthService) {
  }

  get isAdmin() {
    return this.authService.getDecodedToken().authority === 'ROLE_ADMIN'
  }

  ngOnInit() {
    this.bundles = this.route.snapshot.data.bundleData;
  }

  info(event) {
    this.router.navigate(['bundle/' + event]);
  }

  delete(record) {
    this.bundleService.deleteBundleById((record as Bundle).id).subscribe(() => {
      // this.shareDataService.tabIndex = 2;
      // this.router.navigate(['blank']).then(() => this.router.navigate(['account']));
      this.openSnackBar('Bundle deleted successfully');
    })
  }

  switchMode() {
    this.showTable = !this.showTable;
  }

  openSnackBar(message: string) {
    this.shareDataService.snackBarMessage = message;
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: 1000
    })
  }
}
