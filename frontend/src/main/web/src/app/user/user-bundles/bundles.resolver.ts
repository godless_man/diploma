import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {Observable} from "rxjs";
import {BundleService} from "@services/bundle.service";
import {Bundle} from "@models/bundle";

@Injectable()
export class BundlesResolver implements Resolve<any> {

  constructor(private bundleService: BundleService) {
  }

  resolve(): Observable<Bundle[]> {
    return this.bundleService.getAllBundles();
  }
}
