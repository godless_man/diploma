import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Bundle} from '@models/bundle';
import {BundleService} from '@services/bundle.service';
import {TripService} from '@services/trip.service';
import {SnackbarComponent} from '@models/snackbar/snackbar.component';
import {ShareDataService} from '@services/share-data.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {FileInfo} from '@models/file-info';
import {GoogleDriveService} from '@services/google-drive.service';
import {Md5} from 'ts-md5';
import {GOOGLE_DRIVE_PICTURE_BASE_URL} from '@environments/environment';

@Component({
    selector: 'app-bundle-form',
    templateUrl: './bundle-form.component.html',
    styleUrls: ['./bundle-form.component.scss']
})
export class BundleFormComponent implements OnInit {

    @Input() bundle: Bundle;

  @Input() updateMode: boolean;

  itemList = [];
  selectedItems = [];
  itemListOfTrips = [];
  selectedFullTripItems = [];
  settings = {};

  submitted = false;
  error = '';

  currentPicture: FileInfo = null;
  currentPictureInputPlaceholder: string;

  bundleForm = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    numberOfPeople: new FormControl(0, [Validators.required, Validators.min(1), Validators.max(10)]),
    bundleTrips: new FormControl([[], Validators.required]),
    price: new FormControl(0, [Validators.required, Validators.min(0.1), Validators.max(10000000)]),
    imgSrc: new FormControl(null)
  });

  constructor(private formBuilder: FormBuilder,
              private bundleService: BundleService,
              private shareDataService: ShareDataService,
              private snackBar: MatSnackBar,
              private router: Router,
              private tripService: TripService,
              private fileUploader: GoogleDriveService) {

  }

  get form() {
    return this.bundleForm.controls;
  }

  ngOnInit() {
    this.currentPictureInputPlaceholder = 'Choose file';

    if (this.updateMode) {
      this.bundleForm = this.initPredefinedBundle();
    } else {
      this.bundle = {
          id: 0,
          name: ' ',
          description: ' ',
          numberOfPeople: 0,
          typeId: 0,
          statusId: 0,
          providerId: 0,
          price: 0,
          bundleTrips: this.itemList,
          location: this.itemList[0],
          destination: this.itemList[this.itemList.length - 1],
          imgSrc: ''

      };
    }

    this.tripService.getAllTripsWithDetails().subscribe(trips => {
      trips.forEach(trip => {
        this.itemList.push({id: trip.id, itemName: trip.name});
        this.itemListOfTrips.push(trip);
      })
    });

    this.settings = {
        singleSelection: false,
        text: 'Select Trips',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        badgeShowLimit: 3
    };

  }

  initPredefinedBundle() {
    return this.formBuilder.group({

      name: [this.bundle.name],
      description: [this.bundle.description],
      numberOfPeople: [this.bundle.numberOfPeople],
      typeId: [this.bundle.typeId],
      statusId: [this.bundle.statusId],
      providerId: [this.bundle.providerId],
      price: [this.bundle.price],
      bundleTrips: [this.bundle.bundleTrips],
      location: [this.bundle.bundleTrips[0]],
      destination: [this.bundle.bundleTrips[this.bundle.bundleTrips.length - 1]],
      imgSrc: [this.bundle.imgSrc]
    });
  }

  /*initBundle() {
    let bundleOld = this.initPredefinedBundle();
    let tripsArray = this.bundle.bundleTrips;
  }*/

  addBundleValues() {
      this.bundle.name = this.bundleForm.value.name;
      this.bundle.description = this.bundleForm.value.description;
      this.bundle.numberOfPeople = this.bundleForm.value.numberOfPeople;
      this.bundle.typeId = 3;
      this.bundle.statusId = 4;
      this.bundle.providerId = 41;
      this.bundle.price = this.bundleForm.value.price;
      this.selectedItems.forEach(selectedItem => {
          this.itemListOfTrips.forEach(itemList => {
              if (selectedItem.id === itemList.id) {
                  this.selectedFullTripItems.push(itemList)
              }
          })
      });

      this.bundle.bundleTrips = this.selectedFullTripItems;
      this.bundle.imgSrc = this.form.imgSrc.value;
  }

  numberOfPeopleChanged() {
      this.itemList = [];
      this.selectedItems = [];
      this.itemListOfTrips.forEach(trip => {
          if (trip.numberOfPeople === this.bundleForm.value.numberOfPeople) {
              this.itemList.push({id: trip.id, itemName: trip.name});
          }
      })
  }

  onTripSelect(event) {
      if (this.bundleForm.value.numberOfPeople !== '')
          return;
      else {
          this.itemList = [];
          this.itemListOfTrips.forEach(trip => {
              if (trip.id === event.id) {
                  this.bundleForm.patchValue({numberOfPeople: trip.numberOfPeople});
              }
              if (trip.numberOfPeople === this.bundleForm.value.numberOfPeople) {
                  this.itemList.push({id: trip.id, itemName: trip.name});
              }
          });
      }
  }

  createBundle(bundle: Bundle) {
      this.bundleService.createBundle(bundle).subscribe(createdBundle => {
          this.openSnackBar('Bundle created successfully');
          this.router.navigate(['bundle/' + createdBundle.id]);
      });
  }

  onSubmit() {
    this.submitted = true;

    if (this.bundleForm.invalid || this.selectedItems.length <= 1) {
      return;
    }

    if (this.currentPicture != null) {
      this.fileUploader.importFile(
          this.currentPicture,
          (err) => {
              this.error = err;
              window.setTimeout(() => {
                  this.error = '';
              }, 5000);
              return;
          },
          (res) => this.onImportComplete(res),
          () => {
          }
      );
      this.currentPicture = null;
      return;
    }

    this.addBundleValues();
    this.createBundle(this.bundle);
  }

  onFileChanged(event) {
      const file: File = event.target.files[0];

      const fileInfo: FileInfo = new FileInfo();

      this.currentPictureInputPlaceholder = file.name;

      fileInfo.Name = Md5.hashStr(file.name) + Date.now().toString();
      fileInfo.Blob = file;

      this.currentPicture = fileInfo;
  }

  onImportComplete(res) {
    this.form.imgSrc.patchValue(GOOGLE_DRIVE_PICTURE_BASE_URL + JSON.parse(res).id);

    this.onSubmit();
  }

  openSnackBar(message: string) {
    this.shareDataService.snackBarMessage = message;
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: 1000
    })
  }
}
