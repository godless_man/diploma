import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ServiceService} from '@services/service.service';
import {Service} from '@models/service';
import {FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {TroubleTicketService} from '@services/trouble.ticket.service';
import {TroubleTicket, TroubleTicketMessage} from '@models/trouble.ticket';
import {AuthService} from '@services/auth.service';
import {StompClientService} from '@services/stomp-client.service';
import {UsersService} from '@services/users.service';
import {ShareDataService} from '@services/share-data.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackbarComponent} from '@models/snackbar/snackbar.component';
import {Role} from '@models/role';
import {Utils} from '@helpers/utils.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-user-trouble-tickets',
  templateUrl: './user-trouble-tickets.component.html',
  styleUrls: ['./user-trouble-tickets.component.scss']
})
export class UserTroubleTicketsComponent implements OnInit, OnDestroy {

  position = 'after';
  max = 5;
  min = 1;
  value = 0;
  currentServiceId = 0;
  approverUsername = null;
  submittedMessage = false;
  submittedFeedback = false;

  purchasedServices: Service[];

  troubleTicket: TroubleTicket;
  selectedTroubleTicket: TroubleTicket;

  troubleTickets: TroubleTicket[];
  assignedToMe: TroubleTicket[];
  unassigned: TroubleTicket[];
  closed: TroubleTicket[];

  newMessage: string;

  currentMessages: TroubleTicketMessage[];

  isDisabled = false;

  serviceControl = new FormControl();
  modalHeadline = new FormControl('', [Validators.required, Validators.maxLength(120)]);
  modalMessage = new FormControl('', [Validators.required, Validators.maxLength(2000)]);
  feedbackControl = new FormControl('', Validators.maxLength(2000));
  message = new FormControl('', [Validators.required, Validators.maxLength(2000)]);

  subscription: Subscription = new Subscription();
  gravatar = Utils;

  serviceName: string = null;

  @ViewChild('ticketsChat') private scrollContainer: ElementRef;

  constructor(private serviceService: ServiceService,
              private aRoute: ActivatedRoute,
              private router: Router,
              private troubleTicketService: TroubleTicketService,
              private authService: AuthService,
              private stompClientService: StompClientService,
              private usersService: UsersService,
              private shareDataService: ShareDataService,
              private snackBar: MatSnackBar) {
    this.assignedToMe = [];
    this.unassigned = [];
    this.closed = [];
    this.selectedTroubleTicket = {
      id: 0,
      serviceId: 0,
      userId: 0,
      approverId: null,
      statusId: 0,
      headline: null,
      applyTime: null,

      status: null,

      rating: 0,
      text: null
    };

    this.subscription.add(this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.showTroubleTicket();
      }
    }));
  }

  get isApprover() {
    return this.authService.getDecodedToken().authority === Role.Approver;
  }

  get serviceSelected() {
    return this.currentServiceId !== 0 && this.currentServiceId != null;
  }

  get troubleTicketSelected() {
    return this.selectedTroubleTicket.id !== 0 && this.selectedTroubleTicket.id != null;
  }

  get canAssign() {
    return (this.selectedTroubleTicket.approverId === 0 || this.selectedTroubleTicket.approverId == null)
      && (this.selectedTroubleTicket.statusId === 1 || this.selectedTroubleTicket.statusId === 4);
  }

  get canSetAnswered() {
    return this.selectedTroubleTicket.statusId === 2 && this.selectedTroubleTicket.userId === this.authService.getDecodedToken().sub;
  }

  get canPostMessageApprover() {
    if (this.isApprover) {
      return this.selectedTroubleTicket.approverId === this.authService.getDecodedToken().sub;
    } else {
      return true;
    }
  }

  get canPostMessage() {
    return this.selectedTroubleTicket.statusId === 1 || this.selectedTroubleTicket.statusId === 2
      || this.selectedTroubleTicket.statusId === 4
      || (this.selectedTroubleTicket.statusId === 3 && this.selectedTroubleTicket.userId === this.authService.getDecodedToken().sub);
  }

  get isAnswered() {
    return this.selectedTroubleTicket.statusId === 3;
  }

  get isRated() {
    return this.selectedTroubleTicket.statusId === 5;
  }

  get isFeedback() {
    return this.selectedTroubleTicket.statusId === 6;
  }

  get status() {
    return Utils.troubleTicketStatus.get(this.selectedTroubleTicket.statusId);
  }

  ngOnInit() {
    this.subscription.add(this.stompClientService.troubleTicketCreating.subscribe(
      data => {
        if (data) {
          if (this.isApprover) {
            this.initTroubleTickets(0);
          }
          if (this.selectedTroubleTicket.id === data.id) {
            this.selectedTroubleTicket = data;
            if (this.selectedTroubleTicket.statusId === 4) {
              this.approverUsername = null;
            }
            this.loadTroubleTicketMessages(data.id);
          }
        }
      }
    ));

    this.subscription.add(this.stompClientService.troubleTicketsMessage.subscribe(
      data => {
        if (data) {
          if (this.selectedTroubleTicket.id === data.troubleTicketId) {
            this.loadTroubleTicketMessages(data.troubleTicketId);
          }
        }
      }
    ));

    this.subscription.add(this.stompClientService.troubleTicketChanging.subscribe(
      data => {
        if (data) {
          if (this.isApprover) {
            this.initTroubleTickets(0);
          } else {
            if (data.serviceId === this.currentServiceId) {
              this.initTroubleTickets(this.currentServiceId);
            }
          }

          if (this.selectedTroubleTicket.id === data.id) {
            this.selectedTroubleTicket = data;
            this.loadTroubleTicketMessages(data.id);
            this.usersService.getUserSummaryById(this.selectedTroubleTicket.approverId).subscribe(
              userSummary => {
                this.approverUsername = userSummary.username;
              }
            )
          }
        }
      }
    ));

    if (!this.isApprover) {
      this.subscription.add(this.serviceService.getPurchasedServices().subscribe(data => {
        this.purchasedServices = data;
      }));
      this.subscription.add(this.serviceControl.valueChanges.subscribe((data) => {
        this.currentServiceId = data;
        this.initTroubleTickets(this.currentServiceId);
      }));
    } else {
      this.initTroubleTickets(0);
    }

    this.showTroubleTicket();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  initTroubleTickets(serviceId: number) {
    this.troubleTicketService.getTroubleTicketByServiceId(serviceId).subscribe(data => {
      this.troubleTickets = data;
      if (this.isApprover) {
        this.closed = [];
        this.unassigned = [];
        this.assignedToMe = [];
        data.forEach((element: TroubleTicket) => {
          if (element.approverId === 0 || element.approverId == null) {
            this.unassigned.push(element);
          } else if (element.approverId === this.authService.getDecodedToken().sub) {
            if (element.statusId === 5 || element.statusId === 6) {
              this.closed.push(element);
            } else {
              this.assignedToMe.push(element);
            }
          }
        })
      }
    })
  }

  loadTroubleTicketMessages(id: number) {
    this.troubleTicketService.getTroubleTicketMessagesById(id).subscribe(data => {
      this.currentMessages = data;
    });
    if (this.selectedTroubleTicket.approverId) {
      this.usersService.getUserSummaryById(this.selectedTroubleTicket.approverId).subscribe(
        data => {
          this.approverUsername = data.username;
        }
      )
    } else {
      this.approverUsername = null;
    }

    if (this.selectedTroubleTicket) {
      this.serviceService.getServiceById(this.selectedTroubleTicket.serviceId).subscribe(data => {
        this.serviceName = data.name;
      })
    }

  }

  createTroubleTicket() {
    if (this.modalHeadline.errors || this.modalMessage.errors) {
      return;
    }
    this.isDisabled = true;
    this.troubleTicketService.createTroubleTicketWithFirstMessage(this.currentServiceId,
      this.modalHeadline.value, this.modalMessage.value)
      .subscribe(() => {
        this.serviceService.getPurchasedServices().subscribe(data => {
          this.purchasedServices = data;
        });
        this.initTroubleTickets(this.currentServiceId);
        this.openSnackBar('Trouble ticket created');
      });
    this.isDisabled = false;
  }

  postTroubleTicketMessage() {
    this.submittedMessage = true;

    if (!this.message.value || this.message.errors || !this.message.value.trim()) {
      return;
    } else {
      this.newMessage = this.message.value.trim();
    }

    this.isDisabled = true;

    if (this.selectedTroubleTicket.statusId === 3) {
      this.troubleTicketService.postMessage(this.selectedTroubleTicket.id, this.newMessage).subscribe(() => {
        this.initTroubleTickets(this.currentServiceId);
        this.loadTroubleTicketMessages(this.selectedTroubleTicket.id);
        this.openSnackBar('Trouble ticket reopened');
      });
      this.troubleTicket = this.selectedTroubleTicket;
      this.troubleTicket.approverId = 0;
      this.troubleTicketService.changeStatus(this.troubleTicket, 4).subscribe(() => {
        this.troubleTicket.statusId = 4;
        this.troubleTicket.status = 'Reopened';
        this.selectedTroubleTicket = this.troubleTicket;
        this.approverUsername = null;
      })
    } else {
      this.troubleTicketService.postMessage(this.selectedTroubleTicket.id, this.newMessage).subscribe(() => {
        this.loadTroubleTicketMessages(this.selectedTroubleTicket.id);
      })
    }
    this.isDisabled = false;
    this.message.reset();
    Utils.scrollToBottom(this.scrollContainer);
  }

  assignTicket() {
    this.isDisabled = true;
    this.troubleTicket = this.selectedTroubleTicket;
    this.troubleTicket.approverId = this.authService.getDecodedToken().sub;
    this.troubleTicketService.changeStatus(this.troubleTicket, 2).subscribe(() => {
      this.troubleTicket.statusId = 2;
      this.troubleTicket.status = 'In progress';
      this.usersService.getUserSummaryById(this.troubleTicket.approverId).subscribe(
        data => {
          this.approverUsername = data.username;
        }
      );
      this.selectedTroubleTicket = this.troubleTicket;
      this.initTroubleTickets(0);
      this.openSnackBar('Trouble ticket assigned');
    });
    this.isDisabled = false;
  }

  markTicketAsAnswered() {
    this.isDisabled = true;
    this.troubleTicket = this.selectedTroubleTicket;
    this.troubleTicketService.changeStatus(this.troubleTicket, 3).subscribe(() => {
      this.troubleTicket.statusId = 3;
      this.troubleTicket.status = 'Answered';
      this.selectedTroubleTicket = this.troubleTicket;
      this.openSnackBar('Trouble ticket marked as answered');
    });
    this.isDisabled = false;
  }

  postTroubleTicketFeedback() {
    this.submittedFeedback = true;
    if (this.feedbackControl.errors || this.value < 1) {
      return;
    }

    this.isDisabled = true;

    this.troubleTicketService.postFeedback(this.selectedTroubleTicket.id, this.value,
      this.feedbackControl.value).subscribe(() => {
      if (this.feedbackControl.value) {
        this.troubleTicket = this.selectedTroubleTicket;
        this.troubleTicket.rating = this.value;
        this.troubleTicket.text = this.feedbackControl.value.trim();
        this.troubleTicketService.changeStatus(this.troubleTicket, 6).subscribe(() => {
          this.troubleTicket.statusId = 6;
          this.troubleTicket.status = 'Feedback';
          this.selectedTroubleTicket = this.troubleTicket;
        })
      } else {
        this.troubleTicket = this.selectedTroubleTicket;
        this.troubleTicket.rating = this.value;
        this.troubleTicketService.changeStatus(this.troubleTicket, 5).subscribe(() => {
          this.troubleTicket.statusId = 5;
          this.troubleTicket.status = 'Rated';
          this.selectedTroubleTicket = this.troubleTicket;
          this.openSnackBar('Trouble ticket feedback added');
        })
      }
    });
    this.isDisabled = false;
    this.message.reset();
  }

  onKeyDown(event, type: string) {
    if (event.key === 'Enter') {
      if (type === 'feedback') {
        this.postTroubleTicketFeedback();
        this.message.reset();
        event.preventDefault();
      }
      if (type === 'message') {
        this.postTroubleTicketMessage();
        this.message.reset();
        event.preventDefault();
      }
    }
  }

  showTroubleTicket() {
    if (this.shareDataService.ticketId !== 0 && this.shareDataService.ticketId != null) {
      this.troubleTicketService.getTroubleTicketById(this.shareDataService.ticketId).subscribe(data => {
        if (!this.isApprover) {
          this.currentServiceId = data.serviceId;
          this.initTroubleTickets(this.currentServiceId);
        }
        this.selectedTroubleTicket = data;
        this.loadTroubleTicketMessages(this.shareDataService.ticketId);
      })
    }
  }

  openSnackBar(message: string) {
    this.shareDataService.snackBarMessage = message;
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: 1000
    })
  }
}
