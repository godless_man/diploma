import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatTableDataSource} from '@angular/material/table';
import {ReportService} from '@services/report.service';
import {ExcelService} from '@helpers/excel.service';
import {Report} from '@models/report';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RightDateOrder} from '@helpers/right-date-order.validator';
import {SnackbarComponent} from '@models/snackbar/snackbar.component';
import {ShareDataService} from '@services/share-data.service';

export interface TypeService {
    value: string;
    viewValue: string;
}

@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  displayedColumns: string[] = ['location', 'numberTour', 'price'];
  dataSource = new MatTableDataSource();

  reportForm: FormGroup;
  submitted = false;
  loading = false;

  excelData: Report[];

  service: string;

  services: TypeService[] =
    [
      {value: 'trip', viewValue: 'Trip'},
      {value: 'service', viewValue: 'Service'},
      {value: 'bundle', viewValue: 'Bundle'},
      {value: 'suggestion', viewValue: 'Suggestion'}
    ];


  constructor(private reportService: ReportService,
              private excelService: ExcelService,
              private formBuilder: FormBuilder,
              private shareDataService: ShareDataService,
              private snackBar: MatSnackBar) {
  }


  ngOnInit() {
    this.reportForm = this.formBuilder.group({
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      serviceType: [null, [Validators.required]]
    }, {validator: [RightDateOrder('startDate', 'endDate')]})

    // this.generateTable();
  }

  generateTable() {
      this.submitted = true;
      this.loading = true;

      if (this.reportForm.invalid) {
          this.loading = false;
          return;
      }

      this.reportService.getTour(
          [this.reportForm.controls.startDate.value, this.reportForm.controls.endDate.value],
          this.reportForm.controls.serviceType.value
      ).subscribe(res => {
              this.excelData = res;
              this.dataSource = new MatTableDataSource(res);
              this.submitted = false;
              this.loading = false;
              this.openSnackBar('Report generated');
          },
          () => {
              this.loading = false;
          }
      );
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.excelData, 'TripStatistic');
  }

  openSnackBar(message: string) {
    this.shareDataService.snackBarMessage = message;
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: 1000
    })
  }
}
