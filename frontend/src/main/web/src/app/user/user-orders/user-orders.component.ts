import {Component, OnInit} from '@angular/core';
import {Order} from '@models/order';
import {OrderService} from '@services/order.service';
import {AuthService} from '@services/auth.service';

@Component({
  selector: 'app-user-orders',
  templateUrl: './user-orders.component.html',
  styleUrls: ['./user-orders.component.scss']
})

export class UserOrdersComponent implements OnInit {

  orders: Order[] = [];

  panelOpenState = false;

  constructor(private orderService: OrderService, private authService: AuthService) {
  }

  ngOnInit() {
    this.orderService.getOrdersOfUser(this.authService.getDecodedToken().sub).subscribe(orders => {
      this.orders = orders;
    });
  }
}
