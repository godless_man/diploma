import {Component, OnInit} from '@angular/core';
import {Bundle} from '@models/bundle';
import {ActivatedRoute} from '@angular/router';
import {BundleService} from '@services/bundle.service';
import {AuthService} from '@services/auth.service';
import {ViewService} from '@services/view.service';
import {UserBasketComponent} from '../system/header/user-basket/user-basket.component';

@Component({
    selector: 'app-bundle',
    templateUrl: './bundle.component.html',
    styleUrls: ['./bundle.component.scss'],
    providers: [UserBasketComponent]
})
export class BundleComponent implements OnInit {

    bundle: Bundle;
    bundleEditMode = false;
  authority: string;
  views: number;

  constructor(private route: ActivatedRoute,
              private bundleService: BundleService,
              private userBasket: UserBasketComponent,
              private viewService: ViewService,
              private authService: AuthService) {
  }

  get getEditMode() {
    return this.bundleEditMode;
  }

  ngOnInit() {
    if (this.authService.getDecodedToken()) {
      this.authority = this.authService.getDecodedToken().authority;
    }

    this.bundle = this.route.snapshot.data.bundleData;

    this.route.params.subscribe(params => {
        this.viewService.countViewByTripId(params.id).subscribe(data => {
            this.views = data;
        }, err => {
            alert(err);
        });
    });

    window.scrollTo(0, 0);
  }

  switchBundleEditMode() {
    this.bundleEditMode = !this.bundleEditMode;
  }
}



