import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {BundleService} from '@services/bundle.service';
import {Bundle} from '@models/bundle';

@Injectable()
export class BundleResolver implements Resolve<any> {

    constructor(private bundleService: BundleService) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<Bundle> {
        return this.bundleService.getBundleById(+route.paramMap.get('id'));
    }
}
