import {Component, OnDestroy, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {title} from "@environments/environment";
import {StompClientService} from "@services/stomp-client.service";
import {AuthService} from "@services/auth.service";
import {LoadingService} from "@services/loading.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  loading: boolean = false;
  loadingSubscription: Subscription;

  public constructor(private titleService: Title,
                     private authService: AuthService,
                     private stompClient: StompClientService,
                     private loadingService: LoadingService) {
    this.setTitle(title);
    if (!authService.currentUserValue || authService.isTokenExpired()) {
      authService.logout();
    } else {
      stompClient.connect();
    }
  }

  ngOnInit(): void {
    this.loadingSubscription = this.loadingService.test.subscribe((value) => {
      this.loading = value;
    });
  }

  ngOnDestroy(): void {
    this.loadingSubscription.unsubscribe();
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }
}
