import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {User} from '@models/user';
import {UsersService} from '@services/users.service';

@Injectable()
export class ProviderResolver implements Resolve<any> {

  constructor(private  usersService: UsersService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<User> {
    return this.usersService.getUserSummaryById(+route.paramMap.get('id'));
  }
}
