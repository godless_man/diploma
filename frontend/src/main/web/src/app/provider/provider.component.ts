import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {User} from '@models/user';
import {ProviderService} from '@services/provider.service';
import {Trip} from '@models/trip';
import {TripService} from '@services/trip.service';
import {SnackbarComponent} from '@models/snackbar/snackbar.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ShareDataService} from '@services/share-data.service';
import {AuthService} from '@services/auth.service';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent implements OnInit {
  trips: Trip[];
  public isViewable: boolean;
  user: User;

  authority: string;

  isDisabled = false;

  subscriptions: number;

  constructor(private providerService: ProviderService,
              private route: ActivatedRoute,
              private tripService: TripService,
              private authService: AuthService,
              private shareDataService: ShareDataService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    if (this.authService.getDecodedToken()) {
      this.authority = this.authService.getDecodedToken().authority;
    }

    this.route.params.subscribe(params => {
      this.providerService.isSubscribed(params.id).subscribe(data => {
        this.isViewable = !data;
      });

      this.tripService.getAllByProviderId(params.id).subscribe(res => {
        this.trips = res;
      });

      this.providerService.getSubscriptionCount(params.id).subscribe(data => {
        this.subscriptions = data;
      });
    });
    this.user = this.route.snapshot.data.providerData;
    window.scrollTo(0, 0);
  }

  public toggleSub(): void {
    this.isDisabled = true;
    this.route.params.subscribe(() => {
      this.providerService.subscribeToProvider(this.user.id).subscribe();
      this.openSnackBar('Subscribed successfully');
    });
    this.isDisabled = false;
    this.isViewable = !this.isViewable;
  }

  public toggleUnSub(): void {
    this.isDisabled = true;
    this.route.params.subscribe(params => {
      this.providerService.unsubscribeToProvider(params.id).subscribe();
      this.openSnackBar('Unsubscribed successfully');
    });
    this.isDisabled = false;
    this.isViewable = !this.isViewable;
  }

  openSnackBar(message: string) {
    this.shareDataService.snackBarMessage = message;
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: 1000
    })
  }
}
