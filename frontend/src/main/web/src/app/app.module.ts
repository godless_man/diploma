import {BrowserModule, Title} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './system/header/header.component';
import {LoginComponent} from './auth/login/login.component';
import {RegistrationComponent} from './auth/registration/registration.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IndexComponent} from './system/index/index.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {EditUserComponent} from './user/edit-user/edit-user.component';
import {PasswordRecoveryComponent} from './auth/password-recovery/password-recovery.component';
import {routing} from './app.routing';
import {UserComponent} from './user/user.component';
import {JwtInterceptor} from '@helpers/jwt.interceptor';
import {HttpErrorInterceptor} from '@helpers/HttpError.interceptor';
import {UserSummaryComponent} from './user/user-summary/user-summary.component';
import {UserUsersComponent} from './user/user-users/user-users.component';
import {UserBundlesComponent} from './user/user-bundles/user-bundles.component';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {UserTripsComponent} from './user/user-trips/user-trips.component';
import {UserServicesComponent} from './user/user-services/user-services.component';
import {UserDiscountsComponent} from './user/user-discounts/user-discounts.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DashboardComponent} from './user/admin-dashboard/admin.dashboard.component';
import {LayoutModule} from '@angular/cdk/layout';

import {MDBBootstrapModule} from 'angular-bootstrap-md';

import {ToTitlePipe} from './system/data-table/toTitle.pipe';
import {FormatCellPipe} from './system/data-table/formatCell.pipe';
import {DataTableComponent} from './system/data-table/data-table.component';
import {RegistrationSuccessfulComponent} from './auth/registration/registration-successful/registration-successful.component';
import {MaterialModule} from '@helpers/material.module';
import {CurrencyPipe, DatePipe} from '@angular/common';
import {SuggestionServiceComponent} from './user/user-trips/suggestion-service/suggestion-service.component';
import {TripSuggestionComponent} from './user/user-trips/trip-suggestion/trip-suggestion.component';
import {TableComponent} from './user/table/table.component';
import {UserResolver} from './user/table/users.resolver';
import {NotificationRingComponent} from './notification/notification-ring/notification-ring.component';
import {NotificationComponent} from './notification/notification.component';
import {ReportComponent} from './user/user-report/report.component';

import {TripFormComponent} from './user/user-trips/trip-form/trip-form.component';
import {TripsResolver} from './user/user-trips/trips.resolver';
import {CarrierDashboardComponent} from './user/carrier-dashboard/carrier.dashboard.component';
import {ServiceButtonsComponent} from './service.buttons/service.buttons.component';
import {TripComponent} from './trip/trip.component';
import {TripResolver} from './trip/trip.resolver';
import {RegistrationConfirmComponent} from './auth/registration/registration-confirm/registration-confirm.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {EditProfileComponent} from './user-profile/edit-profile/edit-profile.component';
import {ApproverDashboardComponent} from './user/approver-dashboard/approver-dashboard.component';
import {UserBasketComponent} from './system/header/user-basket/user-basket.component';
import {UserBasketTotalComponent} from './system/header/user-basket-total/user-basket-total.component';
import {UserBasketTotalPriceComponent} from './system/header/user-basket-total-price/user-basket-total-price.component';
import {CheckoutComponent} from './checkout/checkout.component';
import {DiscountFormComponent} from './discount-form/discount-form.component';
import {TripDiscountManagerComponent} from './trip-discount-manager/trip-discount-manager.component';
import {TripDiscountsResolver} from './trip-discount-manager/trip-discounts.resolver';
import {StompClientService} from '@services/stomp-client.service';
import {AgmCoreModule} from '@agm/core';
import {GlobalChatComponent} from './chat/global-chat/global-chat.component';
import {UserTroubleTicketsComponent} from './user/user-trouble-tickets/user-trouble-tickets.component';
import {ApproversChatComponent} from './chat/approvers-chat/approvers-chat.component';
import {SupportChatComponent} from './chat/support-chat/support-chat.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {ChatComponent} from './chat/chat-component';
import {ShareDataService} from '@services/share-data.service';
import {SnackbarComponent} from './model/snackbar/snackbar.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ProviderComponent} from './provider/provider.component';
import {ProviderResolver} from './provider/provider.resolver';
import {UserSubscriptionsComponent} from './user/user-subscriptions/user-subscriptions.component';


import {BundleFormComponent} from './user/user-bundles/bundle-form/bundle-form.component';
import {BundleComponent} from './bundle/bundle.component';
import {UserOrdersComponent} from './user/user-orders/user-orders.component';
import {BundleResolver} from './bundle/bundle.resolver';
import {BundlesResolver} from './user/user-bundles/bundles.resolver';
import {BlankPageComponent} from './trip/blank-page/blank-page.component';
import {BundleListComponent} from './bundle-list/bundle-list.component';
import {GapiSession} from './session/gapi.session';
import {AuthorityPipe} from './system/data-table/authority.pipe';
import {ChatTemplateComponent} from './chat/templates/chat/chat-template.component';
import {YourOwnMessageTemplateComponent} from './chat/templates/your-own-message/your-own-message-template.component';
import {SomeonesMessageTemplateComponent} from './chat/templates/someones-message/someones-message-template.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    RegistrationComponent,
    IndexComponent,
    UserComponent,
    EditUserComponent,
    PasswordRecoveryComponent,
    UserSummaryComponent,
    UserUsersComponent,
    UserBundlesComponent,
    UserTripsComponent,
    UserServicesComponent,
    UserDiscountsComponent,
    DashboardComponent,
    RegistrationSuccessfulComponent,
    DataTableComponent,
    FormatCellPipe,
    ToTitlePipe,
    SuggestionServiceComponent,
    TripSuggestionComponent,
    ReportComponent,
    TableComponent,
    NotificationRingComponent,
    NotificationComponent,
    TripFormComponent,
    CarrierDashboardComponent,
    ServiceButtonsComponent,
    RegistrationConfirmComponent,
    ApproverDashboardComponent,
    TripComponent,
    UserProfileComponent,
    EditProfileComponent,
    GlobalChatComponent,
    UserTroubleTicketsComponent,
    ApproversChatComponent,
    SupportChatComponent,
    ChatComponent,
    SnackbarComponent,
    DiscountFormComponent,
    TripDiscountManagerComponent,
    EditProfileComponent,
    UserBasketComponent,
    UserBasketTotalComponent,
    UserBasketTotalPriceComponent,
    CheckoutComponent,
    ProviderComponent,
    UserSubscriptionsComponent,
    BundleFormComponent,
    BundleComponent,
    UserOrdersComponent,
    BlankPageComponent,
    BundleListComponent,
    AuthorityPipe,
    ChatTemplateComponent,
    YourOwnMessageTemplateComponent,
    SomeonesMessageTemplateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    routing,
    BrowserAnimationsModule,
    LayoutModule,
    MDBBootstrapModule.forRoot(),
    MaterialModule,
    AngularMultiSelectModule,
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAcaaElwdGQIGIRnr2Ay1Hl9BuqEwDpjeY'
    }),
    NgbModule,
    NgSelectModule
  ],
  providers: [
    Title,
    CurrencyPipe,
    DatePipe,
    UserResolver,
    FormatCellPipe,
    TripsResolver,
    AuthorityPipe,
    TripResolver,
    TripDiscountsResolver,
    StompClientService,
    BundleResolver,
    BundlesResolver,
    ShareDataService,
    ProviderResolver,
    GapiSession,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true},
    // {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptorService, multi: true},
    {provide: APP_INITIALIZER, useFactory: initGapi, deps: [GapiSession], multi: true}
  ],
  bootstrap: [AppComponent],
  entryComponents: [SnackbarComponent]
})
export class AppModule {
}

export function initGapi(gapiSession: GapiSession) {
  return () => gapiSession.initClient();
}
