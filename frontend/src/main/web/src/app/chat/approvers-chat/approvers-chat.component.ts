import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {StompClientService} from '@services/stomp-client.service';
import {Chat, ChatMessage} from '@models/chat.models';
import {AuthService} from '@services/auth.service';
import {MessageService} from '@services/message.service';
import {Utils} from '@helpers/utils.service';
import {AuthorityToken} from '@models/authority-token';

@Component({
  selector: 'app-approvers-chat',
  templateUrl: './approvers-chat.component.html',
  styleUrls: ['./approvers-chat.component.scss']
})
export class ApproversChatComponent implements OnInit {
  inputValue = null;
  chats: Chat[] = [];
  chosenChat: Chat = null;
  currentChatMessages: ChatMessage[];
  gravatar = Utils;
  token: AuthorityToken;

  @ViewChild('approversChat') private scrollContainer: ElementRef;

  constructor(private stompClientService: StompClientService,
              private authService: AuthService,
              private messageService: MessageService) {

  }

  ngOnInit() {
    this.token = this.authService.getDecodedToken();

    this.messageService.getChatsForApprover().subscribe(
      data => {
        if (data) {
          this.chats = data;
        }
      }
    );
    this.stompClientService.messagesForSupportChat.subscribe(
      message => {
        if (message) {
          if (this.chosenChat) {
            if (this.chosenChat.id === message.chatId) {
              this.currentChatMessages.push(message);
              Utils.scrollToBottom(this.scrollContainer);
            }
          }
          this.messageService.getChatsForApprover().subscribe(
            data => {
              this.chats = data;
            }
          );
        }
      }
    );
  }

  sendMessage(message: string) {
    if (!message.trim() || !this.chosenChat) {
      return
    }
    const messagePayload: ChatMessage = {
      username: this.token.username,
      userId: this.token.sub,
      destinationId: this.chosenChat.userId,
      message,
      chatId: this.chosenChat.id,
      time: new Date(),
      email: this.token.email,
      role: this.token.authority
    };

    this.stompClientService.sendMessageToSupportChat(messagePayload);
    this.inputValue = '';
    Utils.scrollToBottom(this.scrollContainer);
  }

  getChatByChatId(chat: Chat) {
    this.chosenChat = chat;
    this.messageService.getChatMessagesByChatId(chat.id).subscribe(
      data => {
        this.currentChatMessages = data;
      }
    )
  }

  getCurrentChatMessages() {
    if (this.chosenChat) {
      return this.currentChatMessages
    } else {
      return []
    }
  }
}
