import {Component, Input, OnInit} from '@angular/core';
import {ChatMessage} from '@models/chat.models';

@Component({
  selector: 'app-your-own-message',
  templateUrl: './your-own-message-template.component.html',
  styleUrls: ['./your-own-message-template.component.scss']
})
export class YourOwnMessageTemplateComponent implements OnInit {
  @Input() message: ChatMessage;

  constructor() {
  }

  ngOnInit(): void {
  }

}
