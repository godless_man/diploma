import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {ChatMessage} from '@models/chat.models';
import {User} from '@models/user';

@Component({
  selector: 'app-chat-template',
  templateUrl: './chat-template.component.html',
  styleUrls: ['./chat-template.component.scss']
})
export class ChatTemplateComponent {
  @Input() gravatarLink: string;
  @Input() currentUserName: string;
  @Input() listOfChatMessages: ChatMessage[];
  @Output() sendMessageEvent = new EventEmitter<string>();

  inputValue = null;
  currentUser: User;
  @ViewChild('chatWindow', {static: true}) private scrollContainer: ElementRef;

  onKeyDown(event, msg: string) {
    if (event.key === 'Enter') {
      this.sendMessage(msg);
      event.preventDefault();
    }
  }

  sendMessage(input: string) {
    this.sendMessageEvent.emit(input)
  }
}
