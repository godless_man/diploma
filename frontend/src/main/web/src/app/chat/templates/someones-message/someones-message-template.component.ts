import {Component, Input, OnInit} from '@angular/core';
import {ChatMessage} from '@models/chat.models';

@Component({
  selector: 'app-someones-message',
  templateUrl: './someones-message-template.component.html',
  styleUrls: ['./someones-message-template.component.scss']
})
export class SomeonesMessageTemplateComponent implements OnInit {
  @Input() message: ChatMessage;
  @Input() gravatarLink: string;

  constructor() {
  }

  ngOnInit(): void {
  }
}
