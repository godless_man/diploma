import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {StompClientService} from '@services/stomp-client.service';
import {Chat, ChatMessage} from '@models/chat.models';
import {AuthService} from '@services/auth.service';
import {MessageService} from '@services/message.service';
import {Utils} from '@helpers/utils.service';
import {AuthorityToken} from '@models/authority-token';

@Component({
  selector: 'app-support-chat',
  templateUrl: './support-chat.component.html',
  styleUrls: ['./support-chat.component.scss']
})
export class SupportChatComponent implements OnInit {
  value = null;
  notConfirmedMessages: ChatMessage[] = [];

  currentChat: Chat = null;
  messages: ChatMessage[] = [];
  gravatar = Utils;
  token: AuthorityToken;

  @ViewChild('supportChat') private scrollContainer: ElementRef;

  constructor(private stompClientService: StompClientService,
              private authService: AuthService,
              private messageService: MessageService) {

  }

  ngOnInit() {
    this.token = this.authService.getDecodedToken();

    this.messageService.getChatForCurrentUser().subscribe(
      chat => {
        this.currentChat = chat;
        this.messageService.getChatMessagesByChatId(chat.id).subscribe(
          data => {
            this.messages = data;
          }
        );
      }
    );
    this.stompClientService.messagesForSupportChat.subscribe(
      data => {
        if (data) {
          if (data.userId === this.token.sub) {
            this.checkIfSent(data);
          } else {
            this.messages.push(data);
          }
          Utils.scrollToBottom(this.scrollContainer);
        }
      }
    );
    Utils.scrollToBottom(this.scrollContainer);
  }

  sendMsg(msg: string) {
    if (!msg.trim()) {
      return;
    }

    const messagePayload: ChatMessage = {
      username: this.token.username,
      userId: this.token.sub,
      role: this.token.authority,
      message: msg,
      time: new Date(),
      email: this.token.email,
      destinationId: this.currentChat.userId,
      chatId: this.currentChat.id,
    };
    this.stompClientService.sendMessageToSupportChat(messagePayload);
    this.notConfirmedMessages.push(messagePayload);
    this.messages.push(messagePayload);
    this.value = null;
    Utils.scrollToBottom(this.scrollContainer);
  }

  checkIfSent(message: ChatMessage) {
    if (this.notConfirmedMessages.indexOf(message)) {
      this.notConfirmedMessages.splice(this.notConfirmedMessages.indexOf(message), 1);
    }
  }
}
