import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ChatMessage} from '@models/chat.models';
import {StompClientService} from '@services/stomp-client.service';
import {AuthService} from '@services/auth.service';
import {Utils} from '@helpers/utils.service';
import {AuthorityToken} from '@models/authority-token';

@Component({
  selector: 'app-global-chat',
  templateUrl: './global-chat.component.html',
  styleUrls: ['./global-chat.component.scss']
})
export class GlobalChatComponent implements OnInit {

  messages: ChatMessage[] = [];
  value = null;
  role: string;
  notConfirmedMessages: ChatMessage[] = [];
  gravatar = Utils;
  token: AuthorityToken;

  @ViewChild('globalChat') private scrollContainer: ElementRef;

  constructor(private stompClientService: StompClientService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.stompClientService.messageInGlobalChat.subscribe(
      data => {
        if (data) {
          if (data && data.userId === this.token.sub) {
            this.checkIfSent(data);
          } else {
            this.messages.push(data);
          }
          Utils.scrollToBottom(this.scrollContainer);
        }
      }
    );
    this.token = this.authService.getDecodedToken();
    Utils.scrollToBottom(this.scrollContainer);
  }

  sendMessage(message: string) {
    if (!message.trim()) {
      return
    }
    const messagePayload: ChatMessage = {
      username: this.token.username,
      userId: this.token.sub,
      role: this.token.authority,
      message,
      time: new Date(),
      email: this.token.email,
      destinationId: this.token.sub,
      chatId: null
    };

    this.stompClientService.sendMessageToGlobalChat(messagePayload);
    this.notConfirmedMessages.push(messagePayload);
    this.messages.push(messagePayload);
    this.value = null;
    Utils.scrollToBottom(this.scrollContainer);
  }

  checkIfSent(message: ChatMessage) {
    if (this.notConfirmedMessages.indexOf(message)) {
      this.notConfirmedMessages.splice(this.notConfirmedMessages.indexOf(message), 1);
    }
  }
}
