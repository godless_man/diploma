import {Component, OnInit} from '@angular/core';
import {AuthService} from '@services/auth.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  authority;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.authority = this.authService.getDecodedToken().authority;
  }
}
