import {RouterModule, Routes} from '@angular/router';
import {EditUserComponent} from "./user/edit-user/edit-user.component";
import {RegistrationComponent} from "./auth/registration/registration.component";
import {AuthGuard} from "./auth/auth.guard";
import {PasswordRecoveryComponent} from "./auth/password-recovery/password-recovery.component";
import {LoginComponent} from "./auth/login/login.component";
import {UserComponent} from "./user/user.component";
import {RegistrationSuccessfulComponent} from "./auth/registration/registration-successful/registration-successful.component";
import {UserResolver} from "./user/table/users.resolver";
import {TripsResolver} from "./user/user-trips/trips.resolver";
import {IndexComponent} from "./system/index/index.component";
import {NotificationComponent} from "./notification/notification.component";
import {RegistrationConfirmComponent} from "./auth/registration/registration-confirm/registration-confirm.component";
import {TripComponent} from "./trip/trip.component";
import {TripResolver} from "./trip/trip.resolver";
import {UserProfileComponent} from "./user-profile/user-profile.component";
import {EditProfileComponent} from "./user-profile/edit-profile/edit-profile.component";
import {TripDiscountsResolver} from "./trip-discount-manager/trip-discounts.resolver";
import {ChatComponent} from "./chat/chat-component";
import {CheckoutComponent} from "./checkout/checkout.component";
import {ProviderComponent} from "./provider/provider.component";
import {ProviderResolver} from "./provider/provider.resolver";
import {BundlesResolver} from "./user/user-bundles/bundles.resolver";
import {BundleComponent} from "./bundle/bundle.component";
import {BundleResolver} from "./bundle/bundle.resolver";
import {BlankPageComponent} from "./trip/blank-page/blank-page.component";
import {Role} from "@models/role";
import {BundleListComponent} from "./bundle-list/bundle-list.component";


const appRoutes: Routes = [
  {path: 'registration', component: RegistrationComponent},
  {path: 'passwordRecovery', component: PasswordRecoveryComponent},
  {path: 'registrationSuccessful', component: RegistrationSuccessfulComponent},
  {path: 'registrationConfirm/:token', component: RegistrationConfirmComponent},
  {
    path: 'account',
    component: UserComponent,
    canActivate: [AuthGuard],
    resolve: {userData: UserResolver, tripData: TripsResolver, bundleData: BundlesResolver},
    runGuardsAndResolvers: 'always'
  },
  {path: 'account/edit', component: EditUserComponent, canActivate: [AuthGuard]},
  {path: 'edit/:id', component: EditProfileComponent, canActivate: [AuthGuard], data: {roles: [Role.Admin]}},
  {path: 'login', component: LoginComponent},
  {
    path: 'trip/:id',
    component: TripComponent,
    resolve: {tripData: TripResolver, tripDiscountsData: TripDiscountsResolver},
    runGuardsAndResolvers: 'always',
  },
  {path: 'blank', component: BlankPageComponent},
  {path: 'user/:id', component: UserProfileComponent},
  {path: 'bundles', component: BundleListComponent},
  {path: 'bundle/:id', component: BundleComponent, resolve: {bundleData: BundleResolver}},
  {path: 'provider/:id', component: ProviderComponent, resolve: {providerData: ProviderResolver}},
  {
    path: 'notifications',
    component: NotificationComponent,
    canActivate: [AuthGuard],
    data: {roles: [Role.User, Role.Approver, Role.Provider]}
  },
  {path: 'checkout', component: CheckoutComponent, canActivate: [AuthGuard]},
  {path: 'chat', component: ChatComponent, canActivate: [AuthGuard]},
  {path: '', component: IndexComponent},
  {path: '**', redirectTo: ''},
];

export const routing = RouterModule.forRoot(appRoutes, {useHash: true, onSameUrlNavigation: "reload"});
