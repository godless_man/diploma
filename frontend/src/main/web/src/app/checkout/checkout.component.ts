import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserBasketComponent} from '../system/header/user-basket/user-basket.component';
import {PaymentService} from '@services/payment.service';

@Component({
    selector: 'app-checkout',
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.scss'],
    providers: [UserBasketComponent]
})
export class CheckoutComponent implements OnInit {
    public isViewable: boolean;

    isDisabled = false;

    constructor(private http: HttpClient,
                private userBasket: UserBasketComponent,
                private paymentService: PaymentService) {
    }

    ngOnInit() {
        this.isViewable = true;
        window.scrollTo(0, 0);
        if (this.userBasket.getLenght() === 0) {
            this.isDisabled = true;
        }
    }

    pay() {
        this.paymentService.makePayment().subscribe();
        this.userBasket.clear();
        this.isViewable = false;
    }
}
