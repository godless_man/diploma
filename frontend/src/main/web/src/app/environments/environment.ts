export const environment = {
    production: false,
    ws: 'http://localhost:8080/socket'
    // ws: '/socket'
};

export const title = 'TripGod';

// export const BASE_URL = "/";

export const SERVER_URL = 'http://localhost:8080/';
export const API_VERSION = 'api';
export const BASE_URL = SERVER_URL + API_VERSION;

export const LOGIN_URL = BASE_URL + '/auth/signin';
export const REGISTRATION_URL = BASE_URL + '/auth/signup';
export const REGISTRATION_SPECIAL_USER_URL = BASE_URL + '/account/users';
export const PASSWORD_RECOVERY_URL = BASE_URL + '/auth/recovery';
export const REGISTRATION_CONFIRM_URL = BASE_URL + '/auth/signup/';

export const GET_ALL_USERS = BASE_URL + '/account/users';
export const GET_CURRENT_USER = BASE_URL + '/account/summary';
export const GET_USER_BY_ID = BASE_URL + '/account/summary/';

export const DELETE_USER_BY_ID = BASE_URL + '/account/users';

export const EDIT_SUMMARY = BASE_URL + '/account/summary/edit';
export const CHANGE_PASSWORD_SUMMARY = BASE_URL + '/account/summary/changePassword';

export const GET_ADMIN_DASHBOARDS = BASE_URL + '/account/dashboards/admin';
export const GET_CARRIER_DASHBOARDS = BASE_URL + '/account/dashboards/carrier';
export const GET_APPROVER_DASHBOARDS = BASE_URL + '/account/dashboards/approver';

export const GET_ALL_TRIPS = BASE_URL + '/account/trips';
export const GET_ALL_TRIPS_WITH_DETAIL = BASE_URL + '/account/trips/withDetail';
export const GET_ALL_TRIPS_FOR_CURRENT_USER = BASE_URL + '/account/trips/currentUser';
export const GET_TRIP_BY_ID = BASE_URL + '/account/trips/';
export const GET_TRIP_BY_PROVIDER_ID = BASE_URL + '/account/trips/forProvider/';
export const GET_ALL_TRIPS_SORTED_BY_IMG = BASE_URL + '/account/trips/withImg';
export const CREATE_OR_UPDATE_TRIP_URL = BASE_URL + '/account/trips';

export const CREATE_BUNDLE_URL = BASE_URL + '/account/bundles';

export const GET_PURCHASED_SERVICES = BASE_URL + '/account/services/purchased';
export const GET_TROUBLE_TICKET_BY_ID = BASE_URL + '/account/troubleTickets/';
export const GET_TROUBLE_TICKET_BY_SERVICE_ID = BASE_URL + '/account/troubleTickets/service/';
export const GET_TROUBLE_TICKET_MESSAGES_BY_ID = BASE_URL + '/account/troubleTickets/messages/';
export const CREATE_TROUBLE_TICKET = BASE_URL + '/account/troubleTickets';
export const POST_TT_MESSAGE = BASE_URL + '/account/troubleTickets/messages/';
export const POST_TT_FEEDBACK = BASE_URL + '/account/troubleTickets/feedback';
export const CHANGE_TT_STATUS = BASE_URL + '/account/troubleTickets';

export const GET_SERVICE_MESSAGE = BASE_URL + '/trip/spec/';
export const POST_CLARIFICATION_REQUEST = BASE_URL + '/trip/spec';
export const CHANGE_STATUS_APPROVER = BASE_URL + '/trip/spec/approver';
export const CHANGE_STATUS_PROVIDER = BASE_URL + '/trip/spec/carrier';

export const GET_TRIP_FEEDBACK_BY_TRIP_ID = BASE_URL + '/feedback/trip/';
export const CHECK_USER_FEEDBACK_PERMIT_FOR_TRIP_ID = BASE_URL + '/feedback/trip/checkFeedbackPermit/';
export const ADD_FEEDBACK = BASE_URL + '/feedback';
export const UPDATE_FEEDBACK = BASE_URL + '/feedback';
export const DELETE_FEEDBACK = BASE_URL + '/feedback/';

// export const GET_ALL_TOURS = BASE_URL + "/account/report/";

export const GET_REPORT_URL = BASE_URL + '/account/report/';

export const GET_ALL_BUNDLE_WITH_IMG = BASE_URL + '/account/bundles/withImg';
export const GET_ALL_BUNDLES = BASE_URL + '/account/bundles';
export const GET_BUNDLE_BY_ID = BASE_URL + '/account/bundles/';
export const DELETE_BUNDLE_BY_ID = BASE_URL + '/account/bundles/';
export const GET_ORDERS_OF_USER = BASE_URL + '/account/orders/';

export const GET_ALL_COUNTRIES = BASE_URL + '/account/search/country';
export const GET_SEARCHED_TRIPS_BY_RATING = BASE_URL + '/account/search/rating';
export const GET_SEARCHED_TRIPS_BY_PROVIDER = BASE_URL + '/account/search/provider';
export const GET_SEARCHED_TRIPS_BY_DISCOUNT = BASE_URL + '/account/search/discount';
export const GET_SEARCHED_TRIPS_BY_PRICE = BASE_URL + '/account/search/price';
export const GET_SEARCHED_TRIPS_BY_LENGTH = BASE_URL + '/account/search/length';
export const GET_SEARCHED_TRIPS_BY_BUNDLE = BASE_URL + '/account/search/bundle';

export const DISCOUNT_CONTROLLER = BASE_URL + '/discounts';
export const DELETE_DISCOUNT = BASE_URL + '/discounts/';
export const GET_ALL_TRIP_RELATED_DISCOUNTS_BY_TRIP_ID = BASE_URL + '/discounts/trip/';

export const VIEW_CONTROLLER = BASE_URL + '/views';
export const COUNT_VIEWS_BY_TRIP_ID = VIEW_CONTROLLER + '/trip/';
export const COUNT_VIEWS_BY_BUNDLE_ID = VIEW_CONTROLLER + '/bundle/';

export const GET_SERVICES_BY_PROVIDER_ID = BASE_URL + '/account/services/provider';

export const GET_ALL_NOT_READ_NOTIFICATION_FOR_APPROVER = BASE_URL + '/notifications/approver/';
export const GET_ALL_NOT_READ_NOTIFICATION_FOR_PROVIDER = BASE_URL + '/notifications/provider/';
export const GET_ALL_NOT_READ_NOTIFICATION_FOR_USER = BASE_URL + '/notifications/user/';

// export const GET_ALL_MESSAGES_FOR_USER_BY_ID = BASE_URL + "/messages/user/";

export const GET_CHAT_FOR_CURRENT_USER = BASE_URL + '/chat';
export const GET_CHATS_FOR_APPROVER = BASE_URL + '/chat/approver';
export const GET_CHAT_MESSAGES_BY_CHAT_ID = BASE_URL + '/chat/';
export const POST_CHAT_MESSAGE_WITH_CHAT_ID = BASE_URL + '/chat/';
export const PUT_CHANGING_CHAT_ASSIGNMENT = BASE_URL + '/chat';

export const GET_USER_BASKET = BASE_URL + '/basket';
export const ADD_TO_USER_BASKET = BASE_URL + '/basket/';
export const COMPLETE_ORDER = BASE_URL + '/basket/pay';

export const SUBSCRIBE_BY_ID = BASE_URL + '/subscribe/';
export const GET_SUB_COUNT_FOR_PROVIDER = BASE_URL + '/subscribe/count/';
export const GET_USER_SUBSCRIPTIONS = BASE_URL + '/subscribe';
export const GET_COUNTRIES = BASE_URL + '/countries';

export const GET_CITIES_BY_COUNTRY_ID = BASE_URL + '/cities/country/';

export const GRAVATAR_BASE_URL = 'https://www.gravatar.com/avatar/';

export const GOOGLE_DRIVE_PICTURE_BASE_URL = 'https://drive.google.com/uc?id=';
export const GOOGLE_DRIVE_OAUTH2_REFRESH_TOKEN_URL = 'https://www.googleapis.com/oauth2/v4/token';

export const GET_SERVICE_BY_ID = BASE_URL + '/account/services/';

// export const CVET_YAIZ_DROZDA = "#1fcecb";
