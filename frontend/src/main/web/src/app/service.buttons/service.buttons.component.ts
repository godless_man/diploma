import {AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ServiceService} from '../shared/service.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../shared/auth.service';
import {ServiceMessage} from '@models/service.message';
import {Trip} from '@models/trip';
import {ActivatedRoute, Router} from '@angular/router';
import {TripService} from '@services/trip.service';
import {UsersService} from '@services/users.service';
import {Discount} from '@models/discount';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackbarComponent} from '@models/snackbar/snackbar.component';
import {ShareDataService} from '@services/share-data.service';
import {User} from '@models/user';
import {Utils} from '@helpers/utils.service';
import {LoadingService} from '@services/loading.service';
import {TitleCasePipe} from '@angular/common';

@Component({
  selector: 'app-service-buttons',
  templateUrl: './service.buttons.component.html',
  styleUrls: ['./service.buttons.component.scss']
})
export class ServiceButtonsComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() trip: Trip;

  @Output() editEvent = new EventEmitter<Trip>();

  messageForm: FormGroup;

  tripEditMode = false;
  discountManagerMode = false;

  approver: User = null;
  gravatar = Utils;
  isDisabled = false;
  titleCasePipe = new TitleCasePipe();

  get isApprover(): boolean {
    return this.authService.getDecodedToken().authority === 'ROLE_APPROVER';
  }

  get isProvider(): boolean {
    return this.authService.getDecodedToken().authority === 'ROLE_PROVIDER';
  }

  get isDrafted(): boolean {
    return this.trip.status === 'Draft';
  }

  constructor(private serviceService: ServiceService,
              private tripService: TripService,
              private userService: UsersService,
              private formBuilder: FormBuilder,
              private authService: AuthService,
              private route: Router,
              private aRoute: ActivatedRoute,
              private shareDataService: ShareDataService,
              private snackBar: MatSnackBar,
              private loadingService: LoadingService) {
  }

  get isArchived(): boolean {
    return this.trip.status === 'Archived';
  }

  get isAssigned(): boolean {
    return this.trip.approverId !== 0;
  }

  get isUnderClarification(): boolean {
    return this.trip.status === 'Under clarification';
  }

  get hasMessage(): boolean {
    return this.serviceMessage.message !== '' && this.serviceMessage.message != null;
  }

  get isAssignedToMe(): boolean {
    return this.trip.approverId === this.authService.getDecodedToken().sub;
  }

  get isPublished(): boolean {
    return this.trip.status === 'Published';
  }

  get thisTrip(): Trip {
    return this.trip;
  }

  get thisServiceMessage(): ServiceMessage {
    return this.serviceMessage;
  }

  get isOpened(): boolean {
    return this.trip.status === 'Open';
  }

  get isRemoved(): boolean {
    return this.trip.status === 'Removed';
  }

  serviceMessage: ServiceMessage = {
    id: 0,
    serviceId: 0,
    messageTime: null,
    message: '',
    approverId: 0,
    approverUsername: ''
  };

  getApproverSummary() {
    if (this.trip.approverId && this.trip.approverId !== 0) {
      this.userService.getUserSummaryById(this.trip.approverId).subscribe(data => {
        this.approver = data;
      });
    }
  }

  ngOnInit() {
    this.messageForm = this.formBuilder.group({
      message: ['', [Validators.required, Validators.maxLength(2000)]]
    });

    this.getApproverSummary();
  }

  ngAfterViewInit() {
    this.serviceService.getServiceMessage(+this.aRoute.snapshot.paramMap.get('id')).subscribe(data => {
      this.serviceMessage = data;
    });
  }

  ngOnChanges() {
    if (this.isOpened) {
      this.approver = null;
    }
  }

  switchTripEditMode() {
    this.tripEditMode = !this.tripEditMode;
  }

  switchDiscountManagerMode() {
    this.discountManagerMode = !this.discountManagerMode;
  }

  openService() {
    this.changeStatusByProvider(2, 'Open');
  }

  assignService() {
    this.changeStatusByApprover(3, 'Assigned');
  }

  publishService() {
    this.changeStatusByApprover(4, 'Published');
  }

  removeService() {
    this.changeStatusByProvider(6, 'Removed');
    this.openSnackBar('Trip Removed');
  }

  archiveService() {
    this.changeStatusByProvider(7, 'Archived');
  }

  sendServiceMessage() {
    this.isDisabled = true;
    this.loadingService.startLoading();
    if (!this.f.message.errors) {
      this.serviceService.sendServiceMessage(this.thisTrip, this.messageForm.controls.message.value).subscribe(
        () => {
          this.trip.approverId = this.authService.getDecodedToken().sub;
          this.trip.status = 'Under clarification';
          this.serviceService.getServiceMessage(+this.aRoute.snapshot.paramMap.get('id')).subscribe(data => {
            this.serviceMessage = data;
          });
          this.tripService.getTripById(+this.aRoute.snapshot.paramMap.get('id')).subscribe(data => {
            this.trip = data;
          });
          this.openSnackBar('Trip\'s clarification request posted');
          this.isDisabled = false;
          this.loadingService.stopLoading();
        }, error => {
          console.log('Error while changing status by Approver:' + error);
          this.isDisabled = false;
          this.loadingService.stopLoading();
        })
    } else {
      this.isDisabled = false;
      this.loadingService.stopLoading();
    }
  }

  changeStatusByApprover(statusId: number, status: string) {
    this.isDisabled = true;
    this.loadingService.startLoading();
    this.serviceService.changeStatusByApprover(this.thisTrip, statusId).subscribe(
      () => {
        this.trip.approverId = this.authService.getDecodedToken().sub;
        this.trip.status = status;
        this.serviceService.getServiceMessage(+this.aRoute.snapshot.paramMap.get('id')).subscribe(data => {
          this.serviceMessage = data;
        });
        this.tripService.getTripById(+this.aRoute.snapshot.paramMap.get('id')).subscribe(data => {
          this.trip = data;
        });
        if (this.trip.approverId && this.trip.approverId !== 0) {
          this.userService.getUserSummaryById(this.trip.approverId).subscribe(data => {
            this.approver = data;
          });
        }
        this.openSnackBar('Trip ' + status);
        this.isDisabled = false;
        this.loadingService.stopLoading();
      },
      error => {
        console.log('Error while changing status by Approver:' + error);
        this.isDisabled = false;
        this.loadingService.stopLoading();
      });
  }

  changeStatusByProvider(statusId: number, status: string) {
    this.isDisabled = true;
    this.loadingService.startLoading();
    this.serviceService.changeStatusByProvider(this.thisTrip, statusId).subscribe(
      () => {
        this.trip.approverId = null;
        this.trip.status = status;
        this.serviceService.getServiceMessage(+this.aRoute.snapshot.paramMap.get('id')).subscribe(data => {
          this.serviceMessage = data;
        });
        this.tripService.getTripById(+this.aRoute.snapshot.paramMap.get('id')).subscribe(data => {
          this.trip = data;
        });
        if (this.trip.approverId && this.trip.approverId !== 0) {
          this.userService.getUserSummaryById(this.trip.approverId).subscribe(data => {
            this.approver = data;
          });
        }

        if (statusId !== 6) {
          this.openSnackBar('Trip ' + status);
        }

        this.approver = null;
        this.isDisabled = false;
        this.loadingService.stopLoading();
      }, error => {
        this.isDisabled = false;
        this.loadingService.stopLoading();
        console.log('Error while changing status by Provider:' + error)
      });
  }

  get f() {
    return this.messageForm.controls;
  }

  routeToApprover() {
    if (this.serviceMessage.approverId) {
      this.route.navigate(['/user/' + this.serviceMessage.approverId])
    }
  }

  openSnackBar(message: string) {
    this.shareDataService.snackBarMessage = message;
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: 1000
    })
  }

  onUpdateDiscountManager(discount: Discount) {
    this.switchDiscountManagerMode();
    this.switchDiscountManagerMode();

    this.tripService.getTripById(this.trip.id).subscribe(
      data => this.editEvent.emit(data),
      err => alert(err)
    )
  }

  onTripEditEventCather(editedTrip: Trip) {
    this.switchTripEditMode();
    this.editEvent.emit(editedTrip);
  }

  onTripArchivedEdit(editedTrip: Trip) {
    this.changeStatusByProvider(6, 'Removed');

    this.serviceService.getServiceMessage(editedTrip.id).subscribe(data => {
      this.serviceMessage = data;
    });

    this.onTripEditEventCather(editedTrip);
  }

  statusCssClass(status: string) {
    const str = status.split(' ');
    if (str.length > 1) {
      const newStr = [];
      str.forEach(value => newStr.push(this.titleCasePipe.transform(value)));
      return newStr[0] + newStr[1]
    } else {
      return str;
    }
  }
}
