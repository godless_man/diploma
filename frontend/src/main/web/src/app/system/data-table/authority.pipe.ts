import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'authority'
})
export class AuthorityPipe implements PipeTransform {
  private authorityMap: Map<number, string> = new Map()
    .set(1, 'Admin')
    .set(2, 'Approver')
    .set(3, 'Provider')
    .set(4, 'User');

  constructor() {
  }

  transform(value: any): string {
    if (value === undefined) {
      return 'not available';
    }
    return this.authorityMap.get(value);
  }
}
