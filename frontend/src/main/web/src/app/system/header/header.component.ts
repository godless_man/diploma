import {Component} from '@angular/core';
import {AuthService} from '@services/auth.service';
import {StompClientService} from '@services/stomp-client.service';
import {Utils} from '@helpers/utils.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  token?;
  gravatar = Utils;

  constructor(private authService: AuthService,
              private stompClient: StompClientService) {
    authService.getTokenAsObservable().subscribe(data => {
        this.token = data;
      }
    );
  }

  get authority() {
    return this.authService.getDecodedToken().authority;
  }

  isLoggedIn() {
    return this.authService.currentUserValue !== null;
  }

  logout() {
    this.authService.logout();
    this.stompClient.disconnect();
  }
}
